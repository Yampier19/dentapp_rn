import React, {useEffect, useState} from "react";
import { QueryClient } from "react-query";
import { QueryClientProvider } from "react-query";

import { NavigationContainer } from "@react-navigation/native";
import DrawerNavigator from "@Vistas/Navigation/DrawerNavigator";
import { UserProvider } from "./app/providers/user/user-provider";
import { NotRegisterProvides } from "./app/providers/notRegister/useNotRegister";
import { NativeBaseProvider, extendTheme } from 'native-base';


export default App = () => {
  const queryClient = new QueryClient();
  

    const newColorTheme = {
    brand: {
      900: '#5B8DF6',
      800: '#ffffff',
      700: '#cccccc',
    },
  };

const theme = extendTheme({
  colors: newColorTheme,
});

  return (
     <QueryClientProvider client={queryClient}>
      <UserProvider>
        <NotRegisterProvides>
        <NativeBaseProvider theme={theme}>
          <NavigationContainer>
            <DrawerNavigator />
          </NavigationContainer>
          </NativeBaseProvider>
        </NotRegisterProvides>
      </UserProvider>
    </QueryClientProvider>
  );
};
