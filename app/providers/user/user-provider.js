import React from "react";
import { useContext } from "react";
import { createContext } from "react";

import { useProfile } from "../../services/dentapp/user/useUser";

const AuthenticationContext = createContext();

export const UserProvider = ({ children }) => {
  const { data, isLoading, isError, refetch } = useProfile({
    retry: false,
  });
  if (isLoading) {
    console.log("cargando");
  }
  if (isError) {
    console.log("error");
  }
  return (
    <AuthenticationContext.Provider
      value={{
        data,
        refetch,
      }}
      children={children}
    />
  );
};

export const useUserProvider = () => {
  const user = useContext(AuthenticationContext);
  if (typeof user === "undefined") {
  }
  return user;
};
