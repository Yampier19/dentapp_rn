import React from "react";
import { useState } from "react";
import { useContext } from "react";
import { createContext } from "react";
import { useEffect } from "react";

import { useUserListPendingAwards } from "../../services/dentapp/navident/useNaviDent";
import { useCheckStatusInvoice } from "../../services/dentapp/navident/useNaviDent";

const NotRegisterContext = createContext();

export const NotRegisterProvides = ({ children }) => {
  const [visibleModal, setVisibleModal] = useState(true);
  const [visibleModalStatusInvoice, setVisibleModalStatusInvoice] =
    useState(true);
  const { data, isLoading, isError, refetch, error } = useUserListPendingAwards(
    {
      retry: 2,
    }
  );

  const {
    data: Check,
    isLoading: isLoadingCheck,
    isError: isErrorCheck,
    refetch: refectchCheck,
    error: errorCheck,
  } = useCheckStatusInvoice({
    retryDelay: 1000,
  });

  if (isLoading) {
    console.log("cargando");
  }
  if (isError) {
    console.log("error", error);
  }
  if (isLoadingCheck) {
    console.log("isLoadingCheck");
  }
  if (isErrorCheck) {
    console.log("isErrorCheck", errorCheck);
  }
  let visible = data?.status === 200 && data?.data?.pendiente;
  let status = Check?.status === 200 && Check?.data?.estado;

  return (
    <NotRegisterContext.Provider
      value={{
        data,
        Check,
        visibleModal,
        visible,
        refetch,
        status,
        visibleModalStatusInvoice,
        refectchCheck,
        setVisibleModal,
        setVisibleModalStatusInvoice,
        isLoading,
        isLoadingCheck,
      }}
      children={children}
    />
  );
};

export const useNotRegisterProvider = () => {
  const newRegister = useContext(NotRegisterContext);
  if (typeof newRegister === "undefined") {
  }
  return newRegister;
};
