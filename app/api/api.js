
import config from '../config/config';
import { getToken } from '../utils/utils';

export const Request = async (method, resource, data = null) => {
	const url = `${config.API}/${resource}`;
	const token = await getToken();

	return fetch(url, {
		method: method,
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`,
		},
		body: data ? JSON.stringify(data) : null,
	});
};

export const Upload = async (resource, formData) => {
	const url = `${config.API}/${resource}`;
	const token = await getToken();

	return fetch(url, {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'multipart/form-data',
			Authorization: `Bearer ${token}`,
		},
		body: formData,
	});
};