import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { getAmountUsers } from "./hallodent-service";
import { getListUser } from "./hallodent-service";
import { sendFacture } from "./hallodent-service";

export const useGetAmount = () => {
  return useQuery(
    ["hallodent/ammount"],
    () => getAmountUsers().then(res => res.data)
  )
};

export const useListUsers = () => {
  return useQuery(
    ["hallodent/listUsers"],
    () => getListUser().then(res => res.data)
  )
}

export const useSendFacture = (data) => {
  return useMutation((data) => sendFacture(data).then(res => res.data))
}