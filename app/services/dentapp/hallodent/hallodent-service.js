import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const getAmountUsers = () => {
  const url = dentappApiRoutes.v1.hallodent.amount();
  return dentappApiAxios.get(url);
}

export const getListUser = () => {
  const url = dentappApiRoutes.v1.hallodent.list();
  return dentappApiAxios.get(url);
}

export const sendFacture = (data) => {
  const url = dentappApiRoutes.v1.hallodent.sendOne();
  return dentappApiAxios.post(url, data);
}