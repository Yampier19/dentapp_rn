import axios from "axios";

import { getToken } from "../../utils/utils";
import Config from "../../config/config";

export const dentappApiAxios = axios.create();

dentappApiAxios.interceptors.request.use(async (config) => {
  const token = await getToken();
  if (token) {
    config.headers.AUTHORIZATION = `Bearer ${token}`;
  }
  return config;
});

dentappApiAxios.interceptors.request.use((config) => {
  const baseURL = Config.API;
  return { baseURL, ...config };
});
