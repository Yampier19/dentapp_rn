import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const login = (data) => {
  const url = dentappApiRoutes.v1.auth.login();
  return dentappApiAxios.post(url, data);
};

export const logout = () => {
  const url = dentappApiRoutes.v1.auth.logout();
  return dentappApiAxios.post(url);
};

export const forgotPassword = (data) => {
  const url = dentappApiRoutes.v1.auth.forgotPassword();
  return dentappApiAxios.post(url, data);
};

export const changePassword = (data) => {
  const url = dentappApiRoutes.v1.auth.changePassword();
  return dentappApiAxios.post(url, data);
};

export const changeDelivery = (data) => {
  const url = dentappApiRoutes.v1.auth.changeDelivery();
  return dentappApiAxios.post(url, data);
};

export const signUp = (data) => {
  const url = dentappApiRoutes.v1.auth.signup();
  return dentappApiAxios.post(url, data);
};
