import { useMutation } from "react-query";
import { login } from "./auth-service";
import { forgotPassword } from "./auth-service";
import { changePassword } from "./auth-service";
import { changeDelivery } from "./auth-service";
import { signUp } from "./auth-service";

export const useLogin = () => {
  return useMutation((user) => login(user).then(res => res.data));
};

export const useForgotPasswrod = () => {
  return useMutation((user) => forgotPassword(user).then(res => res.data))
};

export const useChangePassword = () => {
  return useMutation((user) => changePassword(user).then(res => res.data))
};

export const useChangeDelivery = () => {
  return useMutation((user) => changeDelivery(user).then(res => res.data))
};

export const useSignUp = () => {
  return useMutation((user) => signUp(user).then(res => res.data))
};