import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const listAwardCategory = ( category, params ) => {
  const url = dentappApiRoutes.v1.awards.findByCategory(category);
  console.log('url', url)
  return dentappApiAxios.get( url, { params } );
};

export const listAllAwardCategories = () => {
  const url = dentappApiRoutes.v1.awards.categories();
  return dentappApiAxios.get( url );
};

export const awardExchange = (data, id) => {
  const url = dentappApiRoutes.v1.awards.awardExchange(id);
  return dentappApiAxios.post( url ,data);
}
