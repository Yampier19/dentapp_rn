import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { listAwardCategory } from "./awards-service";
import { listAllAwardCategories } from "./awards-service";
import { awardExchange } from "./awards-service";

export const useListAwardCategory = ( category, query) => {
  return useQuery(
    ["awards/listAwardCategory", query], 
    () => listAwardCategory(category, query).then(res => res.data),
    )
};

export const useListAllAwardCategories = () => {
  return useQuery(
    ["awards/listAllAwardCategories"], 
    () => listAllAwardCategories().then(res => res.data)
    )
};

export const useAwardExchange = () => {
  return useMutation((award) => awardExchange(award.award, award.awardId).then(res => res.data));
}
