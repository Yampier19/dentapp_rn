import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const listAll = (count, params) => {
  const url = dentappApiRoutes.v1.announcement.listAll(count);
  return dentappApiAxios.get(url, { params });

}

export const findOne = (id) => {
  const url = dentappApiRoutes.v1.announcement.findOne(id);
  return dentappApiAxios.get(url);
}