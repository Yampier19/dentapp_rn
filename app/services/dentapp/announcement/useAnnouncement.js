import { useQuery } from "react-query";
import { listAll } from "./announcement-service";
import { findOne } from "./announcement-service";

export const useAnnouncementList = (count, query) => {
  return useQuery(
    ["announcement/list", query],
    () => listAll(count, query).then(res => res.data),
  )
};

export const useAnnouncementFindOne = (id) => {
  return useQuery(
    ["announcement/findOne", id],
    () => findOne(id).then(res => res.data),
  )
};