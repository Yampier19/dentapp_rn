import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const listBanners = (count, params) => {
  const url = dentappApiRoutes.v1.banners.listAll(count);
  return dentappApiAxios.get(url, { params });
}