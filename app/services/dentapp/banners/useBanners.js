import { useQuery } from "react-query";
import { listBanners } from "./banners-service";

export const useBannersList = (count, query) => {
  return useQuery(
    ["banners/list", query],
    () => listBanners(count, query).then(res => res.data),
  );
};