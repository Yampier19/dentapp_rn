import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const listBrands = () => {
  const url = dentappApiRoutes.v1.quantum.brands();
  return dentappApiAxios.get(url);
};

export const listProducts = (params) => {
  const url = dentappApiRoutes.v1.quantum.products(params);
  return dentappApiAxios.get(url);
};

export const listDepartments = (params) => {
  const url = dentappApiRoutes.v1.quantum.departments(params);
  return dentappApiAxios.get(url);
};

export const listCities = (params) => {
  const { brand, department } = params;
  const url = dentappApiRoutes.v1.quantum.cities(brand, department);
  return dentappApiAxios.get(url);
};

export const verifyBauche = (params) => {
  const url = dentappApiRoutes.v1.quantum.requestBauche(params);
  return dentappApiAxios.get(url);
};

export const verifyExist = (data) => {
  const url = dentappApiRoutes.v1.quantum.verifyExist();
  return dentappApiAxios.post(url, data);
};

export const redention = (id, data) => {
  const url = dentappApiRoutes.v1.quantum.redention(id);
  return dentappApiAxios.post(url, data);
};

export const listSities = (params) => {
  const { brand, department, city } = params;
  const url = dentappApiRoutes.v1.quantum.sities(brand, department, city);
  return dentappApiAxios.get(url);
};