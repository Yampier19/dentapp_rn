import { useMutation } from "react-query";
import { useQuery } from "react-query";
import { listBrands } from "./quantum-service";
import { listProducts } from "./quantum-service";
import { listDepartments } from "./quantum-service";
import { listCities } from "./quantum-service";
import { listSities } from "./quantum-service";
import { verifyBauche } from "./quantum-service";
import { verifyExist } from "./quantum-service";
import { redention } from "./quantum-service";

export const useListBrands = (query) => {
  return useQuery(["quantum/listBrands"], () =>
    listBrands(query).then((res) => res.data)
  );
};

export const useListProducts = (query) => {
  return useQuery(["quantum/listProducts"], () =>
    listProducts(query).then((res) => res.data)
  );
};

export const useListDepartments = (query) => {
  return useQuery(["quantum/listDepartments"], () =>
    listDepartments(query).then((res) => res.data)
  );
};

export const useListCities = (query) => {
  return useQuery(["quantum/listCities"], () =>
    listCities(query).then((res) => res.data)
  );
};

export const useVerifyBauche = (query) => {
  return useQuery(["quantum/verifyBauche"], () =>
    verifyBauche(query).then((res) => res.data)
  );
};

export const useVerifyExist = () => {
  return useMutation((data) => verifyExist(data).then((res) => res.data));
};

export const useRedention = () => {
  return useMutation(({ id, data }) =>
    redention(id, data).then((res) => res.data)
  );
};

export const useListSities = (query, options) => {
  return useQuery(
    ["quantum/listSities"],
    () => listSities(query).then((res) => res.data),
    options
  );
};
