import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const getListPendingAwards = () => {
  const url = dentappApiRoutes.v1.navident.listPending();
  return dentappApiAxios.get(url);
};

export const getList = () => {
  const url = dentappApiRoutes.v1.navident.list();
  return dentappApiAxios.get(url);
};

export const getRegisterListPendingAwards = () => {
  const url = dentappApiRoutes.v1.navident.registerListPending();
  return dentappApiAxios.get(url);
};

export const getUserListPendingAwards = () => {
  const url = dentappApiRoutes.v1.navident.userListPending();
  return dentappApiAxios.get(url);
};

export const postStoreInvoice = (data) => {
  const url = dentappApiRoutes.v1.navident.storeInvoice();
  return dentappApiAxios.post(url, data);
};

export const getRuleta = () => {
  const url = dentappApiRoutes.v1.navident.ruleta();
  return dentappApiAxios.get(url);
};

export const postExchangeAward = (id, data) => {
  const url = dentappApiRoutes.v1.navident.awardExchangeBono(id);
  console.log("rul", url, data);
  return dentappApiAxios.post(url, data);
};

export const postExchangeAwardProduct = (id, data) => {
  const url = dentappApiRoutes.v1.navident.awardExcahangeProduct(id);
  return dentappApiAxios.post(url, data);
};

export const postExchangeAwardCena = (id, data) => {
  const url = dentappApiRoutes.v1.navident.awardExchangeCena(id);
  return dentappApiAxios.post(url, data);
};

export const postStoreInvoiceSignUp = (data) => {
  const url = dentappApiRoutes.v1.navident.storeInvoiceSignUp();
  return dentappApiAxios.post(url, data);
};

export const postChangeAwardByPoints = (id) => {
  const url = dentappApiRoutes.v1.navident.changeAwardByPoints(id);
  return dentappApiAxios.post(url);
};

export const checkStatusInvoiceSend = () => {
  const url = dentappApiRoutes.v1.navident.checkStatusInvoice();
  return dentappApiAxios.get(url);
};
