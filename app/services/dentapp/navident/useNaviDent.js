import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { getListPendingAwards } from "./navident-service";
import { getList } from "./navident-service";
import { postStoreInvoice } from "./navident-service";
import { getRuleta } from "./navident-service";
import { postExchangeAward } from "./navident-service";
import { postExchangeAwardProduct } from "./navident-service";
import { postExchangeAwardCena } from "./navident-service";
import { getRegisterListPendingAwards } from "./navident-service";
import { getUserListPendingAwards } from "./navident-service";
import { postChangeAwardByPoints } from "./navident-service";
import { postStoreInvoiceSignUp } from "./navident-service";
import { checkStatusInvoiceSend } from "./navident-service";

export const useListPendingAwards = () => {
  return useQuery(["navident/listPending"], () =>
    getListPendingAwards().then((res) => res.data)
  );
};

export const useList = () => {
  return useQuery(["navident/list"], () =>
    getList()
      .then((res) => res.data)
      .catch((err) => console.log("err", err.message))
  );
};

export const useRuleta = (options = {}) => {
  return useQuery(
    ["navident/ruleta"],
    () => getRuleta().then((res) => res.data),
    options
  );
};

export const useListRegisterPendingAwards = (options = {}) => {
  return useQuery(
    ["navident/listRegisterPending"],
    () => getRegisterListPendingAwards().then((res) => res.data),
    options
  );
};

export const useStoreInvoice = () => {
  return useMutation((data) => postStoreInvoice(data).then((res) => res.data));
};

export const useExchangeAward = () => {
  return useMutation(({ id, data }) =>
    postExchangeAward(id, data).then((res) => res.data)
  );
};

export const useExchangeAwardProduct = () => {
  return useMutation(({ id, data }) =>
    postExchangeAwardProduct(id, data).then((res) => res.data)
  );
};

export const useExchangeAwardCena = () => {
  return useMutation(({ id, data }) =>
    postExchangeAwardCena(id, data).then((res) => res.data)
  );
};

export const useUserListPendingAwards = (options = {}) => {
  return useQuery(
    ["navident/listUserPending"],
    getUserListPendingAwards,
    options
  );
};

export const useChangeAwardByPoints = (options = {}) => {
  return useMutation(
    (id) => postChangeAwardByPoints(id).then((res) => res.data),
    options
  );
};

export const usePostInvoiceSignUp = (options = {}) => {
  return useMutation(
    (data) => postStoreInvoiceSignUp(data).then((res) => res.data),
    options
  );
};

export const useCheckStatusInvoice = (options = {}) => {
  return useQuery(["navident/checkinvoice"], checkStatusInvoiceSend, options);
};
