import { dentappApiAxios } from "../dentapp-api-axios";
import { dentappApiRoutes } from "../dentapp-api-routes";

export const getProfile = () => {
  const url = dentappApiRoutes.v1.user.profile();
  return dentappApiAxios.get(url);
}

export const getAvatar = (file) => {
  const url = dentappApiRoutes.v1.user.avatar(file);
  return dentappApiAxios.post(url);
}

export const getLevelUser = () => {
  const url = dentappApiRoutes.v1.user.level();
  return dentappApiAxios.get(url);
}

export const storeIp = (data) => {
  const url = dentappApiRoutes.v1.user.storeIp();
  return dentappApiAxios.post(url, data);
}

export const changeAvatar = (data) => {
  const url = dentappApiRoutes.v1.user.changeAvatar();
  return dentappApiAxios.post(url, data);
}

export const updateProfile = (data) => {
  const url = dentappApiRoutes.v1.user.updateProfile();
  return dentappApiAxios.post(url, data);
}

export const updateProfileDisabled = (data) => {
  const url = dentappApiRoutes.v1.user.updateProfileDisabled();
  return dentappApiAxios.post(url, data);
}

export const updatePassword = (data) => {
  const url = dentappApiRoutes.v1.user.updatePassword();
  return dentappApiAxios.post(url, data);
}