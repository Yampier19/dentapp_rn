import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { getProfile } from "./user-service";
import { getAvatar } from "./user-service";
import { getLevelUser } from "./user-service";
import { storeIp } from "./user-service";
import { changeAvatar } from "./user-service";
import { updateProfile } from "./user-service";
import { updateProfileDisabled } from "./user-service";
import { updatePassword } from "./user-service";

export const useProfile = (options = {}) => {
  return useQuery(
    ["cliente/perfil"],
    () => getProfile().then((res) => res.data),
    options
  );
};

export const useAvatar = () => {
  return useMutation((file) => getAvatar(file).then((res) => res));
};

export const useProfileLevel = () => {
  return useQuery(["cliente/nivel"], () => getLevelUser().then((res) => res));
};

export const useChangeAvatar = () => {
  return useMutation((data) => changeAvatar(data).then((res) => res.data));
};

export const useUpdatePassword = () => {
  return useMutation((pass) => updatePassword(pass).then((res) => res.data));
};

export const useUpdateProfile = () => {
  return useMutation((profile) =>
    updateProfile(profile).then((res) => res.data)
  );
};

export const useUpdateProfileDisabled = () => {
  return useMutation((activation) =>
    updateProfileDisabled(activation).then((res) => res.data)
  );
};
export const useStoreIp = () => {
  return useMutation((ip) => storeIp(ip).then((res) => res.data));
};
