
import React, {  useEffect, useRef } from 'react';
import Pusher from 'pusher-js/react-native';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import { Platform } from 'react-native';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
    icon: '../../assets/icon.png'
  }),
});

export const pusher = new Pusher('577986b8e0866841eb12', {
  cluster: 'us2'
});


const channel = pusher.subscribe('notificacion-difusion');
channel.bind('evento-difusion', async function(data) {
  if(data){
    await schedulePushNotification(data);
  }
});

export default function PushNotification({email}){
  // const [expoPushToken, setExpoPushToken] = useState('');
  // const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  useEffect(() => {
    if(email){
      // facturas
      const factura = pusher.subscribe(`factura.${email}`);
      factura.bind('facturaState', async function (data) {
        if (data) {
          await schedulePushNotification(data);
        }
      });
      
      // premios
      const premio = pusher.subscribe(`premio.${email}`);
      premio.bind('premioState', async function (data) {
        if (data) {
          await schedulePushNotification(data);
        }
      });
      
      // puntos
      const puntos = pusher.subscribe(`puntos.${email}`);
      puntos.bind('puntosState', async function (data) {
        if (data) {
          await schedulePushNotification(data);
        }
      });
    }
  }, [email])

  useEffect(() => {
    registerForPushNotificationsAsync().then();

    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      // setNotification(notification);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });

    // return () => {
    //   Notifications.removeNotificationSubscription(notificationListener.current);
    //   Notifications.removeNotificationSubscription(responseListener.current);
    // };
  }, []);

  return <></>;
};


export async function schedulePushNotification({title, message}) {
  await Notifications.scheduleNotificationAsync({
    content: {
      title: title || "DentApp",
      body: message || 'Tienes una nueva notificación de Dentapp',
      // data: { data: 'da click para ir a la vista' },
    },
    trigger: { seconds: 1 },
  });
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}

