import { View, Text, RefreshControl, Image, StyleSheet } from 'react-native'
import React, { useEffect, useState } from "react";
import { When } from "react-if";
import { Unless } from "react-if";
import {
    useProfile
} from "../../services/dentapp/user/useUser";
import { Spinner } from "native-base"
import { resolveImage, getToken } from "../../utils/utils";
import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize,
    responsiveFontSize,
} from "react-native-responsive-dimensions";

export default function Stack_Dental83 ({ props, profile, refresh }) {
    const [user, setUser] = useState(profile);
    const [loading, setLoading] = useState(false);
    const [loadingProfile, setLoadingProfile] = useState(false);
    const { data: Profile } = useProfile();


    useEffect(() => {
        setLoadingProfile(true);
        getProfile();
    }, [refresh]);


    const getProfile = async () => {
        try {
            const data = Profile;
            if (data?.status === "success" && data?.datos) {
                const currentData = await resolveImage(
                    [data?.datos],
                    "cliente/avatar",
                    "foto",
                    "uploads/",
                    true,
                    "POST"
                );
                setUser({
                    ...currentData?.[0],
                    password: data?.password,
                    message: data?.message,
                });
            } else if (data?.message === "Unauthenticated") {
                navigation.navigate("Login");
            }
            setLoading(false);
            setLoadingProfile(false);
        } catch (error) {
            setLoadingProfile(false);
        }
    };


     


    return (
        <View>
            <When condition={!loadingProfile}>
                <When condition={!loading}>
                    <View >
                        <When condition={user?.distribuidor}>
                            {user?.distribuidor == 'Dental 83' ?
                                <>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 120, height: 30, marginRight: 10 }} source={require('@Public/Img/DentApp.png')} resizeMode='cover' />
                                        <Image style={{ width: 105, height: 25, }} source={require('@Public/Img/Distribuidor/dental83.png')} resizeMode='cover' />
                                    </View>

                                </>


                                : null}

                            {user?.distribuidor == 'Dental Nader' ?
                                <>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 120, height: 30, marginRight: 10 }} source={require('@Public/Img/DentApp.png')} resizeMode='cover' />
                                        <Image style={{ width: 50, height: 30, }} source={require('@Public/Img/Distribuidor/dentalnader.png')} resizeMode='cover' />
                                    </View>

                                </>


                                : null}

                            {user?.distribuidor == 'Adental' ?
                                <>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 120, height: 30, marginRight: 10 }} source={require('@Public/Img/DentApp.png')} resizeMode='cover' />
                                        <Image style={{ width: 105, height: 25, }} source={require('@Public/Img/Distribuidor/dentaladental.png')} resizeMode='cover' />
                                    </View>

                                </>


                                : null}

                            {user?.distribuidor == 'Bracket' ?
                                <>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image style={{ width: 120, height: 30, marginRight: 10 }} source={require('@Public/Img/DentApp.png')} resizeMode='cover' />
                                        <Image style={{ width: 105, height: 25, }} source={require('@Public/Img/Distribuidor/dentalbracket.png')} resizeMode='cover' />
                                    </View>

                                </>


                                : null}

                        </When>
                        <Unless condition={user?.distribuidor == 'Dental 83' || user?.distribuidor == 'Adental' || user?.distribuidor == 'Dental Nader' || user?.distribuidor == 'Bracket'}>

                            <View style={{ flexDirection: 'row' }}>
                                <Image style={{ width: responsiveScreenWidth(30), height: responsiveScreenHeight(3), marginRight: 10 }} source={require('@Public/Img/DentApp.png')} resizeMode='cover' />

                            </View>

                        </Unless>

                    </View>
                </When>
                <Unless condition={!loading}>
                    <Spinner color="white" />
                </Unless>
            </When>
            <Unless condition={!loadingProfile}>
                <Spinner color="white" />
            </Unless>



        </View>

    )
}

