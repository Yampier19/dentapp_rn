export default {
    Dental83Head: '#014062',
    DentalNaderHead: '#0079A7',
    DentalAdentalHead: '#17171F',
    BracketHead: '#FFFFFF',

    Dental83Body: '#5DC3F0',
    DentalNaderBody: '#00BAEC',
    DentalAdentalBody: '#EE0000',
    BracketBody: '#00B6E5',

    Dental83Box: '#014062',
    DentalNaderBox: '#0079A7',
    DentalAdentalBox: '#FFFFFF',
    BracketBox: '#00B6E5',

    Dental83BoxTwo: '#014062',
    DentalNaderBoxTwo: '#0079A7',
    DentalAdentalBoxTwo: '#17171F',
    BracketBoxTwo: '#0088CC',

    Dental83Text: '#FFFFFF',
    DentalNaderText: '#FFFFFF',
    DentalAdentalText: '#FFFFFF',
    BracketText: '#FFFFFF'
}