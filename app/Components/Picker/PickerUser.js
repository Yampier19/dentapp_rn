import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

const PickerDoc = ({ onChange, value }) => {
  return (
    <Select
       placeholder="Selecciona el tipo de usuario"
        placeholderStyle={{ color: "#bfc6ea" }}
        placeholderIconColor="#007aff"
        onValueChange={onChange}
        selectedValue={value}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
        <Select.Item label="Selecciona el tipo de usuario" value="" />
               <Select.Item
                    label="Odontólogo"
                    value="6"
                />
                <Select.Item
                    label="Asistente"
                    value="4"
                />
    </Select>
  );
};

export default PickerDoc;
