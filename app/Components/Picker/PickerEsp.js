import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

const PickerDoc = ({ onChange, value }) => {
  return (
    <Select
       placeholder="Selecciona tu distribuidor"
        placeholderStyle={{ color: "#bfc6ea" }}
        placeholderIconColor="#007aff"
        onValueChange={onChange}
        selectedValue={value}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
         <Select.Item label="Selecciona tu especialidad" value="" />
        <Select.Item
          label="Especialista - Odontología"
          value="Especialista - Odontología"
        />
        <Select.Item
          label="Especialista - Endodoncia"
          value="Especialista - Endodoncia"
        />
        <Select.Item
          label="Especialista - Maxilofacial"
          value="Especialista - Maxilofacial"
        />
        <Select.Item
          label="Especialista - Ortodoncia"
          value="Especialista - Ortodoncia"
        />
        <Select.Item
          label="Especialista - Rehabilitación oral"
          value="Especialista - Rehabilitación ora"
        />
        <Select.Item
          label="Especialista - Operatoria dental"
          value="Especialista - Operatoria dental"
        />
    </Select>
  );
};

export default PickerDoc;
