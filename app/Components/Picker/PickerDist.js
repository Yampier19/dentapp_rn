import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

const PickerDoc = ({ onChange, value }) => {
  return (
    <Select
       placeholder="Selecciona tu distribuidor"
        placeholderStyle={{ color: "#bfc6ea" }}
        placeholderIconColor="#007aff"
        onValueChange={onChange}
        selectedValue={value}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
        <Select.Item label="Selecciona tu distribuidor" value="" />
        <Select.Item label="Bracket" value="Bracket" />
        <Select.Item label="Aldental" value="Aldental" />
        <Select.Item label="Vertice Aliados" value="Vertice Aliados" />
        <Select.Item label="Dental Palermo" value="Dental Palermo" />
        <Select.Item label="Dentales Antioquía" value="Dentales Antioquía" />
        <Select.Item
          label="Casa Dental Gabriel Velasquez"
          value="Casa Dental Gabriel Velasquez"
        />
        <Select.Item label="Dentales Padilla" value="Dentales Padilla" />
        <Select.Item label="Orbidental" value="Orbidental" />
        <Select.Item
          label="Dentales y Acrilicos"
          value="Dentales y Acrilicos"
        />
        <Select.Item label="Dental 83" value="Dental 83" />
        <Select.Item
          label="Janer Distribuciones"
          value="Janer Distribuciones"
        />
        <Select.Item label="Dental Nader" value="Dental Nader" />
        <Select.Item label="Casa Odontológica" value="Casa Odontológica" />
        <Select.Item label="Dentales Market" value="Dentales Market" />
        <Select.Item label="Adental" value="Adental" />
    </Select>
  );
};

export default PickerDoc;
