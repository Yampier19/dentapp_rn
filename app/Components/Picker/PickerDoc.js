import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

const PickerDoc = ({ onChange, value }) => {
  return (
    <Select
       placeholder="Selecciona el tipo de documento"
        placeholderStyle={{ color: "#bfc6ea" }}
        placeholderIconColor="#007aff"
        onValueChange={onChange}
        selectedValue={value}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
        <Select.Item label="Selecciona el tipo de documento" value="" />
               <Select.Item
                    label="DNI"
                    value="1"
                />
                <Select.Item
                    label="Cédula"
                    value="2"
                />
                <Select.Item
                    label="Pasaporte"
                    value="3"
                />
                <Select.Item
                    label="Nit"
                    value="4"
                />
                <Select.Item
                    label="Cédula de extranjeria"
                    value="5"
                />
    </Select>
  );
};

export default PickerDoc;
