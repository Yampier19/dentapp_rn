import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

const PickerCity = ({ onChange, value }) => {
  return (
    <Select
       placeholder="Selecciona tu ciudad"
        placeholderStyle={{ color: "#bfc6ea" }}
        placeholderIconColor="#007aff"
        onValueChange={onChange}
        selectedValue={value}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
        <Select.Item label="Selecciona tu ciudad" value="" />
        <Select.Item label="BOGOTA" value="11001" />
        <Select.Item label="MEDELLIN" value="5001" />
        <Select.Item label="BARRANQUILLA" value="8001" />
        <Select.Item label="CARTAGENA" value="13001" />
        <Select.Item label="MONTERIA" value="23001" />
        <Select.Item label="VILLAVICENCIO" value="50001" />
        <Select.Item label="CALI" value="76001" />
        <Select.Item label="CUCUTA" value="54001" />
        <Select.Item label="BUCARAMANGA" value="68001" />
        <Select.Item label="PASTO" value="52001" />
    </Select>
  );
};

export default PickerCity;
