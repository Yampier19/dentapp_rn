import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useRef } from "react";
import { StyleSheet } from "react-native";
import { FlatList } from "react-native";
import { Text } from "react-native";
import { View } from "react-native";
import { Image } from "react-native";
import { TouchableOpacity } from "react-native";
import { Dimensions } from "react-native";
import { Camera } from "expo-camera";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import * as MediaLibrary from "expo-media-library";
import { When } from "react-if";
import { Unless } from "react-if";
import { Switch } from "react-if";
import { Case } from "react-if";

const windowWidth = Dimensions.get("window").width;

const CameraComponent = ({ changeImage }) => {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [flash, setFlash] = useState(Camera.Constants.FlashMode.off);
  const [photos, setPhotos] = useState([]);
  const [image, setImage] = useState(null);
  const cameraRef = useRef(null);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  useEffect(() => {
    (async () => {
      let { status } = await MediaLibrary.requestPermissionsAsync();
      let media = await MediaLibrary.getAssetsAsync({
        mediaType: ["photo"],
        sortBy: MediaLibrary.SortBy.default,
      });
      setPhotos(media.assets);
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const takePicture = async () => {
    if (cameraRef) {
      const data = await cameraRef.current.takePictureAsync();
      changeImage(data.uri);
    }
  };

  const selectImage = (uri) => {
    setImage(uri);
    changeImage(uri);
  };

  const ItemPhoto = ({ onPress, photo }) => (
    <TouchableOpacity onPress={onPress}>
      <Image
        source={{ uri: photo.uri }}
        resizeMode={"cover"}
        style={styles.image}
      />
    </TouchableOpacity>
  );

  const renderItemPhoto = ({ item }) => {
    return <ItemPhoto photo={item} onPress={() => selectImage(item.uri)} />;
  };

  return (
    <View style={styles.container}>
      <View style={styles.photosContainer}>
        <FlatList
          horizontal={true}
          data={photos}
          renderItem={renderItemPhoto}
          keyExtractor={(item) => item.id}
          extraData={image}
        />
      </View>
      <Camera
        ref={cameraRef}
        style={styles.camera}
        type={type}
        flashMode={flash}
      >
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.buttonActiveFlash}
            onPress={() => {
              setFlash(
                flash === Camera.Constants.FlashMode.off
                  ? Camera.Constants.FlashMode.on
                  : Camera.Constants.FlashMode.off
              );
            }}
          >
            <When condition={type === Camera.Constants.Type.front}>
              <MaterialCommunityIcons
                name="flash-off"
                size={40}
                color="white"
              />
            </When>
            <Unless condition={type === Camera.Constants.Type.front}>
              <Switch>
                <Case condition={flash === Camera.Constants.FlashMode.off}>
                  <MaterialCommunityIcons
                    name="flash-off"
                    size={40}
                    color="white"
                  />
                </Case>
                <Case condition={flash === Camera.Constants.FlashMode.on}>
                  <MaterialCommunityIcons
                    name="flash"
                    size={40}
                    color="white"
                  />
                </Case>
              </Switch>
            </Unless>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={takePicture}
            style={styles.buttonTakePicture}
          ></TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonChangeCamera}
            onPress={() => {
              if (Camera.Constants.Type.front)
                setFlash(Camera.Constants.FlashMode.off);
              setType(
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back
              );
            }}
          >
            <MaterialCommunityIcons
              name="camera-party-mode"
              size={40}
              color="white"
            />
          </TouchableOpacity>
        </View>
      </Camera>
    </View>
  );
};

export default CameraComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
    position: "relative",
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "column",
    margin: 20,
  },
  buttonTakePicture: {
    width: 120,
    height: 120,
    position: "absolute",
    bottom: 10,
    borderRadius: 60,
    borderColor: "white",
    borderWidth: 1,
    alignSelf: "center",
    alignItems: "center",
    left: windowWidth / 2 - 75,
  },
  buttonChangeCamera: {
    height: 120,
    display: "flex",
    justifyContent: "center",
    alignSelf: "flex-end",
    alignItems: "center",
    position: "absolute",
    bottom: 10,
    right: 10,
  },
  buttonActiveFlash: {
    height: 120,
    display: "flex",
    justifyContent: "center",
    alignSelf: "flex-end",
    alignItems: "center",
    position: "absolute",
    bottom: 10,
    left: 10,
  },
  photosContainer: {
    width: windowWidth,
    backgroundColor: "black",
    position: "absolute",
    bottom: 160,
    zIndex: 10,
  },
  image: {
    width: 100,
    height: 130,
    marginRight: 5,
  },
  text: {
    fontSize: 18,
    color: "white",
  },
});
