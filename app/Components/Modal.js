import React from 'react';
import { StyleSheet, Modal, View, Text, TouchableOpacity } from 'react-native';
import { Icon, Button } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize
} from "react-native-responsive-dimensions";
import { Ionicons, FontAwesome } from '@expo/vector-icons';

export default function ModalComponent({visible, message, type, onClose, fc}) {

  const onCloseModal = () => {
    onClose({visible: false});  
    if(fc){
      fc();
    }
  }

  const resolveIcon = () => {
    let icon;
    switch (type) {
      case 'info':
        icon = "ios-info"
        break;
      case 'warning':
        icon = "ios-warning"
        break;
      case 'error':
        icon = "ios-help"
        break;
      default:
        icon = "ios-check"
        break;
    }
    return icon;
  };

  return (
    visible ? (
      <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={onCloseModal}
      >
        <View style={styles.centeredView}>
          <View style={styles.box_content}>
          <View style={styles.box_icon}> 
          <Icon
                                            as={Ionicons}
                                            name={resolveIcon()}
                                            size="20"
                                            color="black"
                                            style={{marginTop: 20}}
                                            />
          </View>
          <View style={styles.box_text}> 
            <Text style={styles.text}>{message}</Text>
            </View>
            <View style={styles.box_buttom}> 
            <TouchableOpacity
              style={styles.buttom}
              onPress={onCloseModal}
            >
              <Text style={styles.text_buttom}>CERRAR</Text>
            </TouchableOpacity>
            </View>
          </View>
        </View>
        </Modal>
     
    </View>
    ) : (
      <></>
    )
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  box_content: {
    backgroundColor: 'white',
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(35),
    borderRadius: 10,
    alignItems: 'center'
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignItems: 'center',
    justifyContent: 'center',
    height: responsiveScreenHeight(12)
  },
  icon: {
    color: '#64C2C8',
    fontSize: responsiveScreenFontSize(8)
  },
  box_text:{
    width: responsiveScreenWidth(90),
    alignItems: 'center',
    justifyContent: 'center',
    height: responsiveScreenHeight(15)
  },
  text: {
    fontSize: responsiveScreenFontSize(3),
    marginHorizontal: responsiveScreenWidth(5),
    textAlign: 'center'
  },
  box_buttom:{
    width: responsiveScreenWidth(90),
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  buttom: {
    backgroundColor: '#64C2C8',
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(6),
    justifyContent: 'center',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: 'center',
    color: 'white'
  }
});