import React, {useState, useRef} from 'react';
import { Image, StyleSheet, View, Dimensions, Button } from 'react-native';
import { Video, AVPlaybackStatus } from 'expo-av';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { WebView } from "react-native-webview";

export default function ViewFile ({filetype, file, token, style, resizeMode=null, stop=true}){
  const video = useRef(null);
  const [status, setStatus] = useState({});

  if(filetype === 'png' || filetype === 'jpg'){
      return (
        <Image source={{uri: file}} resizeMode={resizeMode || "cover"} style={style || styles.image} />
      );
  }
  if(filetype === 'mp4'){
    let localVideo = video.current;
    if(localVideo){
      if(!stop){
        localVideo.stopAsync();
      }
    }
    return (
      <View style={style}>
         <Video
        ref={video}
        style={styles.video2}
        source={{
          uri: file,
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
          }
        }}
        useNativeControls
        resizeMode="contain"
        isLooping
        isBuffering
        onPlaybackStatusUpdate={status => setStatus(() => status)}
      />

      </View>
    );
  }

  if(filetype === 'pdf'){
    return (
      <View style={style || styles.pdfContainer}>
        <WebView
          style={{ width: wp('90%'), height: hp(200), marginVertical: 30, alignSelf: 'center' }}
          customStyle={{
          readerContainerZoomContainer: {
            borderRadius: 30,
            backgroundColor: '#64C2C8',
          },
          readerContainer: {
            backgroundColor: 'white'
          },
        }}
          withPinchZoom='true'
          withScroll='true'
          source={{
            uri: file,
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            }
          }}
        />

        {/* <WebView
          bounces
          source={{
            uri: file,
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            }
          }}
          style={{ width: Dimensions.get('screen').width, height: 200 }}
        /> */}
      </View>
    )
  }

  return <></>;
};

const styles = StyleSheet.create({
  image: {
    width: wp('45%'),height: 270, alignSelf: 'center'
  },
  videoStyle: {
    width: '100%',
    height: '100%'
  },
  video: {
      flex: 1,
      width: '100%',
      height: 120,
      alignSelf: 'center',
      justifyContent: "center",
      alignItems: "center",
  },
  playButton: {
      position: 'absolute',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      top: '30%',
      left: '40%',
      width: 40,
      height: 40,
      borderRadius:20,
      backgroundColor: 'rgba(255,255,255,.8)'
  },
  textPlay: {
    color: 'black'
  },
  pdfContainer:{
    flex: 1,
    width: '100%',
    height: 120
  },
  btntext: {
    color: 'black'
  },

    container2: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#ecf0f1',
    },
    video2: {
      alignSelf: 'center',
      width: '90%',
      height: '100%',
    },
    buttons2: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },

});
