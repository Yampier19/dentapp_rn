import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function NotFound ({message}){
  return (
    <View style={styles.container}>
      <Text style={styles.message}>{message || 'No hay datos'}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
  },
  message: {
    color: '#000'
  }
});
