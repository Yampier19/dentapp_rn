import React from "react";
import { View, Dimensions, Text } from "react-native";
import * as Animatable from 'react-native-animatable'

const LoadingScreen = () => {
  const { width, height } = Dimensions.get('window');
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FAFAFA'}}>
      <Animatable.Image
        easing="ease-out"
        iterationCount="infinite"
        style={
          {
            width: width*0.7,
            height: height*0.4,
          }
          }
          source={require('@Public/Img/factura.gif')}
      />
      <Text>EXTRAYENDO INFORMACIÓN</Text>
    </View>
  )
}

export default LoadingScreen;