import React from "react";
import { Modal, StyleSheet, Text, View, ImageBackground } from "react-native";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";

const ModalStatusInvoice = (props) => {
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={
          props.status !== "falta" &&
          props.status !== "no registro" &&
          props.status !== "no participante" &&
          props.status !== "aceptado"
            ? true
            : false
        }
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ImageBackground
              source={require("@Public/Img/Navidad/fondo-gana.png")}
              style={{ width: "100%", height: "100%" }}
            >
              <View style={styles.box_title}>
                <Text style={styles.title}>
                  Lo sentimos tu factura sigue en {props.status}
                </Text>
              </View>
            </ImageBackground>
          </View>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.8)",
  },
  modalView: {
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(75),
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  box_title: {
    width: responsiveScreenWidth(80),
    height: responsiveScreenWidth(110),
    alignSelf: "center",
    justifyContent: "center",
    marginVertical: 20,
    display: "flex",
    alignItems: "center",
  },
  title: {
    fontFamily: "DancingScript_400Regular",
    fontSize: responsiveScreenFontSize(6),
    color: "white",
    textAlign: "center",
  },
  box_description: {
    width: responsiveScreenWidth(70),
    height: responsiveScreenWidth(40),
    borderRadius: 20,
    backgroundColor: "white",
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  text_description: {
    color: "white",
    fontSize: responsiveScreenFontSize(2.5),
    textAlign: "center",
  },
  box_redimir: {
    width: responsiveScreenWidth(55),
    height: responsiveScreenHeight(8),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginVertical: 20,
  },
  boton_redimir: {
    textAlign: "center",
    backgroundColor: "#E7344C",
    paddingHorizontal: 40,
    paddingVertical: 10,
    borderRadius: 10,
  },
});
export default ModalStatusInvoice;
