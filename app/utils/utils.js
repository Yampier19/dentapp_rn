import { Alert } from 'react-native';
import * as SecureStorage from "expo-secure-store";
import { Request } from '../api/api';
import config from '../config/config';

export const notificationAlert = (title, message) => {
  Alert.alert(
    title,
    message,
    [
      { text: "OK", onPress: () => {} }
    ]
  );
};

export const setToken = async (token) => {
  try {
    await SecureStorage.setItemAsync('-token', token)
  } catch (e) {
    console.log(e)
  }
};

export const setLocalEmail = async (email) => {
  try {
    await SecureStorage.setItemAsync('-email', email)
  } catch (e) {
    console.log(e)
  }
};

export const removeToken = async () => {
  try {
    await SecureStorage.deleteItemAsync('-token')
  } catch(e) {
    console.log(e)
  }
}

export const getToken = async () => {
  let token = null;
  try {
    const value = await SecureStorage.getItemAsync('-token')
    if(value !== null) {
      token = value;
    }
  } catch(e) {
    console.log('Error al leer el token en local: ', e);
  }
  return token
}

export const getEmail = async () => {
  let email = null;
  try {
    const value = await SecureStorage.getItemAsync('-email')
    if(value !== null) {
      email = value;
    }
  } catch(e) {
    console.log('Error al leer el email en local: ', e);
  }
  return email
}

export const resolveImage = async (data, resource, field, strReplace, returnData, method = 'GET') => {
  let images = [];

  if(data.length > 0){
    try {
      for (let index = 0; index < data.length; index++) {
        const item = data[index];
        let blob, filetype, uid;

        if(item[field]){
          uid = item[field].replace(strReplace, '');
          filetype = uid.split('.')?.[1];
          if(filetype === 'mp4'){
            data[index].filetype = filetype;
            data[index].file = `${config.API}/${resource}/${uid}`;
            data[index].token = await getToken();
          } else {
            const response = await Request(method, `${resource}/${uid}`);
            blob = await response.blob();
          }
        } else {
          filetype = 'png';
          const response = await Request(method, `${resource}/placeholder.png`);
          blob = await response.blob();
        }

        if(blob){
          data[index].filetype = filetype;

          if(filetype === 'mp4' || filetype === 'pdf'){
            data[index].file = `${config.API}/${resource}/${uid}`;
            data[index].token = await getToken();
          } else {
            const banner = await blobToBase64(blob);
            images.push(banner);
            data[index].file = banner;
          }
        }

      }
      
      return returnData ? data : images;
    } catch (error) {
      console.log('error:  ', error);
      return [];
    }
  }

  return data
};

const blobToBase64 = blob => {
  const reader = new FileReader();
  reader.readAsDataURL(blob);
  return new Promise(resolve => {
    reader.onloadend = () => {
      resolve(reader.result);
    };
  });
};