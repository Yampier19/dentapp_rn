import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions";

  import {Dimensions} from 'react-native'
  const { width, height } = Dimensions.get('window');

  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default {
    content: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff'
    },

    box_title: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(10),
        justifyContent: 'center',
        marginTop: responsiveScreenHeight(2)
    },

    box_search: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(8),
        justifyContent: 'center'
    },

    box_title_sessions: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(8),
        justifyContent: 'center'
    },

    box_archivo: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(50)
    },

    box_archivo_row: {
        flexDirection: 'row',
        width: responsiveScreenWidth(90),
        flexWrap:'wrap',
        paddingBottom: 50
    },

    box_archivo_touch: {
        marginRight: 0
    },

    box_archivo_item: {
        width: wp('45%'),
        height: hp('30%')
    },

    box_text_view: {
        width: wp('45%'),
        padding: 10,
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },

    box_text_capacitacion: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    }

}