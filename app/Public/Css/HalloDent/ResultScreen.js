import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions";

  import {Dimensions} from 'react-native'
  const { width, height } = Dimensions.get('window');

export default {

    box_title:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(20),
        backgroundColor: '#64C1C6', 
        alignItems: 'center',
        paddingTop: 20
    
    },
    
    box_content1: {
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(20),
        backgroundColor: '#64C1C6',
    },

    box_content2: {
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(10)
    },

    box_content3: {
        width: responsiveScreenWidth(95),
        height: responsiveScreenHeight(65),
        alignSelf: 'center'
    },

    box_content4: {
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(10),
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },

    box_content5:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(20),
        marginVertical: 20,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    }





}