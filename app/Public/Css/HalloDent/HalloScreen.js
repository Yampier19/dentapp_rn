import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions";

  import {Dimensions} from 'react-native'
  const { width, height } = Dimensions.get('window');

export default {

    box_title:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(15),
        backgroundColor: '#64C1C6', 
        justifyContent: 'center',
        alignItems: 'center'
    
    },
    box_content:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(10),
        backgroundColor: '#64C1C6', 
        justifyContent: 'center',
        alignItems: 'center',
    },
    
    box_content2:{
        width: responsiveScreenWidth(100),
        backgroundColor: '#64C1C6', 
        flexDirection: 'row',
        alignItems: 'center'
    },
    
    box_content3:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(20),
        marginTop: 40,
        flexDirection: 'row',
        alignItems: 'center',
    },
    
    box_content4:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(15),
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },
    
    box_content5:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(80),
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },
    
    box_content6:{
        width: responsiveScreenWidth(100),
        height: responsiveScreenHeight(20),
        marginVertical: 20,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    }


}