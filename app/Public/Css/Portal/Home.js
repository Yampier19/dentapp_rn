import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions";

  import {Dimensions} from 'react-native'
  const { width, height } = Dimensions.get('window');

export default {

    content: {
        flex: 1,
        backgroundColor: '#fff'
    },

    box_banner: {
        width: width * 1.0,
        height: width * 0.5 * 0.9,
        marginVertical: 10
    },

    box_banner_image: {
        width: width * 1.0,
        height: width * 0.5 * 0.9
    },

    box_title_noticia: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(8),
        justifyContent: 'center',
        alignSelf: 'center',
    },

    box_noticias: {
        width: responsiveScreenWidth(95),
        height: 420,
        alignSelf: 'center'
    },

    box_noticias_null: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },

    box_buttom_noticias: {
        alignSelf: 'center',
        backgroundColor: '#64C2C8',
        width: responsiveScreenWidth(70),
        height: responsiveScreenHeight(5),
        borderRadius: 10, 
        justifyContent: 'center'
    },

    box_title_capacitacion: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(10),
        justifyContent: 'center',
        alignSelf: 'center'
    },

    box_capacitacion: {
        flexDirection: 'row',
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(25),
        justifyContent: 'center',
        alignSelf: 'center'
    },

    box_item_capacitacion: {
        width: responsiveScreenWidth(45),
        borderRadius: 10,
        height: responsiveScreenHeight(25),
        backgroundColor: 'blue'
    },

    box_archivo_capacitacion: {
        width: responsiveScreenWidth(45),
        borderRadius: 10,
        height: responsiveScreenHeight(25)
    },

    box_descripcion_capacitacion: {
        padding: 10,
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0 
    },

    box_text_descripcion_capacitacion: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold' 
    },

    box_buttom_capacitacion: {
        width: responsiveScreenWidth(90),
        height: responsiveScreenHeight(10),
        justifyContent: 'center',
        alignSelf: 'center'
    },

    box_buttom_redirect: {
        alignSelf: 'center',
        backgroundColor: '#64C2C8',
        width: responsiveScreenWidth(70),
        height: responsiveScreenHeight(5),
        borderRadius: 10, 
        justifyContent: 'center'
    },

    box_text_buttom_capacitacion: {
        fontSize: responsiveScreenFontSize(2),
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold'
    },

}