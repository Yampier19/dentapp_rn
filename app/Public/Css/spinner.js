export default {
  spinner: {
    color: '#E7344C',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  }
}