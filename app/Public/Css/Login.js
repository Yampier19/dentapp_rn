export default {

    container: {
        backgroundColor: '#ffffff',
        flex: 1,
   
    },

    bgContainer: {
        backgroundColor: '#E7344C',
        paddingBottom: 20,
        paddingTop: 25
    },

    encabezado: {
        flexDirection: 'row', 
        alignSelf: 'flex-start', 
        marginLeft: 15, 
        alignItems: 'center'
    },

    encabezadoText: {
        color: 'white',
        fontSize: 20,
        top: 10,
        marginLeft: 10
    },

    contenidoPrincipal: {
        alignContent: 'center',
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: 50,
        paddingBottom: '80%'
    },

    cajonUsuario: {
        borderBottomColor: '#64C2C8',
        borderWidth: 30,
        marginRight: 15
    },

    cajonContraseña: {
        borderBottomColor: '#64C2C8',
        borderWidth: 30,
        marginRight: 15,
        marginTop: 50,
        marginBottom: 50
    },

    botonIngresar: {
        backgroundColor: '#64C2C8',
        alignSelf: 'auto',
        marginHorizontal: 15,
        marginBottom: 20 
    },

    textIngresar: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 15,
        fontWeight: 'bold'
    },

    contenidoSecundario: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginBottom: '25%',
        marginTop: 20
    },

    footer:{
        flex: 1
    },

    footerHelp:{
        flexDirection: 'row',
        alignSelf: 'center',
        top: '5%'
    },

    footerRegistrarme: {
        backgroundColor: '#64C2C8',
        width: 200,
        alignSelf: 'center',
        marginTop: 50
    },

    footerTextRegistrarme: {
        fontWeight: 'bold',
        color: '#fff'
    },

    footerVersion: {
        textAlign: 'center',
        top: '40%'
    },

    
}