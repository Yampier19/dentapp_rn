const config = {
  // API: "http://172.16.20.131:8000/api", // LOCAL
  // API: 'https://preview-dentapp.panelrp.com/api', // DEV
  API: "https://dentapp.panelrp.com/api", // PROD
  // API: "https://prod-dentapp.panelrp.com/api"

};

export default config;
