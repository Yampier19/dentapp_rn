import React, { useState } from 'react'
import { View, Text, Modal, StyleSheet, ScrollView } from 'react-native';
import { Button, Spinner } from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome"
import { When } from "react-if";
import { Unless } from "react-if";
import { TextInput } from "react-native-paper";

import { useForgotPasswrod } from "../../services/dentapp/auth/useAuth";
import ModalComponent from '../../Components/Modal';

export default function OlvidoPassScreen(props) {
    const [modalVisible, setModalVisible] = useState(false);
    const [email, setEmail] = useState('');
    const [dialog, setDialog] = useState({visible: false, message: '', type: ''});
    const { mutateAsync: ForgotPassword, isLoading } = useForgotPasswrod();

    const functionOnpress = () => {
        console.log('functionOnpress');
        props.navigation.push('Login')
        setModalVisible(!modalVisible)
    }

    const isValidEmail = (email) => {
        const res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return res.test(email.toLowerCase());
    };

    const onGetPassword = async () => {
        try {
            if(email){
                if(isValidEmail(email)){
                    const response = await ForgotPassword({email});
                    if(response?.status === 'success'){
                        setModalVisible(true)
                        setEmail('');
                    } else {
                        setDialog({
                            visible: true,
                            message: response?.message || 'Occurrió un error al procesar los datos, intentelo nuevamente',
                            type: 'error'
                        });
                    }
                } else {
                    setDialog({
                        visible: true,
                        message: 'Por favor ingresa un correo válido',
                        type: 'warning'
                    });
                }
            } else {
                setDialog({
                    visible: true,
                    message: 'Por favor ingresa un correo eléctronico',
                    type: 'warning'
                });
            }
        } catch (error) {}
     };

    return (
        <View style={styles.container}>
            <When condition={!isLoading}>
                <ScrollView>
                    <View style={styles.box1}>
                        <Text style={{fontSize: 20, color: 'black'}}>
                        Te enviaremos un correo electrónico con los pasos a seguir para crear tu nueva contraseña
                        </Text>
                    </View>
                    <View style={styles.box2}>
                        <TextInput 
                            keyboardType="email-address"
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={(text)=> setEmail(text)}
                            value={email}
                            placeholder="Dirección de correo*"
                            style={{borderBottomColor: '#64C2C8', borderBottomWidth: 1}}
                        />
                    </View>
                    <View style={styles.box3}>
                    <Button onPress={onGetPassword} style={{backgroundColor: '#64C2C8', alignSelf: 'center', paddingHorizontal: 75}}>
                        <Text style={{color: 'white', fontSize: 20}}>Enviar</Text>
                    </Button>
                    </View>
                    <View style={styles.centeredView}>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={modalVisible}
                                onRequestClose={() => {
                                    Alert.alert("Modal has been closed.");
                                    setModalVisible(!modalVisible);
                                }}
                            >
                                <View style={styles.centeredView}>
                                    <View style={{ width: '90%', height: 250, backgroundColor: 'white', justifyContent: 'flex-end', alignItems: 'center', borderRadius: 20 }}>
                                        <Icon name="check-circle" type="FontAwesome5" style={{ color: '#64C2C8', marginBottom: 20, fontSize: 50 }} />
                                        <Text style={{ fontSize: 20, marginBottom: '15%', marginHorizontal: 30, textAlign: 'center' }}>Se ha enviado un link a tu correo electrónico para restaurar tu cuenta</Text>

                                        <Button onPress={functionOnpress} style={{ backgroundColor: '#64C2C8', justifyContent: 'center', borderBottomEndRadius: 20, borderBottomLeftRadius: 20, width: '100%' }}>
                                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>CERRAR</Text>
                                        </Button>
                                        
                                       
                                    </View>
                                </View>
                            </Modal>

                        </View>
                </ScrollView>
            </When>
            <Unless condition={!isLoading}>
                <Spinner color="red" style={styles.spinner} />
            </Unless>
            <ModalComponent onClose={setDialog} {...dialog} />
        </View>
    )
}

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    container: {
        // flex: 1,
        width: '90%',
        alignSelf: 'center'
    },
    box1: {
        width: '100%',
        height: 150,
        paddingTop: 30
    },
    box2: {
        width: '100%',
        height: 150,
        paddingTop: 30,
    },
    box3: {
        width: '100%',
        height: 200,
        paddingTop: 30
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
})
