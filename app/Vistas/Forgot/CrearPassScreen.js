import React, { useState } from "react";
import { View, Text, Modal, StyleSheet, ScrollView } from "react-native";
import { Button } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { TextInput } from "react-native-paper";

import s from "@Public/Css/style";

export default function CrearPassScreen(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const [text, setText] = React.useState("");
  const [viewPassword, setViewPassword] = useState(true);
  const [viewConfirmPassword, setViewConfirmPassword] = useState(true);

  functionOnpress = () => {
    props.navigation.navigate("Login");
    setModalVisible(!modalVisible);
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.box1}>
          <Text style={{ fontSize: 20, color: "black" }}>
            Crea una contraseña nueva segura que no utilices en otros sitios web
          </Text>
        </View>
        <View style={styles.box2}>
          <TextInput
            label="Contraseña*"
            secureTextEntry={viewPassword}
            right={
              <TextInput.Icon
                name="eye"
                onPress={() => setViewPassword(!viewPassword)}
              />
            }
            style={{
              marginVertical: 10,
              borderBottomWidth: 1,
              borderBottomColor: "#64C2C8",
            }}
            left={<TextInput.Icon name="key" color={"#64C2C8"} />}
          />
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              alignSelf: "flex-end",
              marginBottom: 30,
            }}
          >
            <View
              style={{
                backgroundColor: "#666666",
                width: "75%",
                alignItems: "center",
                borderRadius: 40,
                paddingHorizontal: 10,
                paddingVertical: 5,
              }}
            >
              <Text style={{ fontSize: 7, color: "white" }}>
                Minímo 8 carácteres, una mayúscula, una minúscula y un caracter
                especial
              </Text>
            </View>
            <Icon
              name="info-circle"
              type="FontAwesome5"
              style={{ marginHorizontal: 5, fontSize: 16, color: "#666666" }}
            />
          </View>
          <TextInput
            label="Confirmar contraseña*"
            style={{ borderBottomWidth: 1, borderBottomColor: "#64C2C8" }}
            secureTextEntry={viewConfirmPassword}
            right={
              <TextInput.Icon
                name="eye"
                onPress={() => setViewConfirmPassword(!viewConfirmPassword)}
              />
            }
            left={<TextInput.Icon name="key" color={"#64C2C8"} />}
          />
        </View>
        <View style={styles.box3}>
          <Button
            onPress={() => setModalVisible(true)}
            style={{
              backgroundColor: "#64C2C8",
              alignSelf: "center",
              paddingHorizontal: 75,
            }}
          >
            <Text style={{ color: "white", fontSize: 20 }}>Siguiente</Text>
          </Button>
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                <Icon
                  name="check-circle"
                  type="FontAwesome5"
                  style={{ color: "#64C2C8", marginBottom: 20, fontSize: 50 }}
                />
                <Text
                  style={{
                    fontSize: 20,
                    marginBottom: "15%",
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  La cuenta se a restablecido exitosamente
                </Text>

                <Button
                  onPress={functionOnpress}
                  style={{
                    backgroundColor: "#64C2C8",
                    justifyContent: "center",
                    borderBottomEndRadius: 20,
                    borderBottomLeftRadius: 20,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{ fontSize: 15, fontWeight: "bold", color: "white" }}
                  >
                    CERRAR
                  </Text>
                </Button>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "90%",
    alignSelf: "center",
  },
  box1: {
    width: "100%",
    height: 100,
    paddingTop: 30,
  },
  box2: {
    width: "100%",
    height: 300,
    paddingTop: 30,
  },
  box3: {
    width: "100%",
    height: 200,
    paddingTop: 30,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});
