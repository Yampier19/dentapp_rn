import React, { useState } from "react";
import { View, Text, Modal, StyleSheet, ScrollView } from "react-native";
import { Button, Spinner } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { When } from "react-if";
import { Unless } from "react-if";
import { TextInput } from "react-native-paper";

import { useChangePassword } from "../../services/dentapp/auth/useAuth";
import { useLogin } from "../../services/dentapp/auth/useAuth";
import spinner from "@Public/Css/spinner";
import ModalComponent from "../../Components/Modal";
import { setToken, setLocalEmail } from "../../utils/utils";

export default function PassNewScreen({ route, navigation }) {
  const { distribuidor, email } = route.params;
  const [modalVisible, setModalVisible] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [viewPassword, setViewPassword] = useState(true);
  const [viewConfirmPassword, setViewConfirmPassword] = useState(true);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const { mutateAsync: changePassword, isLoading } = useChangePassword();
  const { mutateAsync: Login, isLoading: isLoadingLogin } = useLogin();

  const functionOnpress = async () => {
    if (distribuidor) {
      const res = await Login({ email, password });
      if (res.status === "success") {
        setModalVisible(false);
        setToken(res.accessToken);
        setLocalEmail(email);
        navigation.push("Home", {
          reload: JSON.stringify(new Date()),
        });
      }
    } else {
      navigation.navigate("Distribuidor", {
        email: email,
        password: password,
        confirmPassword: confirmPassword,
      });
    }
  };

  const handleChangePassword = async () => {
    try {
      if (password !== "" && confirmPassword !== "") {
        if (password.length <= 7) {
          setDialog({
            visible: true,
            message: "La contraseña debe tener mínimo 8 caracteres",
            type: "warning",
          });
          return false;
        }
        if (isPasswordValid(password)) {
          if (password === confirmPassword) {
            const data = {
              email: email,
              password: password,
              confirmation: confirmPassword,
            };
            if (distribuidor) {
              const response = await changePassword(data);
              if (response?.status === "success") {
                setModalVisible(true);
              } else if (response?.status === "error") {
                if (
                  response.message ===
                  "El dato de distribuidor debe ser enviado"
                ) {
                  navigation.navigate("Distribuidor", {
                    email: email,
                    password: password,
                    confirmPassword: confirmPassword,
                  });
                } else {
                  setDialog({
                    visible: true,
                    message: response.message,
                    type: "error",
                  });
                }
              }
            } else {
              setModalVisible(true);
            }
          } else {
            setDialog({
              visible: true,
              message: "La contraseñas deben ser iguales",
              type: "warning",
            });
            return false;
          }
        } else {
          setDialog({
            visible: true,
            message:
              "La contraseñas debe tener por lo menos una letra mayúscula, una letra minúscula y un número",
            type: "warning",
          });
          return false;
        }
      } else {
        setDialog({
          visible: true,
          message: "Tiene campos vacios, por favor verifique",
          type: "warning",
        });
      }
    } catch (error) {}
  };

  const isPasswordValid = (pass) => {
    const res = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,}$/;
    return res.test(pass);
  };

  return (
    <View style={styles.container}>
      <When condition={!isLoading || !isLoadingLogin}>
        <ScrollView>
          <View style={styles.box1}>
            <Text style={{ fontSize: 20, color: "black" }}>
              Crea una contraseña nueva segura que no utilices en otros sitios
              web
            </Text>
          </View>
          <View style={styles.box2}>
            <TextInput
              keyboardType="default"
              label="Contraseña *"
              secureTextEntry={viewPassword}
              value={password}
              onChangeText={setPassword}
              right={
                <TextInput.Icon
                  name="eye"
                  onPress={() => setViewPassword(!viewPassword)}
                />
              }
              style={{
                marginTop: 20,
                borderBottomWidth: 1,
                borderBottomColor: "#64C2C8",
              }}
              left={<TextInput.Icon name="key" color={"#64C2C8"} />}
            />
            <View
              style={{
                flexDirection: "row",
                width: "90%",
                height: 35,
                backgroundColor: "#666666",
                marginBottom: 30,
                paddingHorizontal: 5,
                alignSelf: "flex-end",
                borderRadius: 10,
                marginTop: 10,
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 12.5,
                  color: "white",
                  width: "95%",
                  textAlign: "right",
                  marginRight: 5,
                }}
              >
                Minímo 8 carácteres, una mayúscula y un número
              </Text>
              <Icon
                name="info-circle"
                type="FontAwesome5"
                style={{ color: "white", width: "5%" }}
              />
            </View>
            <TextInput
              keyboardType="default"
              label="Confirmar contraseña *"
              style={{ borderBottomWidth: 1, borderBottomColor: "#64C2C8" }}
              secureTextEntry={viewConfirmPassword}
              value={confirmPassword}
              onChangeText={setConfirmPassword}
              right={
                <TextInput.Icon
                  name="eye"
                  onPress={() => setViewConfirmPassword(!viewConfirmPassword)}
                />
              }
              left={<TextInput.Icon name="key" color={"#64C2C8"} />}
            />
          </View>
          <View style={styles.box3}>
            <Button
              onPress={handleChangePassword}
              style={{
                backgroundColor: "#64C2C8",
                alignSelf: "center",
                paddingHorizontal: 75,
              }}
            >
              <Text style={{ color: "white", fontSize: 20 }}>Siguiente</Text>
            </Button>
          </View>
          <View style={styles.centeredView}>
            <Modal
              animationType="fade"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(false);
              }}
            >
              <View style={styles.centeredView}>
                <View
                  style={{
                    width: "90%",
                    height: 250,
                    backgroundColor: "white",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    borderRadius: 20,
                  }}
                >
                  <Icon
                    name="check-circle"
                    type="FontAwesome5"
                    style={{ color: "#64C2C8", marginBottom: 20, fontSize: 50 }}
                  />
                  <Text
                    style={{
                      fontSize: 20,
                      marginBottom: "15%",
                      marginHorizontal: 30,
                      textAlign: "center",
                    }}
                  >
                    La contraseña ha sido creada con éxito
                  </Text>

                  <Button
                    onPress={functionOnpress}
                    style={{
                      backgroundColor: "#64C2C8",
                      justifyContent: "center",
                      borderBottomEndRadius: 20,
                      borderBottomLeftRadius: 20,
                      width: "100%",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: "bold",
                        color: "white",
                      }}
                    >
                      CERRAR
                    </Text>
                  </Button>
                </View>
              </View>
            </Modal>
          </View>
        </ScrollView>
      </When>
      <Unless condition={!isLoading || !isLoadingLogin}>
        <Spinner color="red" style={spinner.spinner} />
      </Unless>
      <ModalComponent onClose={setDialog} {...dialog} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "90%",
    alignSelf: "center",
  },
  box1: {
    width: "100%",
    height: 100,
    paddingTop: 30,
  },
  box2: {
    width: "100%",
    height: 300,
    paddingTop: 30,
  },
  box3: {
    width: "100%",
    height: 200,
    paddingTop: 30,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});
