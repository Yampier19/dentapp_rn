import React, {useState} from 'react'
import { View, Text, TouchableOpacity, Modal, Image, StyleSheet, ScrollView } from 'react-native';
import { Button, Fab, Right, Label, Content, Accordion, Container, Header, Item, Input } from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome"
import { List } from 'react-native-paper';

import s from '@Public/Css/style'


export default function OlvidoUsuarioScreen(props) {
    const [modalVisible, setModalVisible] = useState(false);
    
    functionOnpress = () => {
        props.navigation.navigate('Login')
        setModalVisible(!modalVisible)
     }

    return (



        <View style={styles.container}>
            <ScrollView>
                <View style={styles.box1}>
                    <Text style={{fontSize: 20, color: 'black'}}>
                        Introduce tu número de teléfono o tu dirección de correo electrónico de recuperación.
                    </Text>
                </View>
                <View style={styles.box2}>
                <Item>
                <Input placeholder="Teléfono o dirección de correo*" style={{borderBottomColor: '#64C2C8', borderBottomWidth: 1}} />
                </Item>
                </View>
                <View style={styles.box3}>
                <Button onPress={() => setModalVisible(true)} style={{backgroundColor: '#64C2C8', alignSelf: 'center', paddingHorizontal: 75}}>
                    <Text style={{color: 'white', fontSize: 20}}>Siguiente</Text>
                </Button>
                </View>
                <View style={styles.centeredView}>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <View style={styles.centeredView}>
                                <View style={{ width: '90%', height: 250, backgroundColor: 'white', justifyContent: 'flex-end', alignItems: 'center', borderRadius: 20 }}>
                                    <Icon name="check-circle" type="FontAwesome5" style={{ color: '#64C2C8', marginBottom: 20, fontSize: 50 }} />
                                    <Text style={{ fontSize: 20, marginBottom: '15%', marginHorizontal: 30, textAlign: 'center' }}>Se ha enviado un link a tu correo electrónico para restaurar tu cuenta</Text>

                                    <Button onPress={functionOnpress} style={{ backgroundColor: '#64C2C8', justifyContent: 'center', borderBottomEndRadius: 20, borderBottomLeftRadius: 20, width: '100%' }}>
                                        <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>CERRAR</Text>
                                    </Button>
                                </View>
                            </View>
                        </Modal>

                    </View>
            </ScrollView>

            
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '90%',
        alignSelf: 'center'
    },
    box1: {
        width: '100%',
        height: 150,
        paddingTop: 30
    },
    box2: {
        width: '100%',
        height: 150,
        paddingTop: 30,
    },
    box3: {
        width: '100%',
        height: 200,
        paddingTop: 30
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
})
