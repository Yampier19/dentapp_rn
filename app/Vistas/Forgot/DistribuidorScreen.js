import React, { useState } from "react";
import { View, Text, Modal, StyleSheet, ScrollView } from "react-native";
import { Button, Spinner } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { RadioButton } from "react-native-paper";
import { When } from "react-if";
import { Unless } from "react-if";

import {
  useChangeDelivery,
  useChangePassword,
  useLogin,
} from "../../services/dentapp/auth/useAuth";
import ModalComponent from "../../Components/Modal";
import { setToken, setLocalEmail } from "../../utils/utils";

export default function DistribuidorScreen({ route, navigation }) {
  const [distribuidor, setDistribuidor] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const { email, password, confirmPassword } = route.params;
  const { mutateAsync: ChangeDistribuidor, isLoading: isLoadingDelivery } =
    useChangeDelivery();
  const { mutateAsync: ChangePassword, isLoading: isLoadingPassword } =
    useChangePassword();
  const { mutateAsync: Login, isLoading: isLoadingLogin } = useLogin();

  const functionOnPress = async () => {
    if (email && password && confirmPassword && distribuidor) {
      const res = await Login({ email, password });
      console.log("res", res);
      if (res.status === "success") {
        setToken(res.accessToken);
        setLocalEmail(email);
        navigation.push("Home", {
          reload: JSON.stringify(new Date()),
        });
        setModalVisible(!modalVisible);
      }
    } else if (email && password) {
      const res = await Login({ email, password });
      console.log("res", res);
      if (res.status === "success") {
        setToken(res.accessToken);
        setLocalEmail(email);
        navigation.push("Home", {
          reload: JSON.stringify(new Date()),
        });
        setModalVisible(!modalVisible);
      }
    }
  };

  const handleClickChangeDistribuidor = async () => {
    try {
      if (distribuidor !== "") {
        if (password && confirmPassword) {
          const data = {
            email: email,
            password: password,
            confirmation: confirmPassword,
            distribuidor: distribuidor,
          };
          const response = await ChangePassword(data);
          if (response?.status === "success") {
            functionOnPress();
          } else if (response?.status === "error") {
            setDialog({
              visible: true,
              message: response.message,
              type: "error",
            });
          }
        } else {
          const data = {
            email: email,
            distribuidor: distribuidor,
          };
          const response = await ChangeDistribuidor(data);
          if (response?.status === "success") {
            functionOnPress();
          } else if (response?.status === "error") {
            setDialog({
              visible: true,
              message: response.message,
              type: "error",
            });
          }
        }
      } else {
        setDialog({
          visible: true,
          message: "Tiene campos vacios, por favor verifique",
          type: "warning",
        });
      }
    } catch (error) {}
  };

  const cancelChangeDistribuidor = () => {
    setModalVisible(false);
    setDistribuidor("");
  };

  return (
    <View style={styles.container}>
      <When
        condition={!isLoadingDelivery || !isLoadingPassword || !isLoadingLogin}
      >
        <ScrollView>
          <Text style={{ fontSize: 20, marginTop: 20 }}>
            Selecciona el distribuidor con el que realizas compras:{" "}
          </Text>
          <Text style={{ fontSize: 14, marginTop: 5, marginBottom: 20 }}>
            *Recuerda que solo ganarás puntos por registrar facturas del
            distribuidor que elijas y no podrás cambiarlo en 60 días.
          </Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <RadioButton
              value="Bracket"
              status={distribuidor === "Bracket" ? "checked" : "unchecked"}
              onPress={() => setDistribuidor("Bracket")}
            />
            <Text>Bracket</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Aldental"
              status={distribuidor === "Aldental" ? "checked" : "unchecked"}
              onPress={() => setDistribuidor("Aldental")}
            />
            <Text>Aldental</Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <RadioButton
              value="Vertice Aliados"
              status={
                distribuidor === "Vertice Aliados" ? "checked" : "unchecked"
              }
              onPress={() => setDistribuidor("Vertice Aliados")}
            />
            <Text>Vertice Aliados</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Dental Palermo"
              status={
                distribuidor === "Dental Palermo" ? "checked" : "unchecked"
              }
              onPress={() => setDistribuidor("Dental Palermo")}
            />
            <Text>Dental Palermo</Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <RadioButton
              value="Dentales Antioquía"
              status={
                distribuidor === "Dentales Antioquía" ? "checked" : "unchecked"
              }
              onPress={() => setDistribuidor("Dentales Antioquía")}
            />
            <Text>Dentales Antioquía</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Casa Dental Gabriel Velasquez"
              status={
                distribuidor === "Casa Dental Gabriel Velasquez"
                  ? "checked"
                  : "unchecked"
              }
              onPress={() => setDistribuidor("Casa Dental Gabriel Velasquez")}
            />
            <Text>Casa Dental Gabriel Velasquez</Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <RadioButton
              value="Dentales Padilla"
              status={
                distribuidor === "Dentales Padilla" ? "checked" : "unchecked"
              }
              onPress={() => setDistribuidor("Dentales Padilla")}
            />
            <Text>Dentales Padilla</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Orbidental"
              status={distribuidor === "Orbidental" ? "checked" : "unchecked"}
              onPress={() => setDistribuidor("Orbidental")}
            />
            <Text>Orbidental</Text>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <RadioButton
              value="Dentales y Acrilicos"
              status={
                distribuidor === "Dentales y Acrilicos"
                  ? "checked"
                  : "unchecked"
              }
              onPress={() => setDistribuidor("Dentales y Acrilicos")}
            />
            <Text>Dentales y Acrilicos</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Dental 83"
              status={distribuidor === "Dental 83" ? "checked" : "unchecked"}
              onPress={() => setDistribuidor("Dental 83")}
            />
            <Text>Dental 83</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Janer Distribuciones"
              status={
                distribuidor === "Janer Distribuciones"
                  ? "checked"
                  : "unchecked"
              }
              onPress={() => setDistribuidor("Janer Distribuciones")}
            />
            <Text>Janer Distribuciones</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Janer Distribuciones"
              status={
                distribuidor === "Janer Distribuciones"
                  ? "checked"
                  : "unchecked"
              }
              onPress={() => setDistribuidor("Janer Distribuciones")}
            />
            <Text>Janer Distribuciones</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Dental Nader"
              status={distribuidor === "Dental Nader" ? "checked" : "unchecked"}
              onPress={() => setDistribuidor("Dental Nader")}
            />
            <Text>Dental Nader</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Casa Odontológica"
              status={
                distribuidor === "Casa Odontológica" ? "checked" : "unchecked"
              }
              onPress={() => setDistribuidor("Casa Odontológica")}
            />
            <Text>Casa Odontológica</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Dentales Market"
              status={
                distribuidor === "Dentales Market" ? "checked" : "unchecked"
              }
              onPress={() => setDistribuidor("Dentales Market")}
            />
            <Text>Dentales Market</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginVertical: 10,
            }}
          >
            <RadioButton
              value="Adental"
              status={distribuidor === "Adental" ? "checked" : "unchecked"}
              onPress={() => setDistribuidor("Adental")}
            />
            <Text>Adental</Text>
          </View>
          <Button
            onPress={() => setModalVisible(true)}
            style={{
              backgroundColor: "#64C2C8",
              marginTop: 30,
              alignSelf: "center",
              paddingHorizontal: 75,
            }}
          >
            <Text style={{ color: "white", fontSize: 20, textAlign: "center" }}>
              Registrarse
            </Text>
          </Button>
          <View style={styles.centeredView}>
            <Modal
              animationType="fade"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                setModalVisible(!modalVisible);
              }}
            >
              <View style={styles.centeredView}>
                <View
                  style={{
                    width: "90%",
                    height: 250,
                    backgroundColor: "white",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    borderRadius: 20,
                  }}
                >
                  <Icon
                    name="check-circle"
                    type="FontAwesome5"
                    style={{ color: "#64C2C8", marginBottom: 20, fontSize: 50 }}
                  />
                  <Text
                    style={{
                      fontSize: 20,
                      marginBottom: "15%",
                      marginHorizontal: 30,
                      textAlign: "center",
                    }}
                  >
                    {" "}
                    ¿Seguro quieres a {distribuidor} como tu distribuidor para
                    ganar puntos?
                  </Text>
                  <View style={{ flexDirection: "row", width: "100%" }}>
                    <Button
                      onPress={handleClickChangeDistribuidor}
                      style={{
                        backgroundColor: isLoadingPassword ? "gray" : "#64C2C8",
                        justifyContent: "center",
                        borderBottomStartRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "50%",
                      }}
                      disabled={isLoadingPassword}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: isLoadingPassword ? "black" : "white",
                        }}
                      >
                        SI
                      </Text>
                    </Button>
                    <Button
                      onPress={cancelChangeDistribuidor}
                      style={{
                        backgroundColor: "#E7344C",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomRightRadius: 20,
                        width: "50%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        NO
                      </Text>
                    </Button>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </ScrollView>
      </When>
      <Unless
        condition={!isLoadingDelivery || !isLoadingPassword || !isLoadingLogin}
      >
        <Spinner color="red" style={styles.spinner} />
      </Unless>
      <ModalComponent onClose={setDialog} {...dialog} />
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    //flex: 1,
    width: "90%",
    alignSelf: "center",
  },
  box1: {
    width: "100%",
    height: 150,
    paddingTop: 30,
  },
  box2: {
    width: "100%",
    height: 150,
    paddingTop: 30,
  },
  box3: {
    width: "100%",
    height: 200,
    paddingTop: 30,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});
