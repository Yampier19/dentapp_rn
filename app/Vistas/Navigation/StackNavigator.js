import React, { useState, useEffect, useCallback, Component } from "react";
import { useProfile } from "../../services/dentapp/user/useUser";
import { resolveImage, getToken } from "../../utils/utils";
import {
  TouchableHighlight,
  Text,
  BackHandler,
  StyleSheet,
} from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  createStackNavigator,
  HeaderBackButton,
} from "@react-navigation/stack";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from "@react-navigation/drawer";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { IconButton, Colors } from "react-native-paper";
import { Image, Button } from "react-native";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import PushNotificacion from "./../../Components/PushNotificacion";
import StackDistribuidor from "./../../Components/Distribuidores/Stack";

{
  /* PANTALLAS DE INICIO */
}
import SplashScreen from "@Vistas/Inicio/SplashScreen";
import TutorialScreen from "@Vistas/Tutorial/TutorialScreen";
import WelcomeScreen from "@Vistas/Inicio/WelcomeScreen";
import LoginScreen from "@Vistas/Inicio/LoginScreen";
import RegisterScreen from "@Vistas/Inicio/RegisterScreen";
import VerifyScreen from "@Vistas/Inicio/VerifyScreen";
import HelpScreen from "@Vistas/Ayuda/HelpScreen";
import DistribuidorScreen from "@Vistas/Forgot/DistribuidorScreen";
import OlvidoUsuarioScreen from "@Vistas/Forgot/OlvidoUsuarioScreen";
import OlvidoPassScreen from "@Vistas/Forgot/OlvidoPassScreen";
import CrearPassScreen from "@Vistas/Forgot/CrearPassScreen";
import PassNewScreen from "@Vistas/Forgot/PassNewScreen";

{
  /* PANTALLAS PRINCIPALES */
}
import HomeScreen from "@Vistas/Portal/HomeScreen";
import DetallesBanner from "@Vistas/Portal/DetallesBanner";
import ItemScreen from "@Vistas/Portal/ItemScreen";
import CapacitacionScreen from "@Vistas/Capacitacion/CapacitacionScreen";
import NotificacionScreen from "@Vistas/Notificacion/NotificacionScreen";
import PerfilScreen from "@Vistas/Perfil/PerfilScreen";

{
  /* AYUDA */
}
import OneScreen from "@Vistas/Ayuda/subPages/OneScreen";
import TwoScreen from "@Vistas/Ayuda/subPages/TwoScreen";
import ThreeScreen from "@Vistas/Ayuda/subPages/ThreeScreen";
import FourScreen from "@Vistas/Ayuda/subPages/FourScreen";
import FiveScreen from "@Vistas/Ayuda/subPages/FiveScreen";
import SixScreen from "@Vistas/Ayuda/subPages/SixScreen";

{
  /* NOTICIAS */
}
import NoticiasScreen from "@Vistas/Noticias/NoticiasScreen";
import DetalleNoticiaScreen from "@Vistas/Noticias/DetalleNoticiaScreen";

{
  /* CAPACITACIONES */
}
import SelectCapacitacionScreen from "@Vistas/Capacitacion/SelectCapacitacionScreen";
import EvaluacionScreen from "@Vistas/Capacitacion/EvaluacionScreen";
import InicioCapacitacionScreen from "@Vistas/Capacitacion/InicioCapacitacionScreen";
import ResultadoScreen from "@Vistas/Capacitacion/ResultadoScreen";

{
  /* PREMIOS */
}
import PremioOfertaScreen from "@Vistas/Premio/Categorias/PremioOfertaScreen";
import PremioSelectScreen from "@Vistas/Premio/PremioSelectScreen";
import CanjearDomicilioScreen from "@Vistas/Premio/CanjearDomicilioScreen";
import CanjearOnlineScreen from "@Vistas/Premio/CanjearOnlineScreen";
import DetallesDomicilioScreen from "@Vistas/Premio/DetallesDomicilioScreen";
import DetallesOnlineScreen from "@Vistas/Premio/DetallesOnlineScreen";
import CanjearQuantumScreen from "@Vistas/Premio/CanjearQuantumScreen";

{
  /* PERFIL */
}
import DatosPerfilScreen from "@Vistas/Perfil/DatosPerfilScreen";
import ModificarPassScreen from "@Vistas/Perfil/ModificarPassScreen";
import PrivateScreen from "@Vistas/Perfil/PrivateScreen";

{
  /* NIVEL */
}
import NivelScreen from "@Vistas/Niveles/NivelScreen";
import ItemNivelScreen from "@Vistas/Niveles/ItemNivelScreen";

{
  /* FACTURA */
}
import FacturaScreen from "@Vistas/Facturas/FacturaScreen";
import CargandoFacturaScreen from "@Vistas/Facturas/CargandoFacturaScreen";
import FacturaConfirmadaScreen from "@Vistas/Facturas/FacturaConfirmadaScreen";
import FacturaPendienteScreen from "@Vistas/Facturas/FacturaPendienteScreen";
import FacturaRechazadaScreen from "@Vistas/Facturas/FacturaRechazadaScreen";
import FacturaAceptadaScreen from "@Vistas/Facturas/FacturaAceptadaScreen";
import HistorialFacturaScreen from "@Vistas/Perfil/HistorialFacturaScreen";
import HistorialPremioScreen from "@Vistas/Perfil/HistorialPremioScreen";
import EstadoFacturaScreen from "@Vistas/Perfil/EstadoFacturaScreen";
import CameraScreen from "@Vistas/Facturas/CameraFacturaSreen";
import PreviewFacturaScreen from "@Vistas/Facturas/PreviewFacturaScreen";
import EditFacturaScrenn from "@Vistas/Facturas/EditFacturaScreen";

{
  /* PRODUCTO */
}
import ProductScreen from "@Vistas/Product/ProductScreen";
import ProductRegisterScreen from "@Vistas/Product/ProductRegisterScreen";
import ProductHelpScreen from "@Vistas/Product/ProductHelpScreen";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function AllNavigator({ navigation }) {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{ gestureEnabled: false }}
    >
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetallesBanner"
        component={DetallesBanner}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Item"
        component={ItemScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Noticias"
        component={NoticiasScreen}
        options={{
          title: "Noticias",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="DetalleNoticia"
        component={DetalleNoticiaScreen}
        options={{
          title: "Detalles de noticias",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Facturas"
        component={FacturaScreen}
        options={{
          title: "Facturas",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="CargandoFactura"
        component={CargandoFacturaScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="FacturaConfirmada"
        component={FacturaConfirmadaScreen}
        options={{
          title: "Detalles de factura",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="FacturaPendiente"
        component={FacturaPendienteScreen}
        options={{
          title: "Estado de factura",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="FacturaRechazada"
        component={FacturaRechazadaScreen}
        options={{
          title: "Estado de factura",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="FacturaAceptada"
        component={FacturaAceptadaScreen}
        options={{
          title: "Estado de factura",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="CameraFactura"
        component={CameraScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="EditCameraFactura"
        component={EditFacturaScrenn}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="PreviewFactura"
        component={PreviewFacturaScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Product"
        component={ProductScreen}
        options={{
          title: "Listado de productos",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="ProductRegister"
        component={ProductRegisterScreen}
        options={{
          title: "Listado de productos",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="ProductHelp"
        component={ProductHelpScreen}
        options={{
          title: "Listado de productos",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="SelectCapacitacion"
        component={SelectCapacitacionScreen}
        options={{
          title: "Detalles de capacitación",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="InicioCapacitacion"
        component={InicioCapacitacionScreen}
        options={{
          title: "Iniciar capacitación",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Evaluacion"
        component={EvaluacionScreen}
        options={{
          title: "Evaluación",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Resultado"
        component={ResultadoScreen}
        options={{
          title: "Resultado",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="PremioSelect"
        component={PremioSelectScreen}
        options={{
          title: "Premio",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="CanjearDomicilio"
        component={CanjearDomicilioScreen}
        options={{
          title: "Canjear premio",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="CanjearOnline"
        component={CanjearOnlineScreen}
        options={{
          title: "Canjear premio",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="CanjearQuantum"
        component={CanjearQuantumScreen}
        options={{
          title: "Canjear premio",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="DetallesDomicilio"
        component={DetallesDomicilioScreen}
        options={{
          title: "Detalles de entrega",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="DetallesOnline"
        component={DetallesOnlineScreen}
        options={{
          title: "Detalles de entrega",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Nivel"
        component={NivelScreen}
        options={{
          title: "Niveles",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="ItemNivel"
        component={ItemNivelScreen}
        options={{
          title: "Listado de niveles",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="DatosPerfil"
        component={DatosPerfilScreen}
        options={{
          title: "Datos personales",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="ModificarPass"
        component={ModificarPassScreen}
        options={{
          title: "Modificar contraseña",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Private"
        component={PrivateScreen}
        options={{
          title: "Privacidad y seguridad de datos",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="HistorialFactura"
        component={HistorialFacturaScreen}
        options={{
          title: "Historial de facturas",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="EstadoFactura"
        component={EstadoFacturaScreen}
        options={{
          title: "Estado de factura",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="HistorialPremio"
        component={HistorialPremioScreen}
        options={{
          title: "Historial de premios",
          headerStyle: {
            backgroundColor: "#e7344c",
            height: responsiveScreenHeight(5),
          },
          backgroundColor: "#e7344c90",
          headerTitleAlign: "left",
          headerTintColor: "#fff",
          headerBackTitleVisible: null,
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
    </Stack.Navigator>
  );
}

function MainStackNavigator({ navigation, props }) {
  const { reload } = props?.route?.params || {};
  const [user, setUser] = useState(null);
  const { data: Profile } = useProfile();
  const [token, setToken] = useState(null);

  const getProfile = async () => {
    try {
      const data = Profile;

      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );

        setUser({
          ...currentData?.[0],
          password: data?.password,
          email_verified_at: data?.email_verified_at,
          message: data?.message,
        });
      } else if (data?.message === "Unauthenticated") {
        console.log("error");
      }
    } catch (error) { }
  };

  useEffect(() => {
    main();
  }, [token]);

  const main = async () => {
    getProfile();
    const localToken = await getToken();
    if (localToken) {
      setToken(localToken);
    } else {
      console.log("error");
    }
  };

  return (
    <Stack.Navigator screenOptions={{ gestureEnabled: false }}>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="TutorialScreen"
        component={TutorialScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="WelcomeScreen"
        component={WelcomeScreen}
        options={{
          headerTitle: (
            props // App Logo
          ) => (
            <Image
              style={{
                alignSelf: "center",
                width: responsiveScreenWidth(60),
                height: responsiveScreenHeight(10),
              }}
              source={require("@Public/Img/DentApp.png")}
              resizeMode="cover"
            />
          ),
          headerTitleAlign: "left",
          headerLeft: null,
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(20),
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          headerTitle: (
            props // App Logo
          ) => (
            <Image
              style={{
                alignSelf: "center",
                width: responsiveScreenWidth(60),
                height: responsiveScreenHeight(10),
              }}
              source={require("@Public/Img/DentApp.png")}
              resizeMode="cover"
            />
          ),
          headerTitleAlign: "left",
          headerLeft: null,
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(20),
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{
          title: "Crea una cuenta",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Verify"
        component={VerifyScreen}
        options={{
          title: "Registro",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />

      <Stack.Screen
        name="Help"
        component={HelpScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />

      <Stack.Screen
        name="OneScreen"
        component={OneScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="TwoScreen"
        component={TwoScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="ThreeScreen"
        component={ThreeScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="FourScreen"
        component={FourScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="FiveScreen"
        component={FiveScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="SixScreen"
        component={SixScreen}
        options={{
          title: "Ayuda",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen name="Push" component={PushNotificacion} />
      <Stack.Screen
        name="OlvidoUsuario"
        component={OlvidoUsuarioScreen}
        options={{
          title: "¿Olvidaste tu contraseña?",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="Distribuidor"
        component={DistribuidorScreen}
        options={{
          title: "Escoger un distribuidor",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="OlvidoPass"
        component={OlvidoPassScreen}
        options={{
          title: "¿Olvidaste tu contraseña?",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="CrearPass"
        component={CrearPassScreen}
        options={{
          title: "Crea una contraseña",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />
      <Stack.Screen
        name="PassNew"
        component={PassNewScreen}
        options={{
          title: "Crea una contraseña",
          headerTitleAlign: "left",
          headerStyle: {
            backgroundColor: "#E7344C",
            height: responsiveScreenHeight(12),
          },
          headerBackTitleVisible: null,
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      />

      <Stack.Screen
        name="Home"
        options={{
          headerTitle: (
            props // App Logo
          ) => <StackDistribuidor />,
          headerStyle: {
            backgroundColor: `${user?.distribuidor === "Dental 83"
                ? "#014062"
                : user?.distribuidor === "Dental Nader"
                  ? "#0079A7"
                  : user?.distribuidor === "Adental"
                    ? "#17171F"
                    : user?.distribuidor === "Bracket"
                      ? "#FFFFFF"
                      : "#E7334C"
              }`,

            height: responsiveScreenHeight(12),
          },
          headerTitleAlign: "left",
          headerLeft: null,
          gesturesEnabled: false,
          headerRight: () => (
            <IconButton
              accessible={true}
              accessibilityLabel="Menu de navegacion"
              accessibilityHint="Menu de opciones"
              icon="menu"
              color="white"
              size={responsiveScreenFontSize(4)}
              onPress={() => navigation.openDrawer()}
            />
          ),
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }}
      >
        {() => (
          <Tab.Navigator
            initialRouteName="Home"
            tabBarOptions={{
              labelStyle: {
                fontSize: responsiveScreenFontSize(1.5),
              },
              activeTintColor: "#64C2C8",
            }}
          >
            <Tab.Screen
              name="Main"
              component={AllNavigator}
              options={{
                tabBarLabel: "Inicio",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons
                    name="home"
                    color={"#64C2C8"}
                    size={30}
                  />
                ),
              }}
            />
            <Tab.Screen
              name="Capacitacion"
              component={CapacitacionScreen}
              options={{
                tabBarLabel: "Capacitación",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons name="school" color={'#64C2C8'} size={30} />
                ),
              }}
            />




            <Tab.Screen name="Facturas" component={FacturaScreen} options={{
              tabBarLabel: 'Factura',
              tabBarIcon: ({ color, size }) => (
                <Ionicons name="receipt" color={'#E7334C'} size={30} />
              ),
            }}
            />

            <Tab.Screen
              name="PremioOfertaScreen"
              component={PremioOfertaScreen}
              options={{
                tabBarLabel: "Premio",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons
                    name="wallet-giftcard"
                    color={"#64C2C8"}
                    size={30}
                  />
                ),
              }}
            />

            <Tab.Screen
              name="Perfil"
              component={PerfilScreen}
              options={{
                tabBarLabel: "Perfil",
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons
                    name="account"
                    color={"#64C2C8"}
                    size={30}
                  />
                ),
              }}
            />

          </Tab.Navigator>
        )}
      </Stack.Screen>
    </Stack.Navigator>
  );
}

export { MainStackNavigator };
