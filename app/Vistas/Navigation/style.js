export default {

    container: {
        flex: 1,
    },

    bgContainer: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#A0A0A0',
        backgroundColor: '#E7344C',
        paddingTop: 20
    },

    userContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    },

    userImagen: {
        width: 80,
        height: 80,
        borderRadius: 40
    },

    camaraContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    camaraIcon: {
        width: 20,
        height: 20,
        position: 'absolute',
        left: 15,
        bottom: 3
    },

    userNombre: {
        marginVertical: 10,
    },

    userTitulo: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 16,
        color: 'white'
    },

    userSubTitulo: {
        textAlign: 'center',
        fontSize: 11,
        color: 'white',
        paddingVertical: 5,
    },
    menuContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: 10,
        marginVertical: 10
    },

    iconoContainer: {
        flex: 1.5,
        justifyContent: 'center',
        marginBottom: 5
    },

    tituloContainer: {
        flex: 8.5,
        justifyContent: 'center'
    },

    tituloTxt: {
        fontSize: 13
    },
    difuminado: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)'
    },
    fondoImagen: {
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    }
}