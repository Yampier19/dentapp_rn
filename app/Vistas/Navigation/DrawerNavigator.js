import React, { useEffect, useState } from "react";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from "@react-navigation/drawer";
import { Icon, Spinner } from "native-base";
import {
  Text,
  TouchableOpacity,
  Image,
  View,
  ScrollView,
  Modal,
  StyleSheet,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import { When } from "react-if";
import { Unless } from "react-if";

import { resolveImage } from "../../utils/utils";
import ModalComponent from "../../Components/Modal";
import { removeToken } from "../../utils/utils";
import * as Perfil from "../Perfil/PerfilScreen";
import s from "./style";
import {
  useProfile,
  useProfileLevel,
  useChangeAvatar,
} from "../../services/dentapp/user/useUser";
import { MainStackNavigator } from "./StackNavigator";
import PushNotifications from "../../Components/PushNotificacion";
import Color from "./../../Components/Distribuidores/Color";
import LoginScreen from "@Vistas/Inicio/LoginScreen";

function DrawerMenu(props) {
  return (
    <TouchableOpacity onPress={props.navigation}>
      <View style={s.menuContainer}>
        <View style={s.iconoContainer}>
          <Icon
            as={Ionicons}
            name={props.iconName}
            size="15"
            color="#64C2C8"
            style={{ left: 5, top: 3 }}
          />
        </View>
        <View style={s.tituloContainer}>
          <Text style={s.tituloTxt}>{props.titleName}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

function Menu({ navigation, profile, state, refresh }) {
  const [user, setUser] = useState(profile);
  const [loading, setLoading] = useState(false);
  const [loadingProfile, setLoadingProfile] = useState(false);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
    goTo: "",
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [level, setLevel] = useState(null);
  const { data: Profile } = useProfile();
  const { data: Level } = useProfileLevel();
  const { mutateAsync: ChangeAvatar, isLoading } = useChangeAvatar();

  useEffect(() => {
    setLoadingProfile(true);
    getProfile();
    getLevel();
  }, [profile, state, refresh]);

  const showImagePicker = async () => {
    const permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("You've refused to allow this appp to access your photos!");
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: Platform.OS !== "ios" ? true : false,
      quality: 1,
      base64: true,
    });

    if (!result.cancelled) {
      updateAvatar(result.uri);
    }
  };

  const updateAvatar = async (image) => {
    let formData = new FormData();
    try {
      formData.append("foto", {
        uri: image,
        type: "image/jpeg",
        name: "avatar",
      });
      setLoading(true);
      const response = await ChangeAvatar(formData);
      if (response?.status === "success") {
        setDialog({
          visible: true,
          message:
            response?.message || "La imagen fue actualizada de forma correcta",
          type: "info",
        });
        getProfile();
      } else {
        setDialog({
          visible: true,
          message:
            response?.message ||
            "Ocurrió un error al momento de subir la imagen, intentelo nuevamente",
          type: "error",
        });
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const getProfile = async () => {
    try {
      const data = Profile;
      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );
        setUser({
          ...currentData?.[0],
          password: data?.password,
          message: data?.message,
        });
      } else if (data?.message === "Unauthenticated") {
        navigation.navigate("Login");
      }
      setLoading(false);
      setLoadingProfile(false);
    } catch (error) {
      setLoadingProfile(false);
    }
  };

  const getLevel = async () => {
    try {
      const data = Level.data;
      if (data?.status === "success" && data?.nivel) {
        setLevel(data.nivel);
      } else if (data?.message === "Unauthenticated") {
        navigation.navigate("Login");
      }
    } catch (error) {}
  };

  const functionOnpress = async () => {
    navigation.jumpTo("Login");
    await removeToken();
    setModalVisible(!modalVisible);
  };

  const logout = async () => {
    setModalVisible(true);
  };

  return (
    <View style={s.container}>
      <View style={{"backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83Head :
          user?.distribuidor === "Dental Nader" ? Color.DentalNaderHead :
            user?.distribuidor === "Adental" ? Color.DentalAdentalHead :
              user?.distribuidor === "Bracket" ? Color.BracketHead :
                '#E7334C'
          }`, paddingTop: 30}}>
        <Icon
          onPress={() => navigation.closeDrawer()}
          as={Ionicons}
          name={Platform.OS ? "ios-close" : "md-close"}
          size="15"
          color="white"
          style={{ top: 20, left: 10 }}
        />
        <When condition={!loadingProfile}>
          <When condition={!loading || !isLoading}>
            <View style={s.userContainer}>
              <When condition={user?.file}>
                <Image style={s.userImagen} source={{ uri: user?.file }} />
              </When>
              <Unless condition={user?.file}>
                <Image
                  style={s.userImagen}
                  source={require("@Public/Img/perfil.jpg")}
                />
              </Unless>
              <View style={s.camaraContainer}>
                <Image
                  style={s.camaraIcon}
                  source={require("@Public/Img/camera.png")}
                />
              </View>
            </View>
          </When>
          <Unless condition={!loading || !isLoading}>
            <Spinner color="white" />
          </Unless>
        </When>
        <Unless condition={!loadingProfile}>
          <Spinner color="white" />
        </Unless>
        <View style={s.userNombre}>
          <Text
            style={s.userTitulo}
          >{`${user?.nombre} ${user?.apellido}`}</Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20,
            }}
          >
            <Icon
              name="star-circle"
              type="MaterialCommunityIcons"
              style={{ fontSize: 16, marginRight: 5, color: "white" }}
            />
            <Text style={{ color: "white" }}>
              {user?.total_estrellas
                .toString()
                .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}{" "}
              puntos
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ fontWeight: "bold", color: "white" }}>Nivel: </Text>
            <Text style={{ color: "white" }}>{level?.nombre}</Text>
          </View>
        </View>
      </View>
      <ScrollView>
        <DrawerMenu
          iconName="ios-home"
          titleName="Home"
          navigation={() => navigation.navigate("Home")}
        />
        <DrawerMenu
          iconName="ios-book"
          titleName="Capacitaci&oacute;n"
          navigation={() => navigation.navigate("Capacitacion")}
        />
        <DrawerMenu
          iconName="ios-gift"
          titleName="Premios"
          navigation={() => navigation.navigate("PremioOfertaScreen")}
        />
        <DrawerMenu
          iconName="ios-notifications"
          titleName="Notificaciones"
          navigation={() => navigation.navigate("Notificacion")}
        />
        <DrawerMenu
          iconName="ios-search"
          titleName="Perfil"
          navigation={() => navigation.navigate("Perfil")}
        />
        <DrawerMenu
          iconName="ios-receipt"
          titleName="Enviar Factura"
          navigation={() => navigation.navigate("Facturas")}
        />
        <DrawerMenu
          iconName="ios-book"
          titleName="Noticias"
          navigation={() => navigation.navigate("Noticias")}
        />

        <DrawerMenu
          iconName="log-out-outline"
          titleName="Cerrar sesión"
          navigation={() => logout()}
        />

        <View style={{ marginTop: "10%" }}>
          <DrawerMenu
            iconName="help-circle"
            titleName="Ayuda"
            navigation={() => navigation.navigate("Help")}
          />
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                <Icon
                  as={Ionicons}
                  name={Platform.OS ? "ios-warning" : "md-warning"}
                  size="20"
                  color="black"
                />
                <Text
                  style={{
                    fontSize: 20,
                    marginBottom: "15%",
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  ¿Seguro que quieres cerrar sesión?
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignContent: "center",
                    justifyContent: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={functionOnpress}
                    style={{
                      backgroundColor: "#E7344C",
                      justifyContent: "center",
                      borderBottomLeftRadius: 20,
                      width: "50%",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: "bold",
                        color: "white",
                        textAlign: "center",
                      }}
                    >
                      SI
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => setModalVisible(!modalVisible)}
                    style={{
                      backgroundColor: "#64C2C8",
                      justifyContent: "center",
                      borderBottomEndRadius: 20,
                      width: "50%",
                      padding: 10,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 15,
                        fontWeight: "bold",
                        color: "white",
                        textAlign: "center",
                      }}
                    >
                      NO
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>

      <ModalComponent onClose={setDialog} {...dialog} />
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});

const CustomDrawerContent = (props) => {
  const { state, ...rest } = props;
  const newState = { ...state };
  newState.routes = newState.routes.filter((item) => item.name !== "Login");

  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList state={newState} {...rest} />
    </DrawerContentScrollView>
  );
};

const Drawer = createDrawerNavigator();

const DrawerNavigator = (props) => {
  const [profile, setProfile] = useState(null);
  const { data: Profile } = useProfile();

  useEffect(() => {
    getProfile();
  }, []);

  const getProfile = async () => {
    try {
      const data = Profile;
      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );
        setProfile({
          ...currentData?.[0],
          password: data?.password,
          message: data?.message,
        });
      } else if (data?.message === "Unauthenticated") {
        navigation.navigate("Login");
      }
    } catch (error) {}
  };

  return (
    <Drawer.Navigator
      drawerPosition="right"
      drawerStyle={{ width: "55%" }}
      screenOptions={{ swipeEnabled: false }}
      drawerContent={(props) => (
        <Menu profile={profile} refresh={new Date()} {...props} />
      )}
    >
      <Drawer.Screen name="Config" component={MainStackNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
