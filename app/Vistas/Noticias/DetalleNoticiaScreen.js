import React, { useEffect, useState } from "react";
import { View, StyleSheet, Modal, Text, TouchableOpacity,Alert } from "react-native";
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen';
import { Spinner } from "native-base";
import { When } from "react-if";
import { Unless } from "react-if";
import { Icon, Button } from 'native-base';

import ViewFile from '../../Components/ViewFile';
import { resolveImage } from '../../utils/utils';
import { useAnnouncementFindOne } from "../../services/dentapp/announcement/useAnnouncement";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize
} from "react-native-responsive-dimensions";
import * as Print from "expo-print";
import { not } from "react-native-reanimated";

export default function DetalleNoticiaScreen({ route, navigation, props }) {
  const { noticia } = route.params || {};
  const [news, setNews] = useState(null);
  const [loading, setLoading] = useState(false);
  const { data: Announcement, isLoading } = useAnnouncementFindOne(noticia?.id_noticia);
  const [modalVisible, setModalVisible] = useState(true);

  useEffect(() => {
    if (noticia) {
      setLoading(true);
      getNoticiaById(noticia?.id_noticia)
    }
  }, [noticia?.id_noticia, isLoading])

  const view = (document) => {
  
    var newUri = document.slice(53)
    if(document===undefined){
       Alert.alert('Noticia no encontrada');
    } else {
       Print.printAsync({
        uri: `https://dentapp.panelrp.com/noticia/getDocument/${newUri}`,
      }).then(navigation.navigate("Home"));
    }
    
  };
  
  const getNoticiaById = async () => {
    try {
      setLoading(true);
      const data = Announcement;
      if (data?.noticia) {
        const currentData = await resolveImage([data.noticia], 'noticias/getDocument', 'documento', 'noticias/', true);
        let document =currentData[0]?.file;
        view(document);
        setNews(currentData[0]);

      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', textAlign: "center" }}>
      
      <Unless condition={!loading}>
        <Spinner color="red" />
      </Unless>
     
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  box_content: {
    backgroundColor: 'white',
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(35),
    borderRadius: 10,
    alignItems: 'center'
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignItems: 'center',
    justifyContent: 'center',
    height: responsiveScreenHeight(12)
  },
  icon: {
    color: '#64C2C8',
    fontSize: responsiveScreenFontSize(8)
  },
  box_text: {
    width: responsiveScreenWidth(90),
    alignItems: 'center',
    justifyContent: 'center',
    height: responsiveScreenHeight(15),
    marginTop: 50
  },
  text: {
    fontSize: responsiveScreenFontSize(3),
    marginHorizontal: responsiveScreenWidth(5),
    textAlign: 'center',
    marginBottom: 25
  },
  box_buttom: {
    width: responsiveScreenWidth(90),
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginTop: 25
  },
  buttom: {
    backgroundColor: '#64C2C8',
    width: responsiveScreenWidth(50),
    height: responsiveScreenHeight(6),
    justifyContent: 'center',
    borderRadius: 30
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: 'center',
    color: 'white'
  }
});