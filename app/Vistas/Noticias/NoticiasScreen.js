import React, { Component } from 'react'
import { Text, View, Image, ScrollView, StyleSheet, TouchableOpacity } from 'react-native'
import { Card, CardItem, Button, Icon, Body, Segment, Spinner } from 'native-base';
import { Request } from '../../api/api';
import { resolveImage } from '../../utils/utils';
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize,
  } from "react-native-responsive-dimensions";


export default class NoticiasScreen extends Component {

    state = { user: '' };
    updateUser = user => {
        this.setState({
            user: user,
            ortodoncia: [],
            dental: []
        });
    };



    state = {
        activePage: 1,
        loading: false
    }

    componentDidMount() {
        this.getNews();
    }

    filterData(data) {
        return {
            dental: data.filter(item => item.tipo === 'Dental'),
            ortodoncia: data.filter(item => item.tipo === 'Ortodoncia')
        }
    }

    async getNews() {
        try {
            this.setState({loading: true})
            const response = await Request('GET', 'noticias/list/10');
            const data = await response.json();
            if (data?.status === 'success' && data?.noticias.length > 0) {
                const currentData = await resolveImage(data?.noticias, 'noticias/getImage', 'imagen', 'noticias/', true);
                const resolveData = this.filterData(currentData)

                this.setState({
                    ortodoncia: resolveData.ortodoncia,
                    dental: resolveData.dental,
                });
                this.setState({loading: false})
            } else {
                this.setState({loading: false})
            }
        } catch (error) {
            this.setState({loading: trfalseue})
        }
    };

    selectComponent = (activePage) => () => this.setState({ activePage })

    _renderComponent = () => {
        if (this.state.activePage === 1)
            return (
                <View style={{ width: responsiveScreenWidth(90), marginVertical: responsiveScreenHeight(1) }}>
                {
                this.state?.ortodoncia?.length > 0 && this.state?.ortodoncia?.map((noticia, index) => (
                   
                   <TouchableOpacity onPress={() => this.props.navigation.navigate('DetalleNoticia', {noticia})} style={{alignItems: 'center'}}>
                        <View style={{width: responsiveScreenWidth(58), height: responsiveScreenHeight(43), marginBottom: responsiveScreenHeight(2)}}  key={`${index}-${noticia?.nombre}`}>
                        <View style={{width: responsiveScreenWidth(58), height: responsiveScreenHeight(35)}}>
                            <Image  source={{uri: noticia.file}} style={{ width: responsiveScreenWidth(58), height: responsiveScreenHeight(35)}} />
                            </View> 
                            <View style={{flexDirection: 'row', width: responsiveScreenWidth(58), height: responsiveScreenHeight(8), backgroundColor: '#E8E8E8', alignItems: 'center', borderBottomEndRadius: 10, borderBottomStartRadius: 10}}>
                                <View style={{width: responsiveScreenWidth(5), marginHorizontal: responsiveScreenWidth(2)}}>
                                    <Icon name="circle" type="FontAwesome" style={{ fontSize: responsiveScreenFontSize(2), borderRadius: 40, color: '#64C2C8' }} />
                                </View>
                                <View style={{width: responsiveScreenWidth(50)}}>
                                    <Text style={{fontSize: responsiveScreenFontSize(1.5)}}>
                                        {noticia?.nombre}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                    

                  
                ))
                      }
                </View>
            ) //... Your Component 1 to display
        else
            return (
                <View style={{ width: responsiveScreenWidth(90), marginVertical: responsiveScreenHeight(1) }}>
                {
           this.state?.dental?.length > 0 && this.state?.dental?.map((noticia, index) => (
             
            <TouchableOpacity onPress={() => this.props.navigation.navigate('DetalleNoticia', {noticia})} style={{alignItems: 'center'}}>
                <View style={{width: responsiveScreenWidth(58), height: responsiveScreenHeight(43), marginBottom: responsiveScreenHeight(2)}}  key={`${index}-${noticia?.nombre}`}>
                    <View style={{width: responsiveScreenWidth(58), height: responsiveScreenHeight(35)}}>
                        <Image  source={{uri: noticia.file}} style={{ width: responsiveScreenWidth(58), height: responsiveScreenHeight(35)}} />
                        </View> 
                        <View style={{flexDirection: 'row', width: responsiveScreenWidth(58), height: responsiveScreenHeight(8), backgroundColor: '#E8E8E8', alignItems: 'center', borderBottomEndRadius: 10, borderBottomStartRadius: 10}}>
                            <View style={{width: responsiveScreenWidth(5), marginHorizontal: responsiveScreenWidth(2)}}>
                                <Icon name="circle" type="FontAwesome" style={{ fontSize: responsiveScreenFontSize(2), borderRadius: 40, color: '#5FD653' }} />
                            </View>
                            <View style={{width: responsiveScreenWidth(48)}}>
                                <Text style={{fontSize: responsiveScreenFontSize(1.5)}}>
                                    {noticia?.nombre}
                                </Text>
                            </View>
                        </View>
                    </View>
            </TouchableOpacity>
                    ))
                    }
            </View>

            ) //... Your Component 1 to display
    }



    render() {

        return (
            <View style={{flex: 1, backgroundColor: 'white', alignItems: 'center'}}>

                <ScrollView >

                    <View style={{width: responsiveScreenWidth(90), height: responsiveScreenHeight(10), justifyContent: 'center', flexDirection: 'row', marginTop: 20}}>

                                    <TouchableOpacity active={this.state.activePage === 1} onPress={this.selectComponent(1)} style={{ backgroundColor: this.state.activePage === 1 ? "#40C5D2" : "#f5f5f5", width: responsiveScreenWidth(42), height: responsiveScreenHeight(4), borderRadius: 20, justifyContent: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontSize: responsiveScreenFontSize(2), color: this.state.activePage === 1 ? "#fff" : "#000" }}>ORTODONCIA</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity active={this.state.activePage === 2} onPress={this.selectComponent(2)} style={{ backgroundColor: this.state.activePage === 2 ? "#5FD653" : "#f5f5f5", width: responsiveScreenWidth(42), height: responsiveScreenHeight(4), borderRadius: 20, justifyContent: 'center'}}>
                                        <Text style={{textAlign: 'center', fontSize: responsiveScreenFontSize(2), color: this.state.activePage === 2 ? "#fff" : "#000" }}>DENTAL</Text>
                                    </TouchableOpacity>


                        </View>


                        {this.state.loading ? (<Spinner color="red" style={styles.spinner} />) : this._renderComponent()}


                </ScrollView>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        height: '100%'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '100%',
        height: 50,
        marginBottom: 10,

    },
    box2: {
        width: '100%',
        height: 50,
        marginTop: '10%'

    },

    box7: {
        width: '100%',
        height: 200,
        top: '5%'
    }
});