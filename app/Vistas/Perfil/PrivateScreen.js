import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Modal
} from "react-native";
import { Icon, Button, Spinner } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
  useResponsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { List } from 'react-native-paper'
import { When, Unless } from "react-if";
import ModalComponent from "../../Components/Modal";
import { useUpdateProfileDisabled } from "../../services/dentapp/user/useUser";
import { removeToken } from "../../utils/utils";
export default function PrivateScreen({route, navigation}) {
  const [modalVisible, setModalVisible] = useState(false);

  const { activation } = route.params || {};
  const { mutateAsync: UpdateProfileDisabled, isLoading } = useUpdateProfileDisabled();
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
    goTo: "",
  });
  const [errorEmptyForm, setErrorEmptyForm] = useState(false);
  const [activacion, setActivacion] = useState({
    estado: activation?.login.estado || "pendiente"
  });

  const onGuardar = async () => {

    if (!errorEmptyForm) {
      try {
        const data = await UpdateProfileDisabled(activacion);
        if (data?.status === "success") {
          setDialog({
            visible: true,
            message: "Tus datos personales han sido modificados.",
            type: "info",
            fc: () =>
              navigation.navigate("Perfil", {
                refresh: JSON.stringify(new Date()),
                reload: true,
              }),
          });
        } 
        else {
          setDialog({
            visible: true,
            message:
              data.message ||
              "Ocurrió un error al procesar los datos, vuelva a intentarlo.",
            type: "error",
          });
        }
      } catch (error) { }
    } else {
      setDialog({
        visible: true,
        message: "Tiene campos vacios, por favor verifique.",
        type: "warning",
      });
    }

    await removeToken();
    navigation.push("Login");
  };

  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
       <When condition={!isLoading}>
      <ScrollView
      >
        <View style={{ width: responsiveScreenWidth(100), marginVertical: 10 }}>

          <View style={{ width: responsiveScreenWidth(90) }}>
            <List.Section>
              <List.Subheader>
                <Text style={{ fontWeight: 'bold', fontSize: useResponsiveScreenFontSize(2) }}>Politica de datos</Text>

              </List.Subheader>
              <List.Item style={{ padding: 0 }}
                title="Terminos y condiciones"
                left={() => <List.Icon color="#65C1C7" icon="receipt" />}
              />
            </List.Section>
            <List.Section>
              <List.Subheader>
                <Text style={{ fontWeight: 'bold', fontSize: useResponsiveScreenFontSize(2) }}>Mi cuenta</Text>

              </List.Subheader>

              <List.Item style={{ padding: 0 }} onPress={() => setModalVisible(true)}
                title="Eliminar permanentemente"
                left={() => <List.Icon color="#E7334C" icon="delete" />}
              />

            </List.Section>
          </View>

        </View>
      </ScrollView>
      </When>
      <Unless condition={!isLoading}>
        <Spinner color="red" style={styles.spinner} />
      </Unless>
      <ModalComponent onClose={setDialog} {...dialog} />
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible} >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
            <View style={{ width: responsiveScreenWidth(80), alignItems: 'flex-end' }}>
              <Icon onPress={() => setModalVisible(!modalVisible)}
                as={Ionicons}
                name={Platform.OS ? 'ios-close' : 'md-close'}
                size={15}
                color="red"
              />
            </View>
              <View style={{ width: responsiveScreenWidth(90), alignItems: 'center', paddingHorizontal: 30 }}>
                <Icon
                  as={Ionicons}
                  name={Platform.OS ? 'ios-warning' : 'md-warning'}
                  size="20"
                  style={{color: '#E7334C'}}
                />
                <Text style={{fontWeight: 'bold', textAlign: 'center', fontSize: responsiveScreenFontSize(3)}}>¿Estás seguro de eliminar tu cuenta?</Text>
              </View>
              <View style={{ width: responsiveScreenWidth(90), alignItems: 'center', marginVertical: 20, paddingHorizontal: 20 }}>
              <Text style={{fontSize: responsiveScreenFontSize(2), textAlign: 'center', marginBottom: 10}}>Recuerda que una vez confirmes esta acción tus datos personales serán eliminados permanentemente</Text>
              </View>
              <View style={{ width: responsiveScreenWidth(90), alignItems: 'center'}}>
                  <Button style={{backgroundColor: '#E7334C', padding: 10, borderRadius: 20}} onPress={onGuardar}>
                    <Text style={{color: 'white', fontWeight: 'bold'}}>Eliminar mi cuenta</Text>
                  </Button>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  modalView: {
    backgroundColor: 'white',
    paddingVertical: 30, 
    borderRadius: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
})
