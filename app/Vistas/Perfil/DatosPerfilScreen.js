import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  Modal,
  Alert,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import {
  Right,
  Item,
  Input,
  Form,
  Label,
  Icon,
  Button,
  Spinner,
} from "native-base";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { TextInput } from "react-native-paper";
import { When, Unless } from "react-if";
import PickerEspecializacion from "./PickerEspecializacion";
import s from "@Public/Css/style";
import ModalComponent from "../../Components/Modal";
import { useUpdateProfile } from "../../services/dentapp/user/useUser";

export default function DatosPerfilScreen({ route, navigation }) {
  const { profile } = route.params || {};
  const { mutateAsync: UpdateProfile, isLoading } = useUpdateProfile();

  const [perfil, setPerfil] = useState({
    nombre: profile?.nombre || "",
    apellido: profile?.apellido || "",
    direccion: profile?.direccion || "",
    distribuidor: profile?.distribuidor || "",
    telefono: profile?.telefono || "",
    especialidad: profile?.especialidad || "",
    nombre_c: profile?.consultorio?.nombre || "",
    direccion_c: profile?.consultorio?.direccion || "",
    telefono_c: profile?.consultorio?.telefono || "",
    numero_pacientes: `${profile?.consultorio?.numero_pacientes || ""}`,
    consultorio: true,
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
    goTo: "",
  });
  const [errorEmptyForm, setErrorEmptyForm] = useState(false);

  const functionOnpress = () => {
    props.navigation.navigate("Perfil");
    setModalVisible(!modalVisible);
  };

  const onGuardar = async () => {
    if (!errorEmptyForm) {
      try {
        const data = await UpdateProfile(perfil);
        if (data?.status === "success") {
          setDialog({
            visible: true,
            message: "Tus datos personales han sido modificados.",
            type: "info",
            fc: () =>
              navigation.navigate("Perfil", {
                refresh: JSON.stringify(new Date()),
                reload: true,
              }),
          });
        } else {
          setDialog({
            visible: true,
            message:
              data.message ||
              "Ocurrió un error al procesar los datos, vuelva a intentarlo.",
            type: "error",
          });
        }
      } catch (error) { }
    } else {
      setDialog({
        visible: true,
        message: "Tiene campos vacios, por favor verifique.",
        type: "warning",
      });
    }
  };

  const validField = () => {
    let error = 0;
    Object.entries(perfil).forEach((value) => {
      if (value[1] === "" || value[1] === null || value[1] === undefined) {
        error++;
      } else {
        error;
      }
    });
    if (error > 0) {
      setErrorEmptyForm(true);
    } else {
      setErrorEmptyForm(false);
    }
  }

  useEffect(() => {
    validField();
  }, [perfil])

  return (
    <View style={s.container}>
      <When condition={!isLoading}>
        <ScrollView>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              marginTop: responsiveScreenHeight(3),
              alignItems: 'flex-end'
            }}
          >
            <Text style={{fontWeight: 'bold', color: 'gray', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Mis datos</Text>
          </View>
        <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Nombre: </Text>
            <TextInput
            style={register.input}
               placeholder="Ingresa nombre"
               onChangeText={(text) =>
                 setPerfil({ ...perfil, nombre: text })
               }
               value={perfil?.nombre}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Apellido: </Text>
            <TextInput
            style={register.input}
            placeholder="Ingresa apellido"
            onChangeText={(text) =>
              setPerfil({ ...perfil, apellido: text })
            }
            value={perfil?.apellido}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Telefono: </Text>
            <TextInput
            style={register.input}
            keyboardType="numeric"
                    placeholder="777-77-77"
                    onChangeText={(text) =>
                      setPerfil({ ...perfil, telefono: text })
                    }
                    value={perfil?.telefono}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Especializacion: </Text>
            <PickerEspecializacion
                  onChange={(text) =>
                    setPerfil({ ...perfil, especialidad: text })
                  }
                  selected2={perfil?.especialidad}
                />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Numero de pacientes que atiendes: </Text>
            <TextInput
            style={register.input}
            keyboardType="numeric"
            placeholder="Ingrese un valor"
            onChangeText={(text) =>
              setPerfil({ ...perfil, numero_pacientes: text })
            }
            value={perfil?.numero_pacientes || ""}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              marginTop: responsiveScreenHeight(4),
              alignItems: 'flex-end'
            }}
          >
            <Text style={{fontWeight: 'bold', color: 'gray', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Mi distribuidor</Text>
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Nombre del distribuidor: </Text>
            <TextInput
            style={register.input}
            editable={false}
            placeholder="No existe distribuidor"
            value={perfil?.distribuidor}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              marginTop: responsiveScreenHeight(4),
              alignItems: 'flex-end'
            }}
          >
            <Text style={{fontWeight: 'bold', color: 'gray', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Mi consultorio</Text>
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Nombre del consultorio: </Text>
            <TextInput
            style={register.input}
            placeholder="Ingresa el nombre del consultorio"
            onChangeText={(text) =>
              setPerfil({ ...perfil, nombre_c: text })
            }
            value={perfil?.nombre_c}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Telefono: </Text>
            <TextInput
            style={register.input}
            keyboardType="numeric"
            placeholder="777-77-77"
            onChangeText={(text) =>
              setPerfil({ ...perfil, telefono_c: text })
            }
            value={perfil?.telefono_c}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Direccion del consultorio: </Text>
            <TextInput
            style={register.input}
            placeholder="Cra 68 a # 56 - 28 Sur"
                    onChangeText={(text) =>
                      setPerfil({ ...perfil, direccion_c: text })
                    }
                    value={perfil?.direccion_c}
            />
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              marginTop: responsiveScreenHeight(4),
              alignItems: 'flex-end'
            }}
          >
            <Text style={{fontWeight: 'bold', color: 'gray', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Seguridad</Text>
          </View>
          <View
            style={{
              alignSelf: "center",
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(3),
              marginBottom: 10,
            }}
          >
            <Text style={{fontWeight: 'bold', marginBottom: 10, fontSize: responsiveScreenFontSize(2)}}>Contraseña actual </Text>
            <TextInput
            style={register.input}
            editable={false}
            placeholder="*********"
            />
          </View>
          <TouchableOpacity
                onPress={() =>
                  navigation.navigate("ModificarPass", { profile })
                }
              >
                <Text
                  style={{
                    fontSize: responsiveScreenFontSize(2),
                    textAlign: "right",
                    color: "black",
                    marginTop: 22,
                    marginRight: 20,
                    textDecorationLine: "underline",
                    fontWeight: 'bold'
                  }}
                >
                  Editar contraseña
                </Text>
              </TouchableOpacity>
              <Button
                onPress={onGuardar}
                style={{
                  backgroundColor: "#64C2C8",
                  width: 200,
                  justifyContent: "center",
                  alignSelf: "center",
                  marginTop: 60,
                }}
              >
                <Text
                  style={{ color: "white", fontWeight: "bold", fontSize: 18 }}
                >
                  Guardar
                </Text>
              </Button>
              <View style={styles.centeredView}>
                <Modal
                  animationType="fade"
                  transparent={true}
                  visible={modalVisible}
                  onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                  }}
                >
                <View style={styles.centeredView}>
                  <View
                    style={{
                      width: "90%",
                      height: 250,
                      backgroundColor: "white",
                      justifyContent: "flex-end",
                      alignItems: "center",
                      borderRadius: 20,
                    }}
                  >
                    <Icon
                      name="check-circle"
                      type="FontAwesome5"
                      style={{
                        color: "#64C2C8",
                        marginBottom: 20,
                        fontSize: 50,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 20,
                        marginBottom: "15%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Tus datos personales han sido modificados
                    </Text>

                    <Button
                      onPress={functionOnpress}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        CERRAR
                      </Text>
                    </Button>
                  </View>
                </View>
              </Modal>
            </View>
        </ScrollView>
      </When>
      <Unless condition={!isLoading}>
        <Spinner color="red" style={styles.spinner} />
      </Unless>
      <ModalComponent onClose={setDialog} {...dialog} />
    </View >
  );
}

const register = StyleSheet.create({
  input: {
    backgroundColor: "white",
    borderWidth: 2,
    borderColor: "#D9D9D9",
    height: 50,
    width: responsiveScreenWidth(90),
  },
});

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "90%",
    height: 50,
    marginBottom: 10,
  },
  box2: {
    width: "90%",
    height: 350,
    marginBottom: 10,
  },

  box: {
    width: "90%",
    height: 100,
    marginVertical: 20,
  },

  box3: {
    width: "90%",
    height: 300,
    marginBottom: 10,
  },
  box4: {
    width: "90%",
    height: 150,
    marginBottom: 10,
  },
  box5: {
    width: "90%",
    height: 150,
    marginBottom: 10,
  },
});
