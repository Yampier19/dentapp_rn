import React, { Component } from 'react';
import { Form, Item, Select, Icon, Label } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
export default class PickerEspecializacion extends Component {
    constructor(props) {
    super(props);
    this.state = {
      selected2: props?.selected2 || undefined,
      onChange: props.onChange
    };
  }
  onValueChange2(value) {
    this.setState({
      selected2: value
    });
    this.state.onChange(value);
  }
  render() {
    return (
           
              <Select
                mode="dropdown"
                width={responsiveScreenWidth(90)}
                height={responsiveScreenHeight(6)}
                fontSize={15}
                placeholder="Selecciona la especilidad"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}
              >
                <Select.Item label="Especialista - Odontología" value="Especialista - Odontología" />
                <Select.Item label="Especialista - Endodoncia" value="Especialista - Endodoncia" />
                <Select.Item label="Especialista - Maxilofacial" value="Especialista - Maxilofacial" />
                <Select.Item label="Especialista - Ortodoncia" value="Especialista - Ortodoncia" />
                <Select.Item label="Especialista - Rehabilitación oral" value="Especialista - Rehabilitación ora" />
                <Select.Item label="Especialista - Operatoria dental" value="Especialista - Operatoria dental" />
              </Select>
       
    );
  }
}