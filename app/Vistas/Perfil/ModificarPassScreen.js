import React, { useState, Fragment } from 'react'
import { Text, View, ScrollView, Modal, Alert, StyleSheet } from 'react-native'
import { Form, Label, Icon, Button, Spinner } from 'native-base';
import { TextInput } from 'react-native-paper';
import { When, Unless } from "react-if";
import ModalComponent from '../../Components/Modal';
import s from '@Public/Css/style'
import { useUpdatePassword } from "../../services/dentapp/user/useUser";
import { responsiveScreenFontSize } from 'react-native-responsive-dimensions';

const InputPassActual = () => {
    const [text, setText] = React.useState('');

    
    return (
      <TextInput
        placeholder="*********"
        disabled
        secureTextEntry
        right={<TextInput.Icon name="eye" />}
        style={{backgroundColor: 'white'}}
      />
    );
  };

  const InputPassNew = () => {
    const [text, setText] = React.useState('');
  
    return (
      <TextInput
        secureTextEntry
        right={<TextInput.Icon name="eye" />}
        style={{backgroundColor: 'white'}}
      />
    );
  };

  const InputPassConfirm = () => {
    const [text, setText] = React.useState('');
  
    return (
      <TextInput
        secureTextEntry
        right={<TextInput.Icon name="eye" />}
        style={{backgroundColor: 'white'}}
      />
    );
  };

export default function ModificarPassScreen({route, navigation}) {
    const {profile} = route.params || {};
    const [modalVisible, setModalVisible] = useState(false);
    const [pass, setPass] = useState({password: '', confirmation: ''});
    const [dialog, setDialog] = useState({visible: false, message: '', type: ''});
    const { mutateAsync: UpdatePassword, isLoading } = useUpdatePassword();

    functionOnpress = () => {
        navigation.navigate('Perfil')
        setModalVisible(!modalVisible)
     }

    const onUpdatePassword = async () => {
        if(pass.password){
            if(pass.confirmation){
                if(pass.password === pass.confirmation){
                    try {
                        const data = await UpdatePassword(pass)
                        if(data?.status === 'success'){
                            setDialog({
                                visible: true,
                                message: 'Tu contraseña ha sido modificada',
                                type: 'info',
                                fc: () => navigation.navigate('Perfil', {refresh: JSON.stringify(new Date())})
                            });
                        } else {
                            setDialog({
                                visible: true,
                                message: data.message || 'Ocurrió un error al procesar los datos, vuelva a intentarlo.',
                                type: 'error'
                            });
                        }
                    } catch (error) {
                    }
                } else {
                    setDialog({
                        visible: true,
                        message: 'Las contraseñas no son iguales, por favor verifique',
                        type: 'warning'
                    });
                }
            } else {
                setDialog({
                    visible: true,
                    message: 'Por favor confirma la nueva contraseña',
                    type: 'warning'
                });
            }
        } else {
            setDialog({
                visible: true,
                message: 'Por favor ingresa la nueva contraseña',
                type: 'warning'
            });
        }
    };

    return (
        <Fragment>
            <When condition={!isLoading}>
                <View style={s.container}>
                    <ScrollView >
                        <View style={[styles.content]}>
                            <View style={[styles.box1]}>
                                <Text style={{ alignSelf: 'flex-end', color: 'gray', marginTop: 20, fontWeight: 'bold' }}>
                                    Modificar contraseña
                                </Text>
                            </View>
                            <View style={[styles.box2]}>
                                    <Text style={{color: 'gray'}}>Contraseña actual</Text>
                                    <InputPassActual />

                                    <Text style={{color: 'gray', marginTop: 50}}>Nueva contraseña</Text>
                                    {/* <InputPassNew />*/}
                                    <TextInput
                                        value={pass.password}
                                        onChangeText={(text) => setPass({...pass, password: text})}
                                        secureTextEntry
                                        right={<TextInput.Icon name="eye" />}
                                        style={{backgroundColor: 'white'}}
                                    />

                                    <Text style={{color: 'gray', marginTop: 50}}>Confirmar contraseña</Text>
                                    {/* <InputPassActual /> */}
                                    <TextInput
                                        value={pass.confirmation}
                                        onChangeText={(text) => setPass({...pass, confirmation: text})}
                                        secureTextEntry
                                        right={<TextInput.Icon name="eye" />}
                                        style={{backgroundColor: 'white'}}
                                    />
                            </View>
                            <View style={[styles.box5]}>
                                <Button onPress={onUpdatePassword} style={{backgroundColor: '#64C2C8', justifyContent: 'center', alignSelf: 'center'}}>
                                    <Text style={{color: 'white', fontWeight: 'bold', fontSize: responsiveScreenFontSize(2)}}>
                                        Actualizar contraseña
                                    </Text>
                                </Button>
                            </View>
                            <View style={styles.centeredView}>
                                <Modal
                                    animationType="fade"
                                    transparent={true}
                                    visible={modalVisible}
                                    onRequestClose={() => {
                                        Alert.alert("Modal has been closed.");
                                        setModalVisible(!modalVisible);
                                    }}
                                >
                                    <View style={styles.centeredView}>
                                        <View style={{ width: '90%', height: 250, backgroundColor: 'white', justifyContent: 'flex-end', alignItems: 'center', borderRadius: 20 }}>
                                            <Icon name="check-circle" type="FontAwesome5" style={{ color: '#64C2C8', marginBottom: 20, fontSize: 50 }} />
                                            <Text style={{ fontSize: 20, marginBottom: '15%', marginHorizontal: 30, textAlign: 'center' }}>Tus contraseña ha sido modificada</Text>
                                            <Button onPress={functionOnpress} style={{ backgroundColor: '#64C2C8', justifyContent: 'center', borderBottomEndRadius: 20, borderBottomLeftRadius: 20, width: '100%' }}>
                                                <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>CERRAR</Text>
                                            </Button>
                                        </View>
                                    </View>
                                </Modal>
                            </View>
                        </View>
                    </ScrollView>
                    <ModalComponent onClose={setDialog} {...dialog} />  
                </View>
            </When>
            <Unless condition={!isLoading}>
                <Spinner color="red" style={styles.spinner} />
            </Unless>
        </Fragment>
    );
}


const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)'
    },

    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '90%',
        height: 50,
        marginBottom: 10,

    },
    box2: {
        width: '85%',
        height: 400,
        marginVertical: 20
    },

    box3: {
        width: '90%',
        height: 300,
        marginVertical: 10,
    },
    box4: {
        width: '90%',
        height: 150,
        marginBottom: 10,
    },
    box5: {
        width: '90%',
        height: 150,
        marginBottom: 10,
    },
});

