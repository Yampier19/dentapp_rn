import React, { useEffect, useState } from 'react'
import { Text, View, ScrollView, StyleSheet } from 'react-native'
import { Body, Left, Right, List, ListItem, Button, Content, Avatar, Spinner } from 'native-base';
import { Request } from '../../api/api';
import s from '@Public/Css/style';
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default function HistorialPremioScreen({ route, navigation }) {
    const { profile } = route.params || {};
    const [premios, setPremios] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getPremios();
    }, [])

    const getPremios = async () => {
        try {
            setLoading(true);
            const response = await Request('GET', 'premio/canjeados');
            const data = await response.json();

            if (data?.status === 'success' && data?.premios) {
                const filtred = data.premios.sort((a, b) => new Date(b.fecha_redencion) - new Date(a.fecha_redencion))
                let group = filtred.reduce((r, a) => {
                    r[a.fecha_redencion] = [...r[a.fecha_redencion] || [], a];
                    return r;
                }, {});
                setPremios(group);
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    };

    return (
        <View style={s.container}>
            {loading ? (
                <Spinner color="red" style={styles.spinner} />
            ) : (
                <ScrollView >

                    <View style={[styles.content]}>
                        <View style={{ backgroundColor: '#E7344C', width: wp('100%'), height: 250 }}>



                            <Avatar
                                large
                                source={{ uri: profile?.file }}
                                style={{
                                    width: wp('40%'),
                                    height: 150,
                                    borderRadius: 80,
                                    alignSelf: "center",
                                    marginTop: 20
                                }}
                            />

                            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: 20, textAlign: 'center', marginTop: 10 }}>{`${profile?.nombre} ${profile?.apellido}`}</Text>

                        </View>
                        <View style={[styles.box1]}>
                            <View style={{ width: '50%', paddingLeft: 20 }}>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>Puntos disponibles</Text>
                                <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{profile?.total_estrellas}</Text>
                            </View>
                            <View style={{ width: '50%' }}>
                                <Button onPress={() => navigation.navigate('PremioOfertaScreen')} style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center', backgroundColor: '#E7344C', width: 150, borderRadius: 10 }}>
                                    <Text style={{ color: 'white', fontSize: 20 }}>Ver premios</Text>
                                </Button>
                            </View>
                        </View>
                        {
                            premios !== null && Object.keys(premios).map((name, i) => (
                                <View key={`${i}-${name}`} style={[styles.box2]}>
                                    <View style={{ borderWidth: 1, borderBottomColor: '#F1F1F1', opacity: 0.1 }}></View>
                                    <Text style={{ alignSelf: 'flex-end', color: 'gray', marginTop: 10, fontWeight: 'bold' }}>
                                        {name}
                                    </Text>
                                            {premios[name].length > 0 && premios[name].map((premio, index) => (
                                                <View key={`${index}-${premio?.nombre}`} avatar>
                                                        <Avatar source={{ uri: 'https://img.icons8.com/bubbles/2x/gift.png' }} />
                                                        <Text style={{ color: 'black', fontSize: 18 }}>Canjeaste {premio?.total_estrellas} puntos - ({premio?.nombre})</Text>
                                                  
                                                </View>
                                            ))}
                                </View>
                            ))
                        }

                    </View>
                </ScrollView>
            )}




        </View>
    )


}


const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '100%',
        height: 100,
        backgroundColor: '#64C2C8',
        flexDirection: 'row',
        alignItems: 'center'
    },
    box2: {
        width: '95%',
        // height: 120,
        marginBottom: 10,
        marginTop: 10,
    },

    box3: {
        width: '95%',
        height: 120,
        marginBottom: 10,
        marginTop: 10,
    },


    box4: {
        width: '95%',
        height: 180,
        marginBottom: 10,
        marginTop: 10,
    },
});

