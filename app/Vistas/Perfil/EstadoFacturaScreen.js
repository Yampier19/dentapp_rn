import React, { useState, useEffect } from 'react';
import { Text, View, Image, Modal, ScrollView, TouchableOpacity, StyleSheet, Pressable, Dimensions, Linking } from 'react-native'
import { Item, Icon, Button } from 'native-base';
import { resolveImage,notificationAlert, blobToBase64 } from '../../utils/utils';
import s from '@Public/Css/style'
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import { WebView } from "react-native-webview";

export default function FacturaScreen({navigation, route}) {
    const { invoice, color } = route.params || {};
    const [data, setData] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [ typeFile, setTypeFile ]= useState(null);


      {/*  API WHATSAPP  */}
  const [whatsAppMessage, setWhatsAppMessage] = useState();
  const sendMsg = () => {
    let URL = Linking.openURL('whatsapp://send?text=Buen día, necesito ayuda con Dentaap, mi nombre es ...&phone=+573125614712')
    Linking.openURL(URL)
      .then((data) => {
        console.log('WhatsApp Opened');
      })
      .catch(() => {
        Alert.alert('Make sure Whatsapp installed on your device');
      });
  };


    useEffect(() => {
        if(invoice){
            resolveData();
        }
    }, [invoice])

    const resolveData = async () => {
        setTypeFile(invoice.foto.slice((invoice.foto.lastIndexOf(".") - 1 >>> 0 ) + 2));
        const currentData = await resolveImage([invoice], 'factura/getArchive', 'foto', 'facturas/', true);
        setData(currentData[0]);
    };

    const openImage = () => {
        if(invoice?.file){
            setModalVisible(true);
        } else {
            notificationAlert('No tiene archivo', 'La factura no tiene una imágen para mostrar.')
        }
    };

    return (
        <View style={s.container}>
            <ScrollView>
                <View style={[styles.content]}>
                    <View style={[styles.box1]}>
                        <Text style={{ alignSelf: 'flex-end', color: 'gray' }}>{data?.fecha_registro}</Text>
                    </View>
                    <View style={[styles.box2]}>

                        <View style={{width: 170, height: 170, borderRadius: 170, backgroundColor: 'white', borderColor: '#64C2C8', borderWidth: 2, alignItems: 'center', alignSelf: 'center'}}>

                        <TouchableOpacity style={{ marginBottom: 20, width: 150, height: 150, borderRadius: 80, backgroundColor: color, marginTop: '5%'}}>
                            <Icon type="Ionicons" name="receipt-outline" style={{ fontSize: 80, left: 27, color: 'white', marginTop: '20%' }} />
                        </TouchableOpacity>

                        </View>

                        <TouchableOpacity style={{ backgroundColor: color, padding: 10, width: 200, alignSelf: 'center', marginTop: 10 }} >
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Text style={{ color: 'white', marginLeft: 10, fontWeight: 'bold', textTransform: 'uppercase' }}>{data?.estado}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.box3]}>
                        <Text style={{fontSize: 15, fontWeight: 'bold', lineHeight: 25}}>
                            {data?.descripcion || data?.referencia}
                        </Text>
                    </View>
                    <View style={[styles.box4]}>
                        {
                            data?.estado === 'pendiente' && (
                                <Text style={{fontSize: 15, fontWeight: 'bold', lineHeight: 25}}>
                                    En caso de no tener respuesta o que el estado de la factura no cambie por favor comuníquese con nosotros.
                                </Text>
                            )
                        }
                    </View>
                    <View style={[styles.box5]}>
                        <Button onPress={openImage} style={{backgroundColor: '#E7344C', paddingHorizontal: 50, borderRadius: 10, alignSelf: 'center'}}> 
                            <Text style={{fontSize: 15, fontWeight: 'bold', textAlign: 'center', color: 'white'}}>
                                VER {typeFile === 'pdf' ? "ARCHIVO" : "IMAGEN"}
                            </Text>
                        </Button>
                    </View>
                    <View style={[styles.box6]}>
                    <Text style={{ alignSelf: 'flex-start', color: 'gray', marginTop: 20, fontWeight: 'bold' }}>
                            Contacto
                        </Text>
                        <Text style={{fontSize: 15, fontWeight: 'bold', lineHeight: 25, color: 'gray'}}>
                        Correo: dentapp@peoplecontact.cc
                        </Text>
                        <Text style={{fontSize: 15, fontWeight: 'bold', lineHeight: 25, color: 'gray', textDecorationLine: 'underline'}} onPress={sendMsg}>
                        Teléfono: +57 3125614712
                        </Text>
                    </View>
                </View>
            </ScrollView>

            <Modal
                animationType="fade"
                presentationStyle="fullScreen"
                visible={modalVisible}
                statusBarTranslucent
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        {
                            typeFile === 'pdf' ? (
                                <WebView
                                    style={{ width: 350, maxHeight: 500 }}
                                    source={{
                                        uri: `https://dentapp.panelrp.com/laravel/storage/app/public/${data?.foto}`,
                                    }}
                                />
                            ) : (
                                <ScrollView>
                                    <Image style={styles.imagenFactura} source={{uri: data?.file}} />
                                 </ScrollView>
                            )
                        }
                    </View> 
                        <Pressable
                            style={[styles.button, styles.buttonClose]}
                            onPress={() => setModalVisible(!modalVisible)}
                            >
                            <Text style={styles.textStyle}>Cerrar</Text>
                        </Pressable>
                </View>
            </Modal>
           
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red'
    },
    content: {
        alignItems: 'center',
        marginTop: 30
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    footer11: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '100%',
        height: 30,
        paddingHorizontal: 20,
    },
    box2: {
        width: '100%',
        height: 250,
    },
    box3: {
        width: '90%',
        height: 50,
    },
    box4: {
        width: '90%',
        height: 120,
    },
    box5: {
        width: '90%',
        height: 100,
      
    },
    box6: {
        width: '90%',
        height: 100,
        marginBottom: 80
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
    modalView: {
        backgroundColor: "white",
    },
    button: {
        padding: 14,
        elevation: 2
    },
    buttonClose: {
        backgroundColor: '#E7344C',
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 16,
        textTransform: 'uppercase'
    },
    imagenFactura: {
        alignSelf: 'center',
        resizeMode: 'contain',
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
    }
});
