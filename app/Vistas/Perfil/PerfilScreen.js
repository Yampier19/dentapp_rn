import React, { useState, useEffect, useCallback } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  Modal,
  StyleSheet,
  RefreshControl,
} from "react-native";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
  useResponsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { Avatar, Spinner, Icon, Button } from 'native-base'
import { List } from 'react-native-paper';
import * as ImagePicker from "expo-image-picker";
import { Default, Switch } from "react-if";
import { Case } from "react-if";
import { resolveImage } from "../../utils/utils";
import { removeToken } from "../../utils/utils";
import s from "@Public/Css/style";
import ModalComponent from "../../Components/Modal";
import {
  useProfileLevel,
  useChangeAvatar,
} from "../../services/dentapp/user/useUser";
import { useUserProvider } from "../../providers/user/user-provider";
import { Ionicons, FontAwesome } from '@expo/vector-icons';

export default function PerfilScreen({ route, navigation }) {
  const { refresh, reload } = route.params || {};
  const User = useUserProvider();
  const [profile, setProfile] = useState(User?.data?.datos);
  const [level, setLevel] = useState({});
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState(null);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
    goTo: "",
  });
  const [refreshing, setRefreshing] = useState(false);
  const { data: Level, isLoading: isLoadingProfile } = useProfileLevel();
  const { mutateAsync: ChangeAvatar, isLoading } = useChangeAvatar();

  const onRefresh = useCallback(() => {
    setRefreshing(true);
  }, []);

  const showImagePicker = async () => {
    const permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("You've refused to allow this appp to access your photos!");
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: Platform.OS !== "ios" ? true : false,
      quality: 1,
    });

    if (!result.cancelled) {
      // setImage(result.uri);
      updateAvatar(result.uri);
    }
  };

  const updateAvatar = async (file) => {
    let formData = new FormData();
    try {
      formData.append("foto", {
        uri: file,
        type: "image/jpeg",
        name: "avatar",
      });
      const response = await ChangeAvatar(formData);
      if (response?.status === "success") {
        setDialog({
          visible: true,
          message:
            response?.message || "La imagen fue actualizada de forma correcta",
          type: "info",
        });
        setImage(file);
      } else {
        setDialog({
          visible: true,
          message:
            response?.message ||
            "Ocurrió un error al momento de subir la imagen, intentelo nuevamente",
          type: "error",
        });
      }
    } catch (error) { }
  };

  const functionOnpress = async () => {
    await removeToken();
    setModalVisible(!modalVisible);
    navigation.push("Login");
  };

  useEffect(() => {
    setLoading(true);
    getProfile();
    getLevel();
    User.refetch();
  }, [refresh, refreshing, reload]);

  const getLevel = async () => {
    try {
      const data = Level.data;
      if (data?.status === "success" && data?.nivel) {
        setLevel(data.nivel);
      }
      setRefreshing(false);
    } catch (error) {
      setRefreshing(false);
    }
  };

  const getProfile = async () => {
    try {
      const data = User?.data;
      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );
        setProfile({
          ...currentData?.[0],
          password: data?.password,
          message: data?.message,
        });
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const logout = async () => {
    setModalVisible(true);
  };

  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
      <Switch>
        <Case condition={!isLoadingProfile}>
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            <View style={{ width: responsiveScreenWidth(90) }}>
              {isLoading ? (
                <Spinner color="white" style={styles.spinner} />
              ) : profile?.file ? (
                <TouchableOpacity onPress={showImagePicker}>
                  <Avatar
                    large
                    source={{ uri: image || profile?.file }}
                    style={{
                      width: 150,
                      height: 150,
                      marginTop: 10,
                      borderRadius: 80,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={showImagePicker}>
                  <Avatar
                    large
                    source={require("@Public/Img/perfil.jpg")}
                    style={{
                      width: 150,
                      height: 150,
                      marginTop: 10,
                      borderRadius: 80,
                      alignSelf: "center",
                    }}
                  />
                </TouchableOpacity>
              )}
              <Text
                style={{
                  fontSize: 20,
                  textAlign: "center",
                  marginTop: 20,
                  fontWeight: "bold",
                }}
              >{`${User?.data?.datos?.nombre} ${User?.data?.datos?.apellido}`}</Text>
              <Text
                style={{
                  fontSize: 15,
                  textAlign: "center",
                  marginBottom: 20,
                }}
              >
                {User?.data?.datos?.especialidad}
              </Text>
              <TouchableOpacity onPress={() =>
                      navigation.navigate("DatosPerfil", {
                        profile: User?.data?.datos,
                      })
                    } style={{ backgroundColor: '#E73340', padding: 10, borderRadius: 10, width: responsiveScreenWidth(50), alignSelf: 'center' }}>
                <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold' }} >Editar datos personales</Text>
              </TouchableOpacity>
            </View>
            <View style={{ width: responsiveScreenWidth(90), marginVertical: 30 }}>

              <View style={{ width: responsiveScreenWidth(90) }}>
                <List.Section>
                  <List.Subheader>
                    <Text style={{ fontWeight: 'bold', fontSize: useResponsiveScreenFontSize(2) }}>Mi perfil</Text>

                  </List.Subheader>
                  <List.Item onPress={() => navigation.navigate('Nivel')} style={{ padding: 0 }} title="Niveles" left={() => <List.Icon color="#65C1C7" icon="medal" />} />
                  <List.Item style={{ padding: 0 }} onPress={() => navigation.navigate("HistorialFactura")}
                    title="Historial de facturas"
                    left={() => <List.Icon color="#65C1C7" icon="receipt" />}
                  />
                  <List.Item style={{ padding: 0 }} onPress={() =>
                    navigation.navigate("HistorialPremio", { profile })
                  }
                    title="Historial de premios"
                    left={() => <List.Icon color="#65C1C7" icon="gift" />}
                  />
                  <List.Item style={{ padding: 0 }} onPress={() =>
                    navigation.navigate("Notificacion")}
                    title="Alertas"
                    left={() => <List.Icon color="#65C1C7" icon="bell" />}
                  />
                  <List.Item style={{ padding: 0 }} onPress={() =>
                    navigation.navigate("Private")}
                    title="Privacidad y seguridad de datos"
                    left={() => <List.Icon color="#65C1C7" icon="book" />}
                  />
                  <List.Item style={{ padding: 0 }} onPress={() =>
                    navigation.navigate("Help")
                  }
                    title="Ayuda"
                    left={() => <List.Icon color="#65C1C7" icon="help" />}
                  />
                  <List.Item style={{ padding: 0 }} onPress={logout}
                    title="Cerrar Sesiòn"
                    left={() => <List.Icon color="#65C1C7" icon="logout" />}
                  />
                </List.Section>
              </View>

              <View style={styles.centeredView}>
                <Modal
                  animationType="fade"
                  transparent={true}
                  visible={modalVisible}
                  onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                  }}
                >
                  <View style={styles.centeredView}>
                    <View
                      style={{
                        width: "90%",
                        height: 250,
                        backgroundColor: "white",
                        justifyContent: "flex-end",
                        alignItems: "center",
                        borderRadius: 20,
                      }}
                    >
                      <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-warning' : 'md-warning'}
                                            size="20"
                                            color="black"
                                            />
                      <Text
                        style={{
                          fontSize: 20,
                          marginBottom: "15%",
                          marginHorizontal: 30,
                          textAlign: "center",
                          marginTop: 10
                        }}
                      >
                        ¿Seguro que quieres cerrar sesion?
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          alignContent: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Button
                          onPress={functionOnpress}
                          style={{
                            backgroundColor: "#E7344C",
                            justifyContent: "center",
                            borderBottomLeftRadius: 20,
                            width: "50%",
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 15,
                              fontWeight: "bold",
                              color: "white",
                            }}
                          >
                            SI
                          </Text>
                        </Button>
                        <Button
                          onPress={() => setModalVisible(!modalVisible)}
                          style={{
                            backgroundColor: "#64C2C8",
                            justifyContent: "center",
                            borderBottomEndRadius: 20,
                            width: "50%", 
            }}
                        >
                          <Text
                            style={{
                              fontSize: 15,
                              fontWeight: "bold",
                              color: "white",
                            }}
                          >
                            NO
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>
            </View>
          </ScrollView>
        </Case>
        <Default>
          <Spinner color="red" style={styles.spinner} />
        </Default>
      </Switch>
      <ModalComponent onClose={setDialog} {...dialog} />
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 320,
    marginBottom: 10,
    backgroundColor: "#E7344C",
  },
  box2: {
    width: "100%",
    height: 400,
    marginBottom: 10,
  },

  box3: {
    width: "100%",
    height: 100,
  },
});
