import React, { useEffect, useState } from 'react'
import { Text, View, ScrollView, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { Icon, Spinner } from 'native-base';
import { Request } from '../../api/api';
import NotFound from '../../Components/NotFound';
import s from '@Public/Css/style'
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import { Ionicons, FontAwesome } from '@expo/vector-icons';

export default function HistorialFacturaScreen({route, navigation}) {
    const [invoices, setInvoices] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getInvoice();
        hourFact();
    }, [])

    const getInvoice = async () => {
        try {
            setLoading(true);
            const response = await Request('GET', 'factura/list');
            const data = await response.json();
            if (data?.status === 'success' && data?.facturas) {
                let group = data.facturas.reduce((r, a) => {
                    r[a.fecha_registro] = [...r[a.fecha_registro] || [], a];
                    return r;
                }, {});
                setInvoices(group);
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    };

    const onPressInvoice = (invoice) => {
        if(invoice.estado === "aceptado"){
            navigation.navigate('FacturaAceptada', {factura: invoice, puntos: invoice?.puntos});
        }else if(invoice.estado === "pendiente"){
            navigation.navigate('FacturaPendiente', {factura: invoice });
        }else if(invoice.estado === "rechazado"){
            navigation.navigate('FacturaRechazada', {factura: invoice })
        }
    };

    const hourFact = (date) => {
        let hoursLocal = new Date(date).getUTCHours();
        let minLocal = new Date(date).getUTCMinutes();
        if(hoursLocal <= 11){
            return `${hoursLocal}:${minLocal} AM`;
        }else if(hoursLocal >= 12 && hoursLocal <= 23){
            if(hoursLocal === 12){
                hoursLocal = hoursLocal;
                return `${hoursLocal}:${minLocal} PM`;
            }else {
                hoursLocal = hoursLocal - 12;
                return `${hoursLocal}:${minLocal} PM`;
            }
        }
    }

    const getBackground = (estado) => {
        let backgroundColor;
        switch (estado) {
            case 'aceptado':
                backgroundColor = '#64C2C8';
                break;

            case 'rechazado':
                backgroundColor = '#E7344C';
                break;

            case 'pendiente':
                backgroundColor = '#F28D80';
                break;

            default:
                backgroundColor = 'transparent';
                break;
        }

        return backgroundColor;
    };

    return (
        <View style={s.container}>
            {loading ? (
                <Spinner color="red" style={styles.spinner} />
            ) : (
                invoices && Object.entries(invoices).length > 0 ? (
                    <ScrollView >
                        <View style={[styles.content]}>
                            <View style={[styles.box1]}>
                                {
                                    invoices !== null && Object.keys(invoices).map((name, i) => (
                                        <View key={`${i}-${name}`}>
                                            <Text style={{ color: 'gray', alignSelf: 'flex-end' }}>{ name }</Text>
                                            {
                                                invoices[name].length > 0 && invoices[name].map((invoice, index) => (
                                                    <TouchableOpacity key={`${index}-${invoice?.referencia}`} onPress={() => onPressInvoice(invoice)} style={{ flexDirection: 'row', width: '100%', height: 90, backgroundColor: '#F8F8F8', marginBottom: 20 }} >
                                                        <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center' }}>
                                                            <View style={{ borderRadius: 40, marginBottom: 5, backgroundColor: getBackground(invoice?.estado), width: 50, height: 50, alignItems: 'center', justifyContent: 'center' }}>
                                                            <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-receipt' : 'md-receipt'}
                                            size="15"
                                            color="white"
                                            />
                                                            </View>
                                                            <Text style={{ paddingHorizontal: 15, borderRadius: 5, backgroundColor: getBackground(invoice?.estado), color: 'white' }}>{invoice?.estado}</Text>
                                                        </View>
                                                        {console.log('invoice', invoice?.created_at )}
                                                        <View style={{ width: '70%', padding: 10, justifyContent: 'center' }}>
                                                            <Text style={{ fontSize: 16, lineHeight: 25 }}>{`La factura cargarda el ${invoice?.fecha_registro} ${hourFact(invoice?.created_at)} se encuentra ${invoice?.estado}`}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </View>
                                    ))
                                }
                                
                            </View>
                        </View>
                    </ScrollView>
                ) : (
                    <NotFound message="No tienes facturas disponibles." />
                )
            )}
         
        </View>
    )


}


const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    video: {
        marginLeft: 20,
        width: 352,
        height: 200,
        top: '10%',


    },
    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '95%',
        // height: 150,
        marginTop: 20,
    },
    box2: {
        width: '95%',
        height: 230,
        marginBottom: 10,
        marginTop: 10,
    },

    box3: {
        width: '95%',
        height: 230,
        marginBottom: 10,
        marginTop: 10,
    },
    box4: {
        width: '90%',
        height: 150,
        marginBottom: 10,
    },
    box6: {
        width: '90%',
        height: Dimensions.get('screen').height - 600,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
});

