import React, { useEffect, useState } from "react";
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text,
} from "react-native";
import { Spinner } from "native-base";
import Swiper from "react-native-web-swiper";

export default function HomeScreen(props) {
  const { width } = Dimensions.get("window");
  return (
    <View
      style={{
        width: width * 1.0,
        height: width * 0.5 * 0.9,
        backgroundColor: "#E7344C",
      }}
    >
      <Swiper
        from={1}
        controlsProps={{
          dotsTouchable: true,
          dotsPos: "bottom",
          prevPos: false,
          nextPos: false,
        }}
        minDistanceForAction={0.1}
        accessible={true}
        accessibilityLabel="Imagenes promocionales"
        accessibilityHint="Carousel principal"
        loop={true}
        autoplay={true}
        autoplayTimeout={5.0}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(20,20,200,0.3)",
          }}
        >
           <Image style={{ width: width * 1.0, height: width * 0.5 * 0.9 }} resizeMode = 'cover'
            source={require("@Public/Img/banner_3.jpg")}
          />
        </View>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(20,200,20,0.3)",
          }}
        >
          <Image style={{ width: width * 1.0, height: width * 0.5 * 0.9 }} resizeMode = 'cover'
            source={require("@Public/Img/banner_2.jpg")}
          />
        </View>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(200,20,20,0.3)",
          }}
        >
          <Image style={{ width: width * 1.0, height: width * 0.5 * 0.9 }} resizeMode = 'cover'
            source={require("@Public/Img/banner_1.jpg")}
          />
        </View>
      </Swiper>
    </View>
  );
}

const styles = StyleSheet.create({
  video: {
    flex: 1,
    width: "100%",
    height: 120,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  playButton: {
    position: "absolute",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    top: "30%",
    left: "40%",
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: "rgba(255,255,255,.8)",
  },
  textPlay: {
    color: "black",
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  wrapper: {},

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },

  box2: {
    width: "95%",
    height: 370,
  },
  box3: {
    width: "100%",
    height: 500,
    marginTop: 20,
  },
  box4: {
    width: "100%",
    height: 350,
  },
  box5: {
    width: "100%",
    height: 100,
  },

  centeredView: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
  },
  modalView: {
    backgroundColor: "#E7344C",
    borderRadius: 160,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    left: 50,
    top: 20,
    width: "80%",
    height: 320,
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    justifyContent: "center",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
