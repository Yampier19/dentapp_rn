import React from "react";
import { Box, Heading, AspectRatio, Image, Text, Center, HStack, Stack, NativeBaseProvider } from "native-base";
import { responsiveScreenFontSize, responsiveScreenWidth } from "react-native-responsive-dimensions";
import { View } from 'react-native'

export default function Example() {
  return (
    <>
      <View style={{ flexDirection: 'row', width: responsiveScreenWidth(90), justifyContent: 'center', marginVertical: 20 }}>
        <Box rounded="lg" style={{ width: responsiveScreenWidth(40) }} overflow="hidden" borderColor="coolGray.200" borderWidth="1" _dark={{
          borderColor: "coolGray.600",
          backgroundColor: "gray.700"
        }} _web={{
          shadow: 2,
          borderWidth: 0
        }} _light={{
          backgroundColor: "gray.50"
        }}>
          <Box>
            <AspectRatio w="100%" ratio={16 / 9}>
              <Image source={{ uri: 'https://i.ibb.co/tqzQg6j/1.png' }} alt="¿Cómo puedo ganar en
              DentApp?"/>
            </AspectRatio>
          </Box>
          <Stack p="4" space={3}>
            <Text fontWeight="400" style={{ fontSize: responsiveScreenFontSize(1.5), textAlign: 'center', fontWeight: '700' }}>
              ¿Cómo puedo ganar en
              DentApp?
            </Text>
            <HStack alignItems="center" space={4} justifyContent="space-between">
              <HStack alignItems="center" style={{ width: '100%', backgroundColor: 'red', borderRadius: 5, justifyContent: 'center' }}>
                <Text color="white" _dark={{
                  color: "white"
                }} fontWeight="400">
                  Ver más
                </Text>
              </HStack>
            </HStack>
          </Stack>
        </Box>
        <View style={{ marginHorizontal: responsiveScreenWidth(3) }}></View>
        <Box rounded="lg" style={{ width: responsiveScreenWidth(40) }} overflow="hidden" borderColor="coolGray.200" borderWidth="1" _dark={{
          borderColor: "coolGray.600",
          backgroundColor: "gray.700"
        }} _web={{
          shadow: 2,
          borderWidth: 0
        }} _light={{
          backgroundColor: "gray.50"
        }}>
          <Box>
            <AspectRatio w="100%" ratio={16 / 9}>
              <Image source={{ uri: 'https://i.ibb.co/gPn4WcH/2.png' }} alt="¿Cómo puedo cargar una factura?" />
            </AspectRatio>
          </Box>
          <Stack p="4" space={3}>
            <Text fontWeight="400" style={{ fontSize: responsiveScreenFontSize(1.5), textAlign: 'center', fontWeight: '700' }}>
              ¿Cómo puedo cargar
              una factura?
            </Text>
            <HStack alignItems="center" space={4} justifyContent="space-between">
              <HStack alignItems="center" style={{ width: '100%', backgroundColor: 'red', borderRadius: 5, justifyContent: 'center' }}>
                <Text color="white" _dark={{
                  color: "white"
                }} fontWeight="400">
                  Ver más
                </Text>
              </HStack>
            </HStack>
          </Stack>
        </Box>
      </View>

      <View style={{ flexDirection: 'row', width: responsiveScreenWidth(90), justifyContent: 'center' }}>
        <Box rounded="lg" style={{ width: responsiveScreenWidth(40) }} overflow="hidden" borderColor="coolGray.200" borderWidth="1" _dark={{
          borderColor: "coolGray.600",
          backgroundColor: "gray.700"
        }} _web={{
          shadow: 2,
          borderWidth: 0
        }} _light={{
          backgroundColor: "gray.50"
        }}>
          <Box>
            <AspectRatio w="100%" ratio={16 / 9}>
              <Image source={{ uri: 'https://i.ibb.co/8X8mpq0/3.png' }} alt="¿Cómo puedo subir de nivel?" />
            </AspectRatio>
          </Box>
          <Stack p="4" space={3}>
            <Text fontWeight="400" style={{ fontSize: responsiveScreenFontSize(1.5), textAlign: 'center', fontWeight: '700' }}>
              ¿Cómo puedo subir de nivel?
            </Text>
            <HStack alignItems="center" space={4} justifyContent="space-between">
              <HStack alignItems="center" style={{ width: '100%', backgroundColor: 'red', borderRadius: 5, justifyContent: 'center' }}>
                <Text color="white" _dark={{
                  color: "white"
                }} fontWeight="400">
                  Ver más
                </Text>
              </HStack>
            </HStack>
          </Stack>
        </Box>
        <View style={{ marginHorizontal: responsiveScreenWidth(3) }}></View>
        <Box rounded="lg" style={{ width: responsiveScreenWidth(40) }} overflow="hidden" borderColor="coolGray.200" borderWidth="1" _dark={{
          borderColor: "coolGray.600",
          backgroundColor: "gray.700"
        }} _web={{
          shadow: 2,
          borderWidth: 0
        }} _light={{
          backgroundColor: "gray.50"
        }}>
          <Box>
            <AspectRatio w="100%" ratio={16 / 9}>
              <Image source={{ uri: 'https://i.ibb.co/1vDM7FD/4.png' }} alt="¿Cómo canjear mis puntos?" />
            </AspectRatio>
          </Box>
          <Stack p="4" space={3}>
            <Text fontWeight="400" style={{ fontSize: responsiveScreenFontSize(1.5), textAlign: 'center', fontWeight: '700' }}>
              ¿Cómo canjear mis puntos?
            </Text>
            <HStack alignItems="center" space={4} justifyContent="space-between">
              <HStack alignItems="center" style={{ width: '100%', backgroundColor: 'red', borderRadius: 5, justifyContent: 'center' }}>
                <Text color="white" _dark={{
                  color: "white"
                }} fontWeight="400">
                  Ver más
                </Text>
              </HStack>
            </HStack>
          </Stack>
        </Box>
      </View>
    </>


  )

};