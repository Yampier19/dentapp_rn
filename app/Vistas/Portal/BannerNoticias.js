import React from 'react';
import { Text, View, Image, SafeAreaView, TouchableOpacity } from 'react-native';
import { Spinner } from 'native-base';
import Carousel from 'react-native-snap-carousel';

class BannerNoticias extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      noticias: props?.noticias || [],
      imagenes: [],
      loading: false
    };
  }

  _renderItem({ item, index}) {
    const { nombre, tipo, file } = item;
    return (
<TouchableOpacity onPress={() => this.props.navigation.navigate("DetalleNoticia", {noticia: item})} style={{padding: 5, width: '100%', height: '95%',  backgroundColor: '#F8F8F8',
borderRadius:10}}>
      <View style={{
        backgroundColor: '#F1F1F1',
        height: 290,
        marginLeft: 5,
        marginRight: 5
      }}>
        

        <Image
          source={{uri: file}}
          style={{ width: '100%', height: '100%' }}
        />

        <Text style={{padding: 10}}>{nombre}</Text>
     
        </View>
          
        </TouchableOpacity>
    );
  }

  render() {
    return (
      this.state.loading ? (
        <Spinner color="red" style={{flex: 1, justifyContent: "center", alignItems: "center",}} />
      ) : (
        <SafeAreaView style={{ flex: 1 }} >
          
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }} >
            <Carousel 
             
              layout={"default"}
              ref={ref => this.carousel = ref}
              data={this?.state?.noticias }
              sliderWidth={260}
              itemWidth={260}
              renderItem={this._renderItem.bind(this)}
              lockScrollWhileSnapping
              autoplay
              useScrollView
              loop
              autoplayInterval={5000}
              onSnapToItem={index => this.setState({ activeIndex: index })} />
                
          </View>
        </SafeAreaView>
      )
    );
  }
}

export default BannerNoticias;
