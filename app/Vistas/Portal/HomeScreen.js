import React, { useEffect, useState, useCallback } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  RefreshControl,
  Modal,
  Linking,
} from "react-native";
import { Spinner, Avatar, Icon } from "native-base";
import Swiper from "react-native-web-swiper";
import { DefaultTheme } from "react-native-paper";
import { FAB } from "react-native-paper";
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import BannerNoticias from "./BannerNoticias";
import ViewFile from "../../Components/ViewFile";
import { Request } from "../../api/api";
import { resolveImage, getToken } from "../../utils/utils";
import home from "@Public/Css/Portal/Home";
import TriviaInicial from "./TriviaInicial"
import { useAnnouncementList } from "../../services/dentapp/announcement/useAnnouncement";
import { useProfile } from "../../services/dentapp/user/useUser";
import PushNotification from "../../Components/PushNotificacion";
import Color from "./../../Components/Distribuidores/Color";
import { useBannersList } from "../../services/dentapp/banners/useBanners";

export default function HomeScreen(props) {

  const { reload } = props?.route?.params || {};
  const [user, setUser] = useState(null);
  const [modalVisibleVerify, setModalVisibleVerify] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadingNews, setLoadingNews] = useState(false);
  const [loadingTraining, setLoadingTraining] = useState(false);
  const [trainings, setTrainings] = useState([]);
  const [noticias, setNoticias] = useState([]);
  const [banners, setBanners] = useState([]);
  const { data: Banners, isLoading: isLoadingBanners } = useBannersList();
  const [token, setToken] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [state, setState] = React.useState({ open: false });
  const { data: Announcement, isLoading } = useAnnouncementList(10);
  const { data: Profile, isLoading: isLoadingProfile } = useProfile();

  const onStateChange = ({ open }) => setState({ open });
  const { open } = state;
  useEffect(() => {
    props.navigation.addListener("beforeRemove", (e) => {
      // Prevent default behavior of leaving the screen
      e.preventDefault();
    });
  }, []);


  const onRefresh = useCallback(() => {
    setRefreshing(true);
    main();
  }, []);

  const getCapacitaciones = async () => {
    try {
      setLoadingTraining(true);
      const response = await Request("GET", "training/list");
      const data = await response.json();
      if (data?.status === "success" && data?.trainings?.length > 0) {
        const currentData = await resolveImage(
          data?.trainings,
          "training/getImage",
          "imagen",
          "training/",
          true
        );
        setTrainings(currentData);
        setLoadingTraining(false);
      }
    } catch (error) {
      setLoadingTraining(false);
    }
  };

  const getNews = async () => {
    try {
      setLoadingNews(true);
      const data = Announcement;
      if (data?.status === "success" && data?.noticias.length > 0) {
        const currentData = await resolveImage(
          data?.noticias,
          "noticias/getImage",
          "imagen",
          "noticias/",
          true
        );
        setNoticias(currentData);
        setLoadingNews(false);
      }
    } catch (error) {
      setLoadingNews(false);
    }
  };

  const getProfile = async () => {
    setLoading(true);
    try {
      const data = Profile;


      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );
        setUser({
          ...currentData?.[0],
          password: data?.password,
          email_verified_at: data?.email_verified_at,
          cambiar: data?.cambiar,
          message: data?.message,
        });

        if (user?.cambiar == 0 && user?.email_verified_at == null || user?.email_verified_at == '') {
          setModalVisibleVerify(true);
          console.log('El usuario no esta verificado')

        } else {
          console.log('Ya esta verificado', user?.cambiar, user?.email_verified_at)
        }
      } else if (data?.message === "Unauthenticated") {
        navigation.navigate("Login");
      }
    } catch (error) { }
  };

  const getBanners = async () => {
    try {
      setLoading(true);
      const data = Banners;
      if (data?.status === "success" && data?.banners.length > 0) {
        const images = await resolveImage(
          data.banners,
          "banners/getImage",
          "imagen",
          "banner/",
          false
        );
        if (images.length > 0) {
          let newImg = [];
          images.forEach((img, index) => {
            newImg.push({
              img: img,
              data: data.banners[index],
            });
          });
          setBanners(newImg);
        }
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
    }
  };

  const goToVideo = (video) => {
    if (video.archivo) {
      props.navigation.navigate("DetallesBanner", { video });
    }
  };


  useEffect(() => {
    main();
  }, [
    reload,
    isLoading,
    refreshing,
    isLoadingBanners,
    isLoadingProfile
  ]);


  const main = async () => {
    getProfile();
    const localToken = await getToken();
    if (localToken) {
      setToken(localToken);
      getBanners();
      getNews();
      getCapacitaciones();
      setRefreshing(false);
    } else {
      props.navigation.navigate("Login");
    }


  };

  if (!token) {
    return <></>;
  }

  return (
    <View style={home.content}>
      <View  style={{
        "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83Body :
          user?.distribuidor === "Dental Nader" ? Color.DentalNaderBody :
            user?.distribuidor === "Adental" ? Color.DentalAdentalBody :
              user?.distribuidor === "Bracket" ? Color.BracketBody :
                '#E7334C'
          }`, width: responsiveScreenWidth(100), height: responsiveScreenHeight(25), alignItems: 'center'
      }}>
        <View style={{ width: responsiveScreenWidth(100), flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
          <View style={{ width: responsiveScreenWidth(35), alignItems: 'center' }}>
            <TouchableOpacity onPress={() => props.navigation.navigate('Perfil')}>
              <Avatar large source={{ uri: user?.file }} style={{ width: 100, height: 100 }} />
              <View style={{ width: 35, height: 35, borderRadius: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', position: 'absolute', alignSelf: 'flex-end', bottom: 0 }}>
                <Image source={require('@Public/Img/check.png')} style={{ width: 20, height: 20 }} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ width: responsiveScreenWidth(55), flexDirection: 'column' }}>
            <Text style={{ fontWeight: 'bold', color: 'white', fontSize: responsiveScreenFontSize(2.5) }}>{`${user?.nombre} ${user?.apellido}`}</Text>
            <Text style={{ color: 'white', fontSize: responsiveScreenFontSize(2) }}>{user?.especialidad}</Text>
            <TouchableOpacity style={{
              "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83Box :
                user?.distribuidor === "Dental Nader" ? Color.DentalNaderBox :
                  user?.distribuidor === "Adental" ? Color.DentalAdentalBox :
                    user?.distribuidor === "Bracket" ? Color.BracketBox :
                      '#E7334C'
                }`, padding: 5, borderRadius: 20, marginVertical: 10, fontWeight: 'bold'
            }}>
              <Text style={{
                color: `${user?.distribuidor === "Dental 83" ? Color.Dental83Text :
                  user?.distribuidor === "Dental Nader" ? Color.DentalNaderText :
                    user?.distribuidor === "Adental" ? Color.DentalAdentalText :
                      user?.distribuidor === "Bracket" ? Color.BracketText :
                        '#E7334C'
                  }`, textAlign: 'center'
              }}>Puntos acumulados: {user?.total_estrellas
                .toString()
                .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}{" "}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <View style={{ backgroundColor: 'white', width: responsiveScreenWidth(100), alignItems: 'center', borderRadius: 40, top: -50, paddingVertical: 20 }}>
        <View style={{ width: responsiveScreenWidth(90) }}>
          <View style={{ width: responsiveScreenWidth(90) }}>
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2) }}>Promociones</Text>
          </View>
          <View style={{ width: responsiveScreenWidth(90), flexDirection: 'row', marginVertical: 10 }}>
            <TouchableOpacity onPress={() => props.navigation.navigate('PremioOfertaScreen')} style={{
              "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83Body :
                user?.distribuidor === "Dental Nader" ? Color.DentalNaderBody :
                  user?.distribuidor === "Adental" ? Color.DentalAdentalBody :
                    user?.distribuidor === "Bracket" ? Color.BracketBody :
                      '#E7334C'
                }`, width: responsiveScreenWidth(25), height: responsiveScreenHeight(10), borderRadius: 10, alignItems: 'center', padding: 5
            }}>
              <View style={{ width: '100%', height: '50%', alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('@Public/Img/dental_ico.png')} style={{ width: 30, height: 30 }} />
              </View>
              <View style={{ width: '100%', height: '50%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Dental</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('PremioOfertaScreen')} style={{
              "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83Body :
                user?.distribuidor === "Dental Nader" ? Color.DentalNaderBody :
                  user?.distribuidor === "Adental" ? Color.DentalAdentalBody :
                    user?.distribuidor === "Bracket" ? Color.BracketBody :
                      '#E7334C'
                }`, width: responsiveScreenWidth(25), height: responsiveScreenHeight(10), borderRadius: 10, alignItems: 'center', padding: 5, marginHorizontal: 30
            }}>
              <View style={{ width: '100%', height: '50%', alignItems: 'center', justifyContent: 'center', }}>
                <Image source={require('@Public/Img/ortodoncia_ico.png')} style={{ width: 35, height: 25 }} />
              </View>
              <View style={{ width: '100%', height: '50%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Ortodoncia</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.navigation.navigate('PremioOfertaScreen')} style={{
              "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83Body :
                user?.distribuidor === "Dental Nader" ? Color.DentalNaderBody :
                  user?.distribuidor === "Adental" ? Color.DentalAdentalBody :
                    user?.distribuidor === "Bracket" ? Color.BracketBody :
                      '#E7334C'
                }`, width: responsiveScreenWidth(25), height: responsiveScreenHeight(10), borderRadius: 10, alignItems: 'center', padding: 5
            }}>
              <View style={{ width: '100%', height: '50%', alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('@Public/Img/innovacion_ico.png')} style={{ width: 35, height: 30 }} />
              </View>
              <View style={{ width: '100%', height: '50%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontWeight: 'bold' }}>Innovación</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <View style={home.box_banner}>
            {loading ? (
              <Spinner
                color="red"
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              />
            ) : (
              <Swiper
                from={1}
                controlsProps={{
                  dotsTouchable: true,
                  dotsPos: "bottom",
                  prevPos: false,
                  nextPos: false,
                }}
                minDistanceForAction={0.1}
                accessible={true}
                accessibilityLabel="Imagenes promocionales"
                accessibilityHint="Carousel principal"
                loop={true}
                autoplay={true}
                autoplayTimeout={5.0}
              >
                {banners?.map((image, index) => (
                  <TouchableOpacity
                    key={`banners_${index}`}
                    onPress={() => goToVideo(image.data)}
                  >
                    <Image
                      key={`banner-${index}`}
                      style={home.box_banner_image}
                      resizeMode="cover"
                      source={{ uri: image.img }}
                    />
                  </TouchableOpacity>
                ))}
              </Swiper>
            )}
          </View>

          <View style={{ width: responsiveScreenWidth(90), paddingVertical: 10, alignSelf: 'center' }}>
            <View style={{ width: responsiveScreenWidth(90), marginVertical: 10 }} >
              <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2) }}>Noticias</Text>
            </View>
            <View style={{ width: responsiveScreenWidth(90), height: responsiveScreenHeight(50), marginVertical: 15 }}>
              <View style={home.box_noticias}>
                {loadingNews ? (
                  <Spinner color="red" style={styles.spinner} />
                ) : noticias.length > 0 ? (
                  <BannerNoticias noticias={noticias} navigation={props.navigation} />
                ) : (
                  <View style={home.box_noticias_null}>
                    <Text style={{ alignItems: "center", justifyContent: "center" }}>
                      No hay noticias disponibles
                    </Text>
                  </View>
                )}
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate("Noticias");
                  }}
                  style={{
                    "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83BoxTwo :
                      user?.distribuidor === "Dental Nader" ? Color.DentalNaderBoxTwo :
                        user?.distribuidor === "Adental" ? Color.DentalAdentalBoxTwo :
                          user?.distribuidor === "Bracket" ? Color.BracketBoxTwo :
                            '#E7334C'
                      }`, alignSelf: 'center',
                    width: responsiveScreenWidth(70),
                    height: responsiveScreenHeight(5),
                    borderRadius: 10,
                    justifyContent: 'center'
                  }}
                >
                  <Text
                    style={{
                      fontSize: responsiveScreenFontSize(2),
                      color: "white",
                      textAlign: "center",
                      fontWeight: "bold",
                    }}
                  >
                    Leer todas las noticias
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ width: responsiveScreenWidth(90), height: responsiveScreenHeight(65), marginVertical: 10 }}>
              {loadingTraining ? (
                <Spinner color="red" />
              ) : (

                <View style={home.box_capacitacion}>
                  {trainings.length > 0 && trainings?.[0] !== null && (
                    <TouchableOpacity
                      onPress={() => {
                        props.navigation.navigate("SelectCapacitacion", {
                          uid: trainings[0]?.id_capacitacion,
                        });
                      }}
                      style={home.box_item_capacitacion}
                    >
                      <ViewFile
                        style={home.box_archivo_capacitacion}
                        filetype={trainings[0]?.filetype}
                        file={trainings[0]?.file}
                        token={trainings[0]?.token || null}
                      />
                      <View style={home.box_descripcion_capacitacion}>
                        <Text style={home.box_text_descripcion_capacitacion}>
                          {trainings[0]?.descripcion}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                  {trainings.length > 1 && trainings?.[1] !== null && (
                    <View
                      style={{
                        width: responsiveScreenWidth(5),
                        height: responsiveScreenHeight(26),
                        backgroundColor: "white",
                      }}
                    ></View>
                  )}
                  {trainings.length > 1 && trainings?.[1] !== null && (
                    <TouchableOpacity
                      onPress={() => {
                        props.navigation.navigate("SelectCapacitacion", {
                          uid: trainings[1]?.id_capacitacion,
                        });
                      }}
                      style={home.box_item_capacitacion}
                    >
                      <ViewFile
                        style={home.box_archivo_capacitacion}
                        filetype={trainings[1]?.filetype}
                        file={trainings[1]?.file}
                        token={trainings[1]?.token || null}
                      />
                      <View style={home.box_descripcion_capacitacion}>
                        <Text style={home.box_text_descripcion_capacitacion}>
                          {trainings[1]?.descripcion}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </View>
              )}

              <View style={home.box_buttom_capacitacion}>
                <TouchableOpacity
                  onPress={() => {
                    props.navigation.navigate("Capacitacion");
                  }}
                  style={{
                    "backgroundColor": `${user?.distribuidor === "Dental 83" ? Color.Dental83BoxTwo :
                      user?.distribuidor === "Dental Nader" ? Color.DentalNaderBoxTwo :
                        user?.distribuidor === "Adental" ? Color.DentalAdentalBoxTwo :
                          user?.distribuidor === "Bracket" ? Color.BracketBoxTwo :
                            '#E7334C'
                      }`, alignSelf: 'center',
                    width: responsiveScreenWidth(70),
                    height: responsiveScreenHeight(5),
                    borderRadius: 10,
                    justifyContent: 'center'
                  }}
                >
                  <Text style={home.box_text_buttom_capacitacion}>
                    Ver todos los contenidos
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </ScrollView>
      </View>

      <TouchableOpacity
        style={{ position: "absolute", bottom: 0, right: 0 }}
      >
        <Image
          source={require("@Public/Img/whatsapp.png")}
          style={{
            width: 70,
            height: 70,
            marginBottom: "23%",
            marginRight: "3%",
          }}
        />
      </TouchableOpacity>
      <FAB.Group
        color="white"
        fabStyle={{ backgroundColor: "#E7344C" }}
        theme={{
          ...DefaultTheme,
          colors: {
            ...DefaultTheme.colors,
            surface: "#E7344C",
          },
          dark: true,
        }}
        open={open}
        icon={open ? "close" : "plus"}
        actions={[
          {
            icon: "cloud-upload",
            label: "Enviar Factura",
            onPress: () => props.navigation.navigate("Facturas"),
          },
          {
            icon: "receipt",
            label: "Mis Facturas",
            onPress: () => props.navigation.navigate("HistorialFactura"),
          },
          {
            icon: "help-circle",
            label: "Ayuda",
            onPress: () => props.navigation.navigate("Help"),
            small: false,
          },
        ]}
        onStateChange={onStateChange}
        onPress={() => {
          if (open) {
            // do something if the speed dial is open
          }
        }}
      />

      {user && <PushNotification email={user?.correo} />}

      <TriviaInicial />

      <View style={styles.centeredViewVerify}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisibleVerify}
        >
          <View style={styles.centeredViewVerify}>
            <View style={styles.modalViewVerify}>
              <View style={{ alignItems: 'flex-end', width: responsiveScreenWidth(90), height: responsiveScreenHeight(10) }}>
                {/* <TouchableOpacity onPress={() => setModalVisibleVerify(!modalVisibleVerify)}>
                    <Icon
                      as={Ionicons}
                      name={Platform.OS ? 'ios-close' : 'md-close'}
                      size="16"
                      color="red"
                    />
                  </TouchableOpacity> */}
              </View>
              <View style={{ alignItems: 'center', width: responsiveScreenWidth(90), padding: 20 }}>
                <Image source={require('@Public/Img/verify.png')} style={{ width: responsiveScreenWidth(60), height: responsiveScreenHeight(22) }} />
              </View>
              <View style={{ alignItems: 'center', width: responsiveScreenWidth(90) }}>
                <Text style={{ fontWeight: 'bold', fontSize: responsiveFontSize(2.5), textAlign: 'center', marginHorizontal: 20 }}>¡Hola! Recuerda verificar tu correo electrónico</Text>
                <Text style={{ marginVertical: 30, marginHorizontal: 50, textAlign: 'center', fontSize: responsiveFontSize(2) }}>Revisa el buzón de tu correo y sigue las instrucciones enviadas. El correo fue enviado a:</Text>
                <Text style={{ marginBottom: 30, textAlign: 'center', fontSize: responsiveFontSize(2.5), fontWeight: 'bold' }}>{user?.correo}</Text>
              </View>
              <View style={{ alignItems: 'center', width: responsiveScreenWidth(90) }}>
                <TouchableOpacity style={{ backgroundColor: '#E7334C', padding: 10, paddingHorizontal: 50, borderRadius: 10 }}>
                  <Text style={{ color: 'white' }}>¿Necesitas Ayuda?</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>


    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  centeredViewVerify: {
    flex: 1,
  },
  modalViewVerify: {
    backgroundColor: 'white',
    width: responsiveScreenWidth(100),
    height: responsiveScreenHeight(100),
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    bottom: 140,
    left: 150,
    padding: 3,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  modalView: {
    margin: 20,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 20,
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(50),
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
