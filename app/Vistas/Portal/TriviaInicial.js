import React, { useState, useEffect } from 'react';
import { View, Platform, Text, TouchableOpacity, Modal, StyleSheet, Image } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Request } from '../../api/api';
import { format } from "date-fns";
import { Item, Input, Icon } from 'native-base';
import { Checkbox } from 'react-native-paper';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import ModalComponent from '../../Components/Modal';

export default function App() {
  const [modalVisible, setModalVisible] = useState(true);
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [inputText, setInputText] = useState('');
  const [show, setShow] = useState(false);
  const [checked, setChecked] = useState([]);
  const [index, setIndex] = useState(0);
  const [encuestas, setEncuestas] = useState([]);
  const [currentTrivia, setCurrentTrivia] = useState(null);
  const [dialog, setDialog] = useState({ visible: false, message: '', type: '' });
  const [respuestas, setRespuestas] = useState([]);

  useEffect(() => {
    getTriviasHome();
  }, [])

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const getTriviasHome = async () => {
    try {
      const data = await Request('GET', 'encuesta/listhome/10');
      const response = await data.json();
      if (response.status === 'success' && response.encuestas.length > 0) {
        setCurrentTrivia(response.encuestas[0]);
        getTriviaById(response.encuestas?.[0]?.id_trivia)
      }
    } catch (error) { }
  };

  const getTriviaById = async (id) => {
    try {
      const response = await Request('GET', `encuesta/preguntas/${id}`);
      const data = await response.json();
      if (data?.status === 'success') {
        setEncuestas(data?.preguntas);
      }
    } catch (error) { }
  };

  const saveTrivia = async (payload) => {
    const id = currentTrivia?.id_trivia;
    try {
      const response = await Request('POST', `encuesta/encuestarespuestas/${id}`, payload);
      const data = await response.json();
      if (data?.status === 'success') {
        setDialog({
          visible: true,
          message: data?.message || 'Los datos se enviaron correctamente',
          type: 'info'
        });

      } else {
        setDialog({
          visible: true,
          message: data?.message || 'Ocurrió un error al enviar los datos, intentelo nuevamente',
          type: 'error'
        });
      }
    } catch (error) { }
  };

  const onPressContinue = (trivia) => {
    if (trivia?.tipo === 'radio') {
      if (checked.length > 0) {
        const res = respuestas;
        res.push({
          pregunta: trivia?.informacion,
          respuesta: checked,
          puntuacion: trivia?.puntuacion
        })
        setRespuestas(res);
        setChecked([])
      } else {
        setDialog({
          visible: true,
          message: 'Para continuar por favor seleccione una opción',
          type: 'warning'
        });
      }
    }

    if (trivia?.tipo === 'text') {
      if (inputText) {
        const res = respuestas;
        res.push({
          pregunta: trivia?.informacion,
          respuesta: inputText,
          puntuacion: trivia?.puntuacion
        })
        setRespuestas(res)
        setInputText('')
      } else {
        setDialog({
          visible: true,
          message: 'Para continuar por favor responda la pregunta',
          type: 'warning'
        });
      }
    }

    if (trivia?.tipo === 'date') {
      if (date) {
        const res = respuestas;
        res.push({
          pregunta: trivia?.informacion,
          respuesta: format(date, "dd-MM-yyyy"),
          puntuacion: trivia?.puntuacion
        })
        setRespuestas(res);
        setShow(false)
      } else {
        setDialog({
          visible: true,
          message: 'Para continuar por favor seleccione una fecha',
          type: 'warning'
        });
      }
    }

    if (index >= encuestas.length - 1) {
      const payload = {
        trivia: respuestas,
        total_puntos: total(respuestas)
      };
      saveTrivia(payload);
      setIndex(index + 1);
    } else {
      setIndex(index + 1);
    }
  }

  const skip = async () => {
    if (index >= encuestas.length - 1) {
      if (respuestas.length === 0) {
        try {
          const id = currentTrivia?.id_trivia;
          const response = await Request('GET', `encuesta/skip/${id}`);
          const data = await response.json();
          if (data?.status === 'success') {
            setDialog({
              visible: true,
              message: data?.message || 'La encuesta fue saltada exitosamente no aparecerá más',
              type: 'info'
            });

          }
        } catch (error) { }
      }
    }
    setIndex(index + 1);
  }

  const total = (data) => {
    let result = 0;
    data.forEach(res => {
      result += res.puntuacion;
    });

    return result;
  };

  return (
    encuestas.length > 0 && encuestas?.[index] ? (

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={{ width: responsiveScreenWidth(80), alignItems: 'flex-end' }}>
              <Icon onPress={() => setModalVisible(!modalVisible)}
                as={Ionicons}
                name={Platform.OS ? 'ios-close' : 'md-close'}
                size="15"
                color="red"
              />
            </View>
            <View style={{ width: responsiveScreenWidth(80), height: responsiveScreenHeight(18), alignItems: 'center' }}>
              <Image source={require('@Public/Img/perfil.png')} style={{ width: responsiveScreenWidth(30), height: 100 }} />
              <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2) }}>Encuesta DentApp</Text>
              <Text>Por favor digita la siguiente información</Text>
            </View>
            <View style={{ width: responsiveScreenWidth(80), height: responsiveScreenHeight(27), alignItems: 'center' }}>


              <View style={{ width: responsiveScreenWidth(70), height: responsiveScreenHeight(20) }}>
                <View style={{ flexDirection: 'row', height: '30%', alignItems: 'center' }}>
                  <Text style={{ fontSize: 20 }}>{encuestas?.[index]?.informacion}</Text>
                </View>

                {encuestas?.[index]?.tipo === 'date' &&
                  <View style={{ flexDirection: 'row', height: '50%', alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={showDatepicker}>
                      <Text style={{ alignSelf: 'center', fontSize: 24, color: 'black', borderColor: '#64C2C8', borderTopWidth: 2, borderBottomWidth: 2, fontWeight: 'bold', paddingVertical: 10 }}>{date ? format(date, "dd - MM - yyyy") : 'Seleccione una fecha'}</Text>
                    </TouchableOpacity>
                  </View>
                }


                {encuestas?.[index]?.tipo === 'text' &&
                  <View style={{ flexDirection: 'row', height: '50%', alignItems: 'center', justifyContent: 'center' }}>
                    <Input placeholder="Ingresa tu respuesta aquí" onChangeText={(text) => setInputText(text)} value={inputText} />
                  </View>
                }

                {encuestas?.[index]?.tipo === 'radio' && encuestas?.[index]?.respuestas.map(respuesta =>
                  <View key={`${respuesta?.id_respuesta}-${respuesta?.informacion}`} style={{ flexDirection: 'row', height: '25%', alignItems: 'center' }}>
                    <Checkbox
                      status={checked.includes(respuesta?.informacion) ? 'checked' : 'unchecked'}
                      uncheckedColor="#64C2C8"
                      color="#64C2C8"
                      onPress={() => setChecked(respuesta?.informacion)}
                    />
                    <Text>{respuesta?.informacion}</Text>
                  </View>
                )
                }

                <View style={{ flexDirection: 'row', height: '20%', alignItems: 'center' }}>
                  <View style={{ width: '100%' }}>
                    <Text style={{ fontSize: 18, textAlign: 'right', color: '#64C2C8' }} onPress={() => onPressContinue(encuestas?.[index])}>Continuar</Text>
                  </View>
                </View>

                {show && (
                  <DateTimePicker
                    style={{
                      height: 100,
                      marginTop: 0
                    }}
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="spinner"
                    onChange={onChange}
                    customStyles={{
                      dateTouch: {
                        // height: 40,
                        width: '100%'
                      },
                      dateTouchBody: {
                        // marginTop:10,
                        backgroundColor: 'white',
                        width: '100%'
                      },
                      dateInput: {
                        borderRadius: 5,
                      },
                      placeholderText: {
                      },

                    }}
                  />
                )}


                <ModalComponent onClose={setDialog} {...dialog} />
              </View>


            </View>
          </View>
        </View>
      </Modal>
    ) : (
      <></>
    )
  );
};

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    bottom: 140,
    left: 150,
    padding: 3,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  modalView: {
    margin: 20,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 20,
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(50),
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
