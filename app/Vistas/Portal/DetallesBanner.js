import React, { useEffect, useState, Fragment } from "react";
import { View, StyleSheet, Modal, Button, ScrollView, Text, TouchableOpacity , Platform} from "react-native";
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ViewFile from '../../Components/ViewFile';
import { Request } from '../../api/api';
import { resolveImage } from '../../utils/utils';
import { Spinner } from "native-base";
import { Video, AVPlaybackStatus } from 'expo-av';
import * as ScreenOrientation from 'expo-screen-orientation';



export default function DetallesBanner({ route, navigation }) {
    const video = React.useRef(null);
    const { video: videoParams } = route.params;
    const [status, setStatus] = React.useState({});
    const [ loading, setLoading ] = useState(false);
    const [ localVideo, setLocalVideo ] = useState(null);

  

    const getVideo = async () => {
        try {
            setLoading(true);
            const response = await resolveImage([videoParams], 'banners/getVideo','archivo','banner/',true);
            if(response && response.length > 0){
                setLocalVideo(response[0]);
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    }

    useEffect(() => {
        getVideo();
    },[videoParams])
    return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center', textAlign: "center" }}>
            {
                loading ? <Spinner color='red' /> : (
                    <Fragment>
                        <Video
                        shouldPlay={true}
                            ref={video}
                            style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                bottom: 0,
                                right: 0,}}
                            source={{
                                uri: localVideo?.file,
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${localVideo?.token}`,
                                }
                            }}
                            useNativeControls
                            repeat={true}
                            resizeMode="contain"  
                           
                            isLooping
                            isBuffering
                            onPlaybackStatusUpdate={status => setStatus(() => status)}
                        />
                      
                    </Fragment>
                )
            }
        </View>
     
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

