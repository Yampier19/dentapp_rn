import React, {useState, useEffect, useCallback} from 'react'
import { Text, View, ScrollView, StyleSheet, TouchableOpacity, Modal, RefreshControl } from 'react-native'
import { Icon, Spinner, Button } from 'native-base';
import { Request } from '../../api/api';
import NotFound from '../../Components/NotFound';
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions";

export default function NotificacionScreen({route, navigation}) {
    const [loading, setLoading] = useState(false);
    const [notifications, setNotifications] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        getNotifications();
    }, []);

    useEffect(() => {
        getNotifications();
    }, [route])

    const getNotifications = async () => {
        try {
            setLoading(true);
            const response = await Request('GET', 'notificaciones/list');
            const data = await response.json();
            if(data?.notificaciones?.length > 0){
                setNotifications(data?.notificaciones);
            } else {
                setNotifications([]);
            }
            setLoading(false);
            setRefreshing(false)
        } catch (error) {
            setLoading(false);
            setRefreshing(false)
        }
    };
    const [estado, setEstado] = useState(false);

    const goTo = async (to, id) => {
        await read(id);
        // setEstado(!estado);
        if(to === 'Capacitación'){
            navigation.navigate('Capacitacion')
        }
        if(to === 'Noticia'){
            navigation.navigate('Noticias')
        }
        if(to === 'Puntos'){
            navigation.navigate('Nivel')
        }
        if(to === 'Factura'){
            navigation.navigate('HistorialFactura')
        }
    };

    const read = async (id) => {
        try {
            const response = await Request('GET', `notificaciones/read/${id}`);
            const data = await response.json();
            getNotifications();
        } catch (error) {}
    };

    const deleteall = async () => {
        try {
            const response = await Request('GET', `notificaciones/deleteall`);
            const data = await response.json();
            getNotifications();
        } catch (error) {}
    };
   

      
    return (
       
       
        <View style={{flex: 1, backgroundColor: '#fff'}}>

{/* <Text onPress={() => navigation.navigate('Push')} style={{color: 'red'}}>Hacer prueba de Push Notificacion</Text> */}
{loading && !refreshing  ? (
                <Spinner color={'red'} style={styles.spinner} />
            ) : (
                notifications.length > 0 ? (
                <View>


      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', alignItems: 'center'}}>
        <View style={{ width: '90%', height: 250, backgroundColor: 'white', justifyContent: 'flex-end', alignItems: 'center', borderRadius: 20 }}>
                                    <Icon name="check-circle" type="FontAwesome5" style={{ color: '#64C2C8', marginBottom: 20, fontSize: 50 }} />
                                    <Text style={{ fontSize: 20, marginBottom: '15%', marginHorizontal: 30, textAlign: 'center' }}>¿Seguro que quieres eliminar todas tus notificaciones?</Text>
                                   <View style={{flexDirection: 'row', alignContent: 'center', justifyContent: 'center'}}>
                                        <Button onPress={() => {
                                            deleteall();
                                            getNotifications();
                                            setModalVisible(!modalVisible)
                                        }} style={{ backgroundColor: '#E7344C', justifyContent: 'center', borderBottomLeftRadius: 20, width: '50%' }}>
                                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>SI</Text>
                                        </Button>
                                        <Button onPress={()=> setModalVisible(!modalVisible)} style={{ backgroundColor: '#64C2C8', justifyContent: 'center', borderBottomEndRadius: 20, width: '50%' }}>
                                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'white' }}>NO</Text>
                                        </Button>
                                    </View>
                                </View>
        </View>
      </Modal>
     
    

                        <View style={{width: responsiveScreenWidth(90), height: responsiveScreenHeight(10), alignSelf: 'center', justifyContent: 'center', marginVertical: responsiveScreenHeight(2)}}>
                            <Text style={{fontSize: responsiveScreenFontSize(2), fontWeight: 'bold', marginBottom: 30}}>Mis Notificaciones</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Text style={{color: 'gray', fontSize: responsiveScreenFontSize(1.5)}}>Hoy</Text>
                                <Icon style={{color: 'red', fontSize: responsiveScreenFontSize(1.8)}} onPress={() => setModalVisible(true)} type="FontAwesome5" name="trash" />
                            </View>
    
                        </View>
                
                        <View style={{width: responsiveScreenWidth(90), height: responsiveScreenHeight(66), alignSelf: 'center'}}>
                        <ScrollView
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                />
                            }
                        >  
                            <View style={{width: responsiveScreenWidth(90), height: responsiveScreenHeight(65)}}>
                                {
                                    notifications.length > 0 && notifications.map(notify =>(
                                        <TouchableOpacity onPress={() => goTo(notify?.notificacion?.tipo, notify?.notificacion?.id_notificaciones)} key={notify?.id_cliente_notificaciones} style={{width: responsiveScreenWidth(90), height: responsiveScreenHeight(9)}}>
                                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                            <Icon name="circle" type="FontAwesome" style={{fontSize: responsiveScreenFontSize(1), color: notify?.read_at ? 'gray' : 'black'}} />
                                            <Text style={{fontSize: responsiveScreenFontSize(1.5), marginLeft: responsiveScreenWidth(1), color: notify?.read_at ? 'gray' : 'black'}}>{notify?.notificacion?.descripcion}</Text>
                                        </View>
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        </ScrollView>
                    </View>
                </View>
           ) : (
            <NotFound message="No tienes notificaciones pendientes." />
        )
    )}
           
        
        </View>
         
    )
}

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
});

