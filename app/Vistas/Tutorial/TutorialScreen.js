import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Swiper from "react-native-swiper";

var styles = {
  wrapper: {},
};

class TutorialScreen extends React.Component {
  render() {
    return (
      
      <View style={{ backgroundColor: "black", flex: 1 }}>
        <Swiper
          loop={false}
          ref="swiper"
          style={styles.wrapper}
          showsButtons={false}
        >
          <View testID="Hello" style={{ flex: 1 }}>
            <ImageBackground
              source={require("@Public/Img/tutorial_1.png")}
              resizeMode="cover"
              style={{ flex: 1, alignItems: "center" }}
            >
              <ScrollView>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={require("@Public/Img/perfil.png")}
                    style={{ width: 200, height: 200, marginTop: "15%" }}
                  />
                  <Image
                    source={require("@Public/Img/DentApp.png")}
                    style={{ width: 200, height: 60 }}
                  />
                </View>
                <Text
                  style={{
                    color: "white",
                    fontSize: 20,
                    textAlign: "center",
                    marginTop: "50%",
                    marginHorizontal: "10%",
                  }}
                >
                  ¡Bienvenido/a a la comunidad! Disfruta de un espacio exclusivo
                  de premios y beneficios creado para ti.
                </Text>
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => this.refs.swiper.scrollBy(1)}
                    style={{
                      backgroundColor: "#E7344C",
                      borderRadius: 10,
                      paddingHorizontal: "15%",
                      paddingVertical: "2%",
                      marginVertical: "15%",
                    }}
                  >
                    <Text style={{ fontWeight: "bold", color: "white" }}>
                      SIGUIENTE
                    </Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </ImageBackground>
          </View>
          <View testID="Hello2" style={{ flex: 1 }}>
            <ImageBackground
              source={require("@Public/Img/tutorial_2.png")}
              resizeMode="cover"
              style={{ flex: 1, alignItems: "center" }}
            >
              <ScrollView>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={require("@Public/Img/perfil.png")}
                    style={{ width: 200, height: 200, marginTop: "15%" }}
                  />
                  <Image
                    source={require("@Public/Img/DentApp.png")}
                    style={{ width: 200, height: 60 }}
                  />
                </View>
                <Text
                  style={{
                    color: "white",
                    fontSize: 20,
                    textAlign: "center",
                    marginTop: "50%",
                    marginHorizontal: "10%",
                  }}
                >
                 Accede a noticias y contenido exclusivo de salud oral.
                </Text>
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => this.refs.swiper.scrollBy(1)}
                    style={{
                      backgroundColor: "#E7344C",
                      borderRadius: 10,
                      paddingHorizontal: "15%",
                      paddingVertical: "2%",
                      marginVertical: "15%",
                    }}
                  >
                    <Text style={{ fontWeight: "bold", color: "white" }}>
                      SIGUIENTE
                    </Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </ImageBackground>
          </View>
          <View testID="Hello3" style={{ flex: 1 }}>
            <ImageBackground
              source={require("@Public/Img/tutorial_3.png")}
              resizeMode="cover"
              style={{ flex: 1, alignItems: "center" }}
            >
              <ScrollView>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={require("@Public/Img/perfil.png")}
                    style={{ width: 200, height: 200, marginTop: "15%" }}
                  />
                  <Image
                    source={require("@Public/Img/DentApp.png")}
                    style={{ width: 200, height: 60 }}
                  />
                </View>
                <Text
                  style={{
                    color: "white",
                    fontSize: 20,
                    textAlign: "center",
                    marginTop: "50%",
                    marginHorizontal: "10%",
                  }}
                >
                 Gana puntos por tus compras y redime premios.
                </Text>
                <View style={{ alignItems: "center" }}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('WelcomeScreen')}
                    style={{
                      backgroundColor: "#E7344C",
                      borderRadius: 10,
                      paddingHorizontal: "15%",
                      paddingVertical: "2%",
                      marginVertical: "15%",
                    }}
                  >
                    <Text style={{ fontWeight: "bold", color: "white" }}>
                      INGRESAR A DENTAPP
                    </Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </ImageBackground>
          </View>
        </Swiper>
      </View>
    );
  }
}

export default TutorialScreen;
