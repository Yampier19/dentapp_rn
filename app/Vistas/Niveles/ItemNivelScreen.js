import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import BannerNivel from "./BannerNivel";

import { resolveImage } from "../../utils/utils";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";
import { useProfile, useProfileLevel } from "../../services/dentapp/user/useUser";


export default function ItemNivelScreen({ route, navigation }) {
  const { id } = route.params;
  const { profile } = route.params || {};
  const [score, setScore] = useState(0);
  const [level, setLevel] = useState(null);
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState(null);
  const { data: Profile } = useProfile();
  const { data: Level } = useProfileLevel();


  useEffect(() => {
    if (!profile) {
      getProfile();
    }
    getLevel();
  }, []);

  const getProfile = async () => {
    try {
      setLoading(true);
      const data = Profile
      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );
        setUser({
          ...currentData?.[0],
          password: data?.password,
          message: data?.message,
        });
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const getLevel = async () => {
    try {
      setLoading(true);
      const data = Level.data
      if (data?.status === "success" && data?.nivel) {
        setLevel(data);
        resolveScore(data?.dinero);
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const resolveScore = (dinero) => {
    const userScore = dinero || 0;
    if (userScore >= 6600000) {
      setScore(1);
    } else {
      let total = ((userScore * 1.2) / 6600000);
      setScore(total);
    }
  };

  if (id === 1) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>

          <View style={{ width: wp('100%'), height: 220 }}>
            <BannerNivel navigation={navigation} />
          </View>

          <View style={{ width: wp('100%'), height: 150, padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: wp('30%'), marginRight: 10 }}>
                <Image source={require('@Public/Img/MEDALLA_BRONCE.png')} style={{ width: 100, height: 100, marginTop: 20 }} />
              </View>
              <View style={{ width: wp('65%') }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#B78964' }}>BRONCE</Text>
                <Text style={{ marginVertical: 10 }}>De $350.000 a $1.499.000 en compras acumuladasde productos 3M seleccionados en tu distribuidor autorizado.</Text>
              </View>
            </View>
          </View>



        </ScrollView>
      </View>


    )
  }

  if (id === 2) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
       

          <View style={{ width: wp('100%'), height: 220 }}>
            <BannerNivel navigation={navigation} />
          </View>

          <View style={{ width: wp('100%'), height: 150, padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: wp('30%'), marginRight: 10 }}>
                <Image source={require('@Public/Img/MEDALLA_PLATA.png')} style={{ width: 100, height: 100, marginTop: 20 }} />
              </View>
              <View style={{ width: wp('65%') }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#B78964' }}>PLATA</Text>
                <Text style={{ marginVertical: 10 }}>De $1.500.000 a $1.949.000 en compras acumuladas de productos 3M seleccionados en tu distribuidor autorizado.</Text>
              </View>
            </View>
          </View>

       

        </ScrollView>

      </View>



    )
  }

  if (id === 3) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          

          <View style={{ width: wp('100%'), height: 220 }}>
            <BannerNivel navigation={navigation} />
          </View>

          <View style={{ width: wp('100%'), height: 150, padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: wp('30%'), marginRight: 10 }}>
                <Image source={require('@Public/Img/MEDALLA_ORO.png')} style={{ width: 100, height: 100, marginTop: 20 }} />
              </View>
              <View style={{ width: wp('65%') }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#B78964' }}>ORO</Text>
                <Text style={{ marginVertical: 10 }}>De $1.950.000 a $2.849.000 en compras acumuladas de productos 3M seleccionados en tu distribuidor autorizado.</Text>
              </View>
            </View>
          </View>

        </ScrollView>
      </View>

    )
  }

  if (id === 4) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>

          <View style={{ width: wp('100%'), height: 220 }}>
            <BannerNivel navigation={navigation} />
          </View>

          <View style={{ width: wp('100%'), height: 150, padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: wp('30%') }}>
                <Image source={require('@Public/Img/MEDALLA_ZAFIRO.png')} style={{ width: 100, height: 100, marginTop: 20 }} />
              </View>
              <View style={{ width: wp('65%') }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#B78964' }}>ZAFIRO</Text>
                <Text style={{ marginVertical: 10 }}>De $2.850.000 a $6.599.000 en compras acumuladas de productos 3M seleccionados en tu distribuidor autorizado.</Text>
              </View>
            </View>
          </View>

        </ScrollView>
      </View>



    )
  }

  if (id === 5) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
        

          <View style={{ width: wp('100%'), height: 220 }}>
            <BannerNivel navigation={navigation} />
          </View>

          <View style={{ width: wp('100%'), height: 150, padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: wp('30%'), marginRight: 10 }}>
                <Image source={require('@Public/Img/MEDALLA_DIAMANTE.png')} style={{ width: 100, height: 100, marginTop: 20 }} />
              </View>
              <View style={{ width: wp('65%') }}>
                <Text style={{ fontSize: 30, fontWeight: 'bold', color: '#B78964' }}>DIAMANTE</Text>
                <Text style={{ marginVertical: 10 }}>Más de $6.600.000 en compras acumuladas de productos 3M seleccionados en tu distribuidor autorizado.</Text>
              </View>
            </View>
          </View>

        </ScrollView>
      </View>
    )
  }



}