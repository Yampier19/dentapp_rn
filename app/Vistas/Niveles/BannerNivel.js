import * as React from 'react';
import {
  Text, 
  View,
  SafeAreaView, Image, TouchableOpacity } from 'react-native';

import Carousel from 'react-native-snap-carousel';

export default class App extends React.Component {

 
    constructor(props){
        super(props);
        this.state = {
          activeIndex:0,
          carouselItems: [
          {
            id: 1,
              title:"BRONCE",
              text: "Acumula $350.000 en compras y sé nivel Bronce.",
              uri: require('@Public/Img/MEDALLA_BRONCE.png'),
          },
          {
            id: 2,
              title:"PLATA",
              text: "Acumula $1.500.000 en compras y sé nivel Plata.",
              uri: require('@Public/Img/MEDALLA_PLATA.png'),
          },
          {
            id: 3,
              title:"ORO",
              text: "Acumula $1.950.000 en compras y sé nivel Oro.",
              uri: require('@Public/Img/MEDALLA_ORO.png'),
          },
          {
            id: 4,
              title:"ZAFIRO",
              text: "Acumula $2.850.000 en compras y sé nivel Zafiro.",
              uri: require('@Public/Img/MEDALLA_ZAFIRO.png'), 
          },

          {
            id: 5,
              title:"DIAMANTE",
              text: "Acumula $6.600.000 en compras y sé nivel Diamante.",
              uri: require('@Public/Img/MEDALLA_DIAMANTE.png'),
          },
        ]
      }
    }

    _renderItem = ({item,index}) => {
        return (
          <TouchableOpacity style={{
              backgroundColor:'#64C1C6',
              borderRadius: 20,
              height: 180,
              marginLeft: 10,
              padding: 10 }} onPress={() => this.props.navigation.navigate('ItemNivel', {id: item.id})}>
              
            <View style={{width: '100%', height: '100%'}}>
                <View style={{flexDirection: 'row'}}>
                  <Image style={{width: 50, height: 50}} source={item.uri} />
                  <Text style={{fontWeight: 'bold', fontSize: 30,  color: 'white', marginLeft: 10}}>{item.title}</Text>
                </View>
                <View style={{borderWidth: 1, width: '70%', marginLeft: 40, alignSelf: 'center', borderColor: 'white'}}></View>
                <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white', marginHorizontal: 20, marginTop: 20}}>{item.text}</Text>
                <Text style={{color: 'white', fontSize: 15, marginHorizontal: 20, marginTop: 10}}>VER BENEFICIOS</Text>

            </View>
             



          </TouchableOpacity>




        )
    }

    render() {
        return (
          <SafeAreaView style={{flex: 1, paddingTop: 10 }}>
            <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', }}>
                <Carousel
                  layout={"default"}
                  ref={ref => this.carousel = ref}
                  data={this.state.carouselItems}
                  sliderWidth={300}
                  itemWidth={300}
                  activeSlideAlignment={"start"}
                  renderItem={this._renderItem}
                  onSnapToItem = { index => this.setState({activeIndex:index}) } />
            </View>
          </SafeAreaView>
        );
    }
}
