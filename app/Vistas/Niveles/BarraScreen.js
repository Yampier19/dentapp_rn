import React from "react";
import { AppRegistry, StyleSheet, View } from "react-native";
import { ProgressBar } from 'react-native-paper';
import { Progress } from "native-base";

 
export const ScoreComponent = ({score = 0}) => {
  return (
    <>
      <Progress progress={score} style={{ backgroundColor: '#EDEDED' }} colorScheme="danger" size="lg" />
    </>
  );
}
 
 
AppRegistry.registerComponent("ScoreComponent", () => scoreComponent);