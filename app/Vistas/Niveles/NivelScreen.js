

import React, { useState, useEffect, useCallback } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  RefreshControl
} from "react-native";
import { VStack, Box, Divider, Avatar, NativeBaseProvider, Center, Progress, Spinner } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
  responsiveFontSize,
} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';

import BannerNivel from "./BannerNivel";
import { ScoreComponent } from "./BarraScreen";
import { resolveImage } from "../../utils/utils";
import { useProfile, useProfileLevel } from "../../services/dentapp/user/useUser";

export default function NivelScreen({ navigation, route }) {
  const { refresh } = route.params || {};
  const [profile, setProfile] = useState(null);
  const [loadingAvatar, setLoadingAvatar] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [score, setScore] = useState(0);
  const [level, setLevel] = useState(null);
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState(null);
  const { data: Profile } = useProfile();
  const { data: Level } = useProfileLevel();

  useEffect(() => {
    if (!profile) {
      getProfile();
    }
    getLevel();
    setLoading(true);
    getProfile();
  }, [refresh]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    getProfile();
    getLevel();
  }, []);

  const getLevel = async () => {
    try {
      setLoading(true);
      const data = Level.data
      if (data?.status === "success" && data?.nivel) {
        setLevel(data);
        resolveScore(data?.dinero);
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const resolveScore = (dinero) => {
    const userScore = dinero || 0;
    if (userScore >= 6600000) {
      setScore(1);
    } else {
      let total = ((userScore * 1.2) / 6600000);
      setScore(total);
    }
  };

  const getProfile = async () => {
    try {
      setLoading(true);
      const data = Profile
      if (data?.status === 'success' && data?.datos) {
        const currentData = await resolveImage([data?.datos], 'cliente/avatar', 'foto', 'uploads/', true, 'POST');
        setProfile({
          ...currentData?.[0],
          password: data?.password,
          message: data?.message
        });
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  return (

    <View style={{ flex: 1 }}>
      {loading && !refreshing ? (
        <Spinner color="red" style={styles.spinner} />
      ) : (
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }
        >
          <View style={{
            backgroundColor: '#E7334C',
            width: responsiveScreenWidth(100), height: responsiveScreenHeight(20), alignItems: 'center'
          }}>
            <View style={{ width: responsiveScreenWidth(100), flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
              <View style={{ width: responsiveScreenWidth(35), alignItems: 'center' }}>
                <TouchableOpacity >
                  {loadingAvatar ? <Spinner color="white" style={styles.spinner} /> : (

                    profile?.file ? (
                      <Avatar large source={{ uri: profile?.file }} style={{ width: 100, height: 100 }} />
                    ) : (
                      <Avatar large source={require('@Public/Img/DentApp.png')} style={{ width: 100, height: 100 }} />
                    )
                  )}
                  <View style={{ width: 35, height: 35, borderRadius: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', position: 'absolute', alignSelf: 'flex-end', bottom: 0 }}>
                    <Avatar source={require('@Public/Img/Niveles/plata.png')} />
                  </View>

                </TouchableOpacity>
              </View>
              <View style={{ width: responsiveScreenWidth(55), flexDirection: 'column' }}>
                <Text style={{ fontWeight: 'bold', color: 'white', fontSize: responsiveScreenFontSize(2.5) }}>{profile?.nombre}</Text>
                <Text style={{ color: 'white', fontSize: responsiveScreenFontSize(2) }}>{profile?.total_estrellas.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")} puntos</Text>
              </View>
            </View>
          </View>
          <View style={{ width: responsiveScreenWidth(90), justifyContent: 'center', alignItems: 'center', flexDirection: 'row', alignSelf: 'center', top: -25 }}>
            <View>
              <Box rounded="lg" style={{ justifyContent: 'center', alignItems: 'center', width: responsiveScreenWidth(20), height: responsiveScreenHeight(8), backgroundColor: 'white' }}>
                <Avatar large source={require('@Public/Img/Niveles/bronce.png')} />
                <Text style={{ color: 'gray', fontSize: 10 }}>Nivel 1</Text>
              </Box>
              <Box style={{ marginTop: 10, alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold' }}>Bronce</Text>
                <Text style={{ color: 'gray', fontSize: 10 }}>0 pts</Text>
              </Box>
            </View>
            <View>
              <Box rounded="lg" style={{ justifyContent: 'center', alignItems: 'center', width: responsiveScreenWidth(20), height: responsiveScreenHeight(8), backgroundColor: 'white', marginHorizontal: 10 }}>
                <Avatar large source={require('@Public/Img/Niveles/plata.png')} />
                <Text style={{ color: 'gray', fontSize: 10 }}>Nivel 2</Text>
              </Box>
              <Box style={{ marginTop: 10, alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold' }}>Plata</Text>
                <Text style={{ color: 'gray', fontSize: 10 }}>1.000 pts</Text>
              </Box>
            </View>
            <View>
              <Box rounded="lg" style={{ justifyContent: 'center', alignItems: 'center', width: responsiveScreenWidth(20), height: responsiveScreenHeight(8), backgroundColor: 'white' }}>
                <Avatar large source={require('@Public/Img/Niveles/oro.png')} />
                <Text style={{ color: 'gray', fontSize: 10 }}>Nivel 3</Text>
              </Box>
              <Box style={{ marginTop: 10, alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold' }}>Oro</Text>
                <Text style={{ color: 'gray', fontSize: 10 }}>3.000 pts</Text>
              </Box>
            </View>
            <View>
              <Box rounded="lg" style={{ justifyContent: 'center', alignItems: 'center', width: responsiveScreenWidth(20), height: responsiveScreenHeight(8), backgroundColor: 'white', marginLeft: 10 }}>
                <Avatar large source={require('@Public/Img/Niveles/diamante.png')} />
                <Text style={{ color: 'gray', fontSize: 10 }}>Nivel 4</Text>
              </Box>
              <Box style={{ marginTop: 10, alignItems: 'center' }}>
                <Text style={{ fontWeight: 'bold' }}>Diamante</Text>
                <Text style={{ color: 'gray', fontSize: 10 }}>6.000 pts</Text>
              </Box>
            </View>
          </View>
          <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }}>
            <Box >
            <ScoreComponent score={score} />
            </Box>
          </View>
          <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center', marginVertical: 30 }}>
            <Box style={{ width: responsiveScreenWidth(90), backgroundColor: 'white', flexDirection: 'row' }} rounded="lg" p="1">
              <View style={{ width: responsiveScreenWidth(10), alignItems: 'center', justifyContent: 'center' }}>
                <Avatar size="xs" source={require('@Public/Img/up.png')} />
              </View>
              <View style={{ width: responsiveScreenWidth(74), padding: 5 }}>
                <Text style={{ color: 'gray' }}>Te faltan mas en compras para subir al nivel {level?.proximo_nivel}</Text>
                <Text style={{ color: '#65C1C7' }} onPress={() => navigation.navigate('Facturas')}>¡Continúa subiendo más facturas!</Text>
              </View>
            </Box>
          </View>
          <View style={{ marginBottom: 20 }}>
            <BannerNivel navigation={navigation} />
          </View>
        </ScrollView>
      )}
    </View>
  );
}


const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
})