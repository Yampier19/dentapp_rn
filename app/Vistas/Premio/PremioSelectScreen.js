import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Modal,
  Pressable,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";

export default function PremioSelectScreen({ route, navigation }) {
  const { premio, profile } = route.params || {};
  const [modalVisible, setModalVisible] = useState(false);

  const onPressCanjear = () => {
    if (premio?.distribuidor === "Quantum" && premio?.producto_id) {
      navigation.navigate("CanjearQuantum", { premio, profile });
    } else  {
      if (premio?.entrega === "Domicilio") {
        navigation.navigate("CanjearDomicilio", { premio, profile });
      } else {
        navigation.navigate("CanjearOnline", { premio, profile });
      }
    }
  };

  const openImage = () => {
    if (premio?.file) {
      setModalVisible(true);
    } else {
      notificationAlert(
        "No tiene archivo",
        "El Premio no tiene una imágen para mostrar."
      );
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <ScrollView
        style={{
          height: hp("100%"),
          width: wp("100%"),
          paddingHorizontal: "5%",
          alignSelf: "center",
        }}
      >
        <View style={{ marginBottom: 20 }}>
          <Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 20 }}>
            {premio?.nombre}
          </Text>
        </View>
        <TouchableOpacity
          onPress={openImage}
          style={{ width: wp("43%"), height: hp("30%"), alignSelf: "center" }}
        >
          <ImageBackground
            source={{ uri: premio?.file }}
            style={{ flex: 1, resizeMode: "cover" }}
          ></ImageBackground>
        </TouchableOpacity>
        <View style={{ marginVertical: 30 }}>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            {premio?.numero_estrellas
              .toString()
              .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}{" "}
            Puntos
          </Text>
        </View>
        <TouchableOpacity
          accessible={true}
          disabled={
            Number(profile?.total_estrellas) <= Number(premio?.numero_estrellas)
          }
          accessibilityLabel="Canjear un premio"
          accessibilityHint="Canjear premio"
          onPress={onPressCanjear}
          style={{
            padding: "3%",
            backgroundColor:
              Number(profile?.total_estrellas) <=
              Number(premio?.numero_estrellas)
                ? "#E1E1E1"
                : "#E7344C",
            borderRadius: 10,
          }}
        >
          <Text style={{ color: "white", textAlign: "center", fontSize: 16 }}>
            CANJEAR PREMIO
          </Text>
        </TouchableOpacity>
        <View style={{ marginVertical: 30 }}>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>Entrega</Text>
          <Text style={{ fontSize: 16 }}>{premio?.entrega}</Text>
        </View>
        <View>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>
            Informacion del producto
          </Text>
          <Text style={{ fontSize: 16, marginVertical: 20 }}>
            {premio?.descripcion}
          </Text>
          {console.log('premio', premio)}
        </View>
      </ScrollView>
      <Modal
        animationType="fade"
        presentationStyle="fullScreen"
        visible={modalVisible}
        statusBarTranslucent
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ImageBackground
              style={{ flex: 1, resizeMode: "cover" }}
              source={{ uri: premio?.file }}
            ></ImageBackground>
            <Pressable
              style={{
                padding: "3%",
                backgroundColor: "#E7344C",
                borderRadius: 10,
                marginVertical: "5%",
              }}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text
                style={{ color: "white", textAlign: "center", fontSize: 16 }}
              >
                Cerrar
              </Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  box_content: {
    backgroundColor: "white",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(35),
    borderRadius: 10,
    alignItems: "center",
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(12),
  },
  icon: {
    color: "#E7344C",
    fontSize: responsiveScreenFontSize(8),
  },
  box_text: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(15),
  },
  text: {
    fontSize: responsiveScreenFontSize(3),
    marginHorizontal: responsiveScreenWidth(5),
    textAlign: "center",
  },
  box_buttom: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "flex-end",
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttom: {
    backgroundColor: "#64C2C8",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(6),
    justifyContent: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: "center",
    color: "white",
  },
  modalView: {
    backgroundColor: "white",
    width: wp("83%"),
    height: hp("90%"),
    padding: 20,
    borderRadius: 20,
  },
});
