import React, { Fragment, useState } from "react";
import { Text, TouchableOpacity } from "react-native";
import DateTimePicker from '@react-native-community/datetimepicker';


const DateTimePickerCustom = (value, onChange) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isDateSet, setIsDateSet] = useState(false);

  const [date, setDate] = useState(new Date());


  const showDateTimePicker = () => {
    setDatePickerVisibility(true);

  };
  const onChanged = (date) => {
    value.onChange(date);
    setDatePickerVisibility(false);
  }

  const onDateSelected = (valuefuntion) => {
    setDate(valuefuntion);
    setIsDateSet(true);
  };

  const onDateSelectedButton = async (event, valuefuntion) => {
    await onDateSelected(valuefuntion);
    if (isDateSet) {
      onChanged(date);
    }
  };


  return (
    <Fragment>
      <Text style={{fontSize: 16}}>Fecha de nacimiento</Text>
      <TouchableOpacity style={{ marginTop: 10 }} onPress={showDateTimePicker} >
        {value
          ? <Text style={{ fontSize: 15, textAlign: 'left', marginRight: 220, width: '100%' }}>{date.toDateString()}</Text>
          : <Text style={{ fontSize: 15, textAlign: 'left', marginRight: 60, width: '100%' }}>Selecciona tu fecha de nacimiento...</Text>
        }
      </TouchableOpacity>
      {isDatePickerVisible && (
        <DateTimePicker
          value={date}
          mode={"date"}
          display={Platform.OS === 'ios' ? 'spinner' : 'default'}
          is24Hour={true}
          onChange={onDateSelectedButton}
        />
      )}
    </Fragment>
  )
}

export default DateTimePickerCustom;