
import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";


const PickerSitio = ({ onChange, value, options = [], enabled }) => {

  return (
    <Select
          placeholder="Selecciona tu sitio"
        onValueChange={onChange}
        selectedValue={value}
        enabled={enabled}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
          <Select.Item label="Selecciona tu sitio" value="" />
        {options.map((option, index) => (
          <Select.Item
            label={option.site_name}
            value={option.site_id}
            key={index}
          />
        ))}
    </Select>
  );
};

export default PickerSitio;
