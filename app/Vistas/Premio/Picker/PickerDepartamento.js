
import React, {useState} from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";


const PickerDepartamento = ({ onChange, options = [], enabled }) => {
  const [value, setValue] = useState(null);
  const handleChange = (data) => {
    setValue(data);
    onChange(data);
  };
  return (
    <Select
    placeholder="Selecciona tu departamento"
    onValueChange={handleChange}
    selectedValue={value}
       enabled={enabled}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
         <Select.Item label="Selecciona tu departamento" value="" />
        {options.map((item, index) => {
          return (
            <Select.Item
              label={item.dep_name}
              value={item.dep_id}
              key={index}
            />
          );
        })}
    </Select>
  );
};

export default PickerDepartamento;