import React from "react";
import { Icon, Select } from 'native-base';
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";

const PickerCiudad = ({ onChange, value, options=[], enabled}) => {
  return (
    <Select
       placeholder="Selecciona tu ciudad"
       onValueChange={onChange}
       selectedValue={value}
       enabled={enabled}
        width={responsiveScreenWidth(90)}
        height={responsiveScreenHeight(6)}
        fontSize={15}
    >
        <Select.Item label="Selecciona tu ciudad" value="" />
          {options.map((option, index) => (
            <Select.Item label={option.city_name} value={option.city_id} key={index} />
          ))}
    </Select>
  );
};

export default PickerCiudad;
