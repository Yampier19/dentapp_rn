import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Modal,
  TouchableOpacity,
} from "react-native";
import {
  Button,
  Icon,
  TextArea,
  Spinner,
} from "native-base";
import {TextInput} from 'react-native-paper'

import s from "@Public/Css/style";
import { Request } from "../../api/api";
import ModalComponent from "../../Components/Modal";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { When } from "react-if";
import { Unless } from "react-if";

import { useAwardExchange } from "../../services/dentapp/awards/useAwards";
import { useExchangeAwardProduct } from "../../services/dentapp/navident/useNaviDent";
import { useExchangeAwardCena } from "../../services/dentapp/navident/useNaviDent";

export default function CanjearScreen({ route, navigation }) {
  const { premio, profile, campaign, id_navidad, type } = route.params || {};
  const [modalVisible, setModalVisible] = useState(false);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const [data, setdata] = useState({
    receptor: profile.nombre || "",
    telefono: profile.telefono || "",
    cantidad: 1,
    direccion: profile.direccion || "",
    adicional: "Sin adicional",
    cedula: "",
    email: profile.correo || "",
  });
  const [errorEmptyForm, setErrorEmptyForm] = useState(false);
  const {
    mutateAsync: awardExchange,
    isLoading,
    data: response,
  } = useAwardExchange();
  const {
    mutateAsync: awardExcahangeProduct,
    isLoading: loadingNavident,
    data: responseNavident,
  } = useExchangeAwardProduct();
  const {
    mutateAsync: awardExcahangeCena,
    isLoading: loadingNavidentCena,
    data: responseNavidentCena,
  } = useExchangeAwardCena();

  useEffect(() => {
    validField();
  }, [data]);

  const onChangeText = (name, value) => {
    setdata({
      ...data,
      [name]: value,
    });
  };

  const validField = () => {
    Object.values(data).forEach((value) => {
      if (value === "" || value === null || value === undefined) {
        setErrorEmptyForm(true);
      } else {
        setErrorEmptyForm(false);
      }
    });
  };

  const onClick = async () => {
    try {
      if (!errorEmptyForm) {
        if (data.cedula.length > 4 && data.cedula.length < 20) {
          if (data.receptor && data.direccion && data.telefono) {
            let payload = {
              cantidad: 1,
              cedula: data.cedula,
              receptor: data.email,
              telefono: data.telefono,
              direccion: data.direccion,
              adicional: data.adicional,
            };
            let response = null;
            if (campaign === "Navident") {
              if (type === "cena") {
                response = await awardExcahangeCena({
                  id: id_navidad,
                  data: payload,
                });
              } else {
                response = await awardExcahangeProduct({
                  id: id_navidad,
                  data: payload,
                });
              }
            } else {
              response = await awardExchange({
                award: payload,
                awardId: premio.id_premio,
              });
            }
            if (response !== null) {
              if (response?.status === "success") {
                setModalVisible(true);
              } else {
                setDialog({
                  visible: true,
                  message:
                    response?.message ||
                    "Ocurrió un error al momento de procesar los datos, intentelo nuevamente.",
                  type: "error",
                });
              }
            }
          } else {
            setDialog({
              visible: true,
              message:
                "Tienes campos vacíos, por favor verifique e intentelo nuevamente.",
              type: "warning",
            });
          }
        } else {
          setDialog({
            visible: true,
            message:
              "La cedula debe tener mas de 4 caracteres y menos a 20 caracteres",
            type: "warning",
          });
        }
      } else {
        setDialog({
          visible: true,
          message:
            "Tienes campos vacíos, por favor verifique e intentelo nuevamente.",
          type: "warning",
        });
      }
    } catch (error) {}
  };

  const onPressClose = () => {
    setModalVisible(false);
    navigation.navigate("DetallesDomicilio", {
      response: response || responseNavident || responseNavidentCena,
      premio,
      data,
    });
  };

  return (
    <View style={s.container}>
      <When condition={!isLoading || !loadingNavident || !loadingNavidentCena}>
        <ScrollView style={{ backgroundColor: "white" }}>
          <View style={[styles.content]}>
            <View style={[styles.box1]}>
              <Text
                style={{
                  marginHorizontal: 20,
                  fontSize: 20,
                  fontWeight: "bold",
                }}
              >
                Registra tus datos de entrega exactos para recibir tu premio.
              </Text>
              <Text
                style={{
                  alignSelf: "flex-end",
                  marginTop: 20,
                  marginHorizontal: 20,
                  fontSize: 15,
                  fontWeight: "bold",
                  color: "gray",
                }}
              >
                Datos de domicilio
              </Text>
              <View
                style={{
                  backgroundColor: "#F5F5F5",
                  width: "90%",
                  height: 910,
                  alignSelf: "center",
                  padding: 10,
                  borderRadius: 10,
                  marginVertical: 20,
                }}
              >
                <View >
                  <Text>Nombres</Text>
                  <TextInput
                    onChangeText={(text) => onChangeText("receptor", text)}
                    value={data.receptor}
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    placeholder="Andres Felipe"
                  />
                </View>
                <View  style={{ marginVertical: 20 }}>
                  <Text>Correo</Text>
                  <TextInput
                    onChangeText={(text) => onChangeText("email", text)}
                    value={data.email}
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    placeholder="Correo"
                  />
                </View>
                <View >
                  <Text>Teléfono</Text>
                  <TextInput
                    onChangeText={(text) => onChangeText("telefono", text)}
                    value={data.telefono}
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    placeholder="777-77-77"
                  />
                </View>
                <View >
                  <Text>Dirección del domicilio</Text>
                  <TextInput
                    onChangeText={(text) => onChangeText("direccion", text)}
                    value={data.direccion}
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    placeholder="Cra 68 a # 56 - 28 Sur"
                  />
                </View>
                <View  style={{ marginVertical: 20 }}>
                  <Text>Número de identificación</Text>
                  <TextInput
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    keyboardType="phone-pad"
                    placeholder="12312525"
                    //value="1231215"
                    value={data.cedula}
                    onChangeText={(text) => onChangeText("cedula", text)}
                  />
                </View>
                <TextArea
                  onChangeText={(text) => onChangeText("adicional", text)}
                  value={data.adicional}
                  style={{ padding: 10 }}
                  rowSpan={5}
                  bordered
                  placeholder="Informacion adicional"
                />
              </View>
            </View>
            <View style={[styles.box2]}>
              <View style={{ marginHorizontal: 25 }}>
                <Button
                  onPress={onClick}
                  block
                  style={{
                    width: 250,
                    marginVertical: 40,
                    alignSelf: "center",
                    backgroundColor: "#64C2C8",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: "bold",
                      color: "#ffffff",
                    }}
                  >
                    Confirmar
                  </Text>
                </Button>
              </View>
            </View>
          </View>
          <ModalComponent onClose={setDialog} {...dialog} />
        </ScrollView>
      </When>
      <Unless
        condition={!isLoading || !loadingNavident || !loadingNavidentCena}
      >
        <Spinner color="red" style={styles.spinner} />
      </Unless>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.box_content}>
            <View style={styles.box_icon}>
              <Icon name="gift" type="FontAwesome5" style={styles.icon} />
            </View>
            <View style={styles.box_text}>
              <Text style={styles.text}>¡Tu premio ha sido canjeado!</Text>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(2),
                  marginHorizontal: 30,
                  textAlign: "center",
                  marginTop: 10,
                }}
              >
                Te contactaremos en las próximas 24h para realizar la entrega
              </Text>
            </View>
            <View style={styles.box_buttom}>
              <TouchableOpacity style={styles.buttom} onPress={onPressClose}>
                <Text style={styles.text_buttom}>CERRAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignViews: "center",
  },
  container2: {
    flex: 1,
    paddingTop: 22,
  },
  View: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignViews: "center",
    backgroundColor: "white",
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 650,
    marginTop: 30,
  },
  box2: {
    width: "100%",
    height: 120,
    marginBottom: 10,
    marginTop: 10,
  },
  box3: {
    width: "85%",
    height: 200,
    marginTop: 10,
  },
  box4: {
    width: "85%",
    height: 200,
  },

  box5: {
    width: "100%",
    height: 50,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignViews: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  box_content: {
    backgroundColor: "white",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(35),
    borderRadius: 10,
    alignViews: "center",
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignViews: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(12),
  },
  icon: {
    color: "#E7344C",
    fontSize: responsiveScreenFontSize(8),
  },
  box_text: {
    width: responsiveScreenWidth(90),
    alignViews: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(15),
  },
  text: {
    fontSize: responsiveScreenFontSize(3),
    marginHorizontal: responsiveScreenWidth(5),
    textAlign: "center",
  },
  box_buttom: {
    width: responsiveScreenWidth(90),
    alignViews: "center",
    justifyContent: "flex-end",
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttom: {
    backgroundColor: "#64C2C8",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(6),
    justifyContent: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: "center",
    color: "white",
  },
});
