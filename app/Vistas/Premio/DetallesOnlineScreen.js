import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native'
import { Spinner } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { When } from "react-if";
import { Unless } from "react-if";

import { useProfile } from "../../services/dentapp/user/useUser";

export default function DetallesDomicilioScreen({route, navigation}) {
    const {response, premio} = route.params || {};
    const [profile, setProfile] = useState(null);
    const [loading, setLoading] = useState(false);
    const { data: Profile } = useProfile();

    useEffect(() => {
        getProfile()
    }, [response])

    const getProfile = async () => {
        try {
            setLoading(true);
            const data = Profile;
            if(data?.status === 'success' && data?.datos){
                setProfile(data?.datos)
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    };

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <When condition={!loading}>
                <ScrollView style={{height: hp('100%'), width: wp('100%'), paddingHorizontal: '5%', alignSelf: 'center'}}>
                    {/* <View style={{alignSelf: 'flex-end', marginVertical: 20}}>
                        <Text style={{color: 'gray'}}>01/01/1999</Text>
                    </View> */}
                    <View style={{width: wp('43%'), height: hp('30%'), alignSelf: 'center'}} >
                        <ImageBackground source={{uri: premio?.file}} style={{
                            flex: 1,
                            resizeMode: "cover",
                        }} >
                        </ImageBackground>
                    </View>
                    <View style={{marginTop: 30}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>¡Felicidades!</Text>
                        <Text style={{fontSize: 16}}>{profile?.nombre} {profile?.apellido}</Text>
                    </View>
                    <View style={{marginTop: 20}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Has canjeado {premio?.numero_estrellas.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") || 0} puntos en uno de nuestros premios ({premio?.nombre}).</Text>
                    </View>
                    <View style={{marginVertical: 30}}>
                        <Text style={{fontSize: 16}}>No olvides seguir cargando facturas para acumular muchos puntos más.</Text>
                    </View>
                    <TouchableOpacity 
                        accessible={true}
                        accessibilityLabel="Canjear un premio"
                        accessibilityHint="Canjear premio" 
                        onPress={() => {navigation.navigate('PremioOfertaScreen')}} 
                        style={{padding: '3%',backgroundColor: '#E7344C', borderRadius: 10, marginTop: 20}}
                    >
                            <Text style={{color: 'white', textAlign: 'center', fontSize: 16}}>SEGUIR CANJEANDO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        accessible={true}
                        accessibilityLabel="Canjear un premio"
                        accessibilityHint="Canjear premio" onPress={() => {navigation.navigate('Nivel', {profile})}} 
                        style={{width: wp('75%'), alignSelf: 'center', marginVertical: 20, padding: '3%',backgroundColor: '#64C2C8', borderRadius: 10}}
                    >
                        <Text style={{color: 'white', textAlign: 'center', fontSize: 16}}>MI PROGRESO</Text>
                    </TouchableOpacity>
                </ScrollView>
            </When>
            <Unless condition={!loading}>
                <Spinner color="red" style={styles.spinner} />
            </Unless>
        </View>
    )
}

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    container2: {
        flex: 1,
        paddingTop: 22
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
    box1: {
        width: '100%',
        height: 600,
        marginTop: 10
    },
    box2: {
        width: '100%',
        height: 150,
        marginBottom: 10,
        marginTop: 10
    }
});
