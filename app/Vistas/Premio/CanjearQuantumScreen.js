import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Modal,
  Pressable,
  TouchableOpacity,
  Alert
} from "react-native";
import {
  Button,
  Spinner,
  Icon,
  TextArea,
} from "native-base";
import { TextInput } from "react-native-paper";
import { When } from "react-if";
import { Unless } from "react-if";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";
import { useListDepartments } from "../../services/dentapp/quantum/useQuantum";
import { useListCities } from "../../services/dentapp/quantum/useQuantum";
import { useListSities } from "../../services/dentapp/quantum/useQuantum";
import { useVerifyExist } from "../../services/dentapp/quantum/useQuantum";
import { useRedention } from "../../services/dentapp/quantum/useQuantum";
import { useExchangeAward } from "../../services/dentapp/navident/useNaviDent";
import { useChangeAwardByPoints } from "../../services/dentapp/navident/useNaviDent";
import { Request } from "../../api/api";
import ModalComponent from "../../Components/Modal";
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import s from "@Public/Css/style";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import PickerCiudad from "./Picker/PickerCiudad";
import PickerDepartamento from "./Picker/PickerDepartamento";
import PickerSitio from "./Picker/PickerSitio";

export default function CanjearQuantumScreen({ route, navigation }) {
  const { premio, profile, campaign, id_navidad, typeModal } =
    route.params || {};
  const [loading, setLoading] = useState(false);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const [form, setForm] = useState({
    cantidad: 1,
    receptor: profile?.nombre || "",
    telefono: profile?.telefono || "",
    cedula: "",
    direccion: profile?.direccion || "",
    adicional: "Sin adicional",
    brand_id: premio?.brand_id,
    product_id: premio?.producto_id,
    site_id: null,
    birthdate: "2000-01-01",
    email: profile?.correo || "",
    city_id: "",
  });
  const [departId, setDepartId] = useState(null);
  const [cityId, setCityId] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);


  const [modalVisible3, setModalVisible3] = useState(false);
  const [messageDisponibility, setMessageDisponibility] = useState("");
  const [modalVisible2, setModalVisible2] = useState(false);
  const [errorEmptyForm, setErrorEmptyForm] = useState(false);
  const [existDeparment, setExistDeparment] = useState(false);
  const [existCity, setExistCity] = useState(false);
  const [existSitie, setExistSitie] = useState(false);
  const [enabledButtonRendention, setEnabledButtonRendention] = useState(true);
  const { data: departments } = useListDepartments(premio?.brand_id);
  const { mutateAsync: VerifyExist, isLoading: isLoadingVerify } =
    useVerifyExist();
  const { mutateAsync: Redention } = useRedention();
  const { mutateAsync: RedentionNavidad } = useExchangeAward();
  const { mutateAsync: ChangeByPoints, isLoading: isLoadingChange } =
    useChangeAwardByPoints();
  const {
    data: cities,
    refetch,
    remove,
  } = useListCities({
    brand: premio?.brand_id,
    department: departId,
  });

  const {
    data: sities,
    refetch: refetchSities,
    remove: removeSities,
  } = useListSities({
    brand: premio?.brand_id,
    department: departId,
    city: cityId,
  });

  useEffect(() => {
    validField();
  }, [form, departId, cityId]);

  useEffect(() => {
    validExistDepartment();
    refetch();
  }, [departments]);

  useEffect(() => {
    refetch();
    validExistCity();
    refetchSities();
  }, [departId, cities]);

  useEffect(() => {
    validExistSities();
    refetchSities();

  }, [departId, cityId, sities]);

  const onChangeText = (name, value) => {
    setForm({
      ...form,
      [name]: value,
    });
    console.log("fecha" + form.birthdate)
  };

  const onPressCanjear = async () => {
    setEnabledButtonRendention(true);
    if (!errorEmptyForm) {
      if (form.cedula.length > 4 && form.cedula.length < 20) {
        if (form.receptor && form.telefono) {
          const payload = {
            cantidad: form?.cantidad,
            cedula: Number(form?.cedula),
            receptor: form?.receptor,
            telefono: form?.telefono,
            direccion: form?.direccion,
            adicional: form?.adicional,
            brand_id: form?.brand_id,
            product_id: form?.product_id,
            site_id: form?.site_id || null,
            email: form?.email,
            birthdate: "2000-01-01",
          };
          try {
            setLoading(true);
            let data = null;
            if (campaign === "Navident") {
              data = await RedentionNavidad({ id: id_navidad, data: payload });

            } else {
              data = await Redention({ id: premio?.id_premio, data: payload });
            }
            if (data !== null) {
              if (data?.status === "success") {
                navigation.navigate("DetallesOnline", {
                  response: data,
                  premio,
                });
                setModalVisible2(!modalVisible2);
                setModalVisible(true);
              } else {
                setDialog({
                  visible: true,
                  message:
                    data?.message ||
                    "Ocurrió un error al momento de canjear el premio, vuelva a intentarlo",
                  type: "error",
                });
                setEnabledButtonRendention(false);
              }
              setLoading(false);
            }
          } catch (error) {
            setLoading(false);
          }
        } else {
          setDialog({
            visible: true,
            message: "Tiene campos vacios, por favor verifique.",
            type: "warning",
          });
        }
      } else {
        setDialog({
          visible: true,
          message:
            "La cedula debe tener mas de 4 caracteres y menos a 20 caracteres",
          type: "warning",
        });
      }
    } else {
      setDialog({
        visible: true,
        message: "Tiene campos vacios, por favor verifique.",
        type: "warning",
      });
    }
  };

  const onPressCheckDisponibility = async () => {

    if (!errorEmptyForm) {

      if (form.cedula.length > 4 && form.cedula.length < 20) {
        if (form.receptor && form.telefono) {
          try {
            setLoading(true);
            setModalVisible2(true);
            const data = await VerifyExist({
              brand_id: form.brand_id,
              product_id: form.product_id,
              user_data: { email: form.email },
              site_id: form.site_id,
            });

            if (data?.status === "success") {
              setEnabledButtonRendention(false);
              setMessageDisponibility(data?.message);
            } else {
              setEnabledButtonRendention(true);
              setMessageDisponibility(
                "No esta disponible el premio para redencion."
              );
            }
            setLoading(false);
          } catch (error) {
            setLoading(false);
          }
        } else {
          setDialog({
            visible: true,
            message: "Tiene campos vacios, por favor verifique.",
            type: "warning",
          });
        }
      } else {
        setDialog({
          visible: true,
          message:
            "La cedula debe tener mas de 4 caracteres y menos a 20 caracteres",
          type: "warning",
        });
      }
    } else {
      setDialog({
        visible: true,
        message: "Tiene campos vacios, por favor verifique.",
        type: "warning",
      });
    }
  };
  
  const validExistDepartment = () => {

    if (
      departments?.status === "error" &&
      departments?.message ===
      "[1600]Error, No existen Sitios para los datos suministrados"
    ) {
      setExistDeparment(false);
      refetch();
      Alert.alert("Premio no disponible");
    } else if (departments?.status === "success") {
      remove("quantum/listCities");
      setExistDeparment(true);
      Alert.alert("Premio disponible");
    }

  };

  const validExistCity = () => {

    if (
      cities?.status === "error" &&
      cities?.message ===
      "[1600]Error, No existen Sitios para los datos suministrados"
    ) {
      setExistCity(false);
    } else if (cities?.status === "success") {

      setExistCity(true);
    }
  };

  const validExistSities = () => {

    if (
      sities?.status === "error" &&
      sities?.message ===
      "[1600]Error, No existen Sitios para los datos suministrados"
    ) {
      setExistSitie(false);
      refetchSities();
    } else if (sities?.status === "success") {
      setExistSitie(true);
    }
  };

  const validField = () => {
    let error = 0;
    Object.entries(form).forEach((value) => {
      if (value[1] === "" || value[1] === null || value[1] === undefined) {
        if (value[0] === "site_id" && value[1] === null && !existDeparment) {
          error;
        } else {
          error++;
        }
      } else {
        error;
      }
    });
    if (error > 1) {
      setErrorEmptyForm(true);
    } else {
      setErrorEmptyForm(false);
    }
  };

  const handleChangeDepartment = (data) => {
    remove("quantum/listCities");
    setDepartId(data);
  };

  const handleChangecity = (data) => {
    setCityId(data);
    removeSities("quantum/listSities");
  };

  const changeAwardByPoints = () => {
    ChangeByPoints(id_navidad).then((res) => {
      if (res.status === "success") {
        navigation.navigate("Home");
        setModalVisible3(!modalVisible3);
      }
    });
  };

  return (
    <View style={s.container}>
      {loading ? (
        <Spinner color="red" style={styles.spinner} />
      ) : (
        <ScrollView>
          <View style={[styles.content]}>
            <Text
              style={{
                marginHorizontal: 20,
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              ¡Confirma tus datos para reclamar tu premio!
            </Text>
            <Text
              style={{
                alignSelf: "flex-end",
                marginTop: 30,
                marginHorizontal: 20,
                fontSize: 15,
                fontWeight: "bold",
                color: "gray",
                marginBottom: 20
              }}
            >
              Datos de domicilio
            </Text>

            <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
            >
              <TextInput
                onChangeText={(text) => onChangeText("receptor", text)}
                value={form.receptor}
                placeholder="Andres Felipe"
              />
            </View>
            <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
            >
              <TextInput
                onChangeText={(text) => onChangeText("email", text)}
                value={form.email}
                placeholder="Correo"
              />
            </View>
            <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
            >
              <TextInput
                onChangeText={(text) => onChangeText("telefono", text)}
                value={form.telefono}
                placeholder="777-77-77"
              />
            </View>

            {typeModal === "register" ? (
                <Item stackedLabel>
                  <View
                    style={{
                      width: responsiveScreenWidth(80),
                      alignItems: "flex-end",
                      justifyContent: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: responsiveScreenFontSize(1.5),
                        top: 20,
                      }}
                    >
                      ¿No encuentras tu departamento?
                    </Text>
                    <Icon
                      name="info-circle"
                      type="FontAwesome"
                      style={{ marginBottom: 10 }}
                      onPress={() => setModalVisible3(true)}
                    />
                  </View>
                </Item>
              ) : null}
              <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
              >
                <PickerDepartamento
                onChange={handleChangeDepartment}
                options={departments?.departamentos}
                enabled={existDeparment}
              />
              </View>
              
              <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
              >
              <PickerCiudad
                onChange={handleChangecity}
                value={cityId}
                options={cities?.ciudades}
                enabled={existCity && Boolean(departId)}
              />
              </View>
              <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
              >
              <PickerSitio
                onChange={(value) => onChangeText("site_id", value)}
                value={form.site_id}
                options={sities?.sitios}
                enabled={existSitie && Boolean(departId)}
              />
              </View>
              <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
              >
              <TextInput
                  onChangeText={(text) => onChangeText("direccion", text)}
                  value={form.direccion}
                  placeholder="Cra 68 a # 56 - 28 Sur"
              />
              </View>
              <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
              >
              <TextInput
                   keyboardType="phone-pad"
                   placeholder="12312525"
                   value={form.cedula}
                   onChangeText={(text) => onChangeText("cedula", text)}
              />
              </View>
              <View
              style={{
                width: responsiveScreenWidth(90),
                borderRadius: 20,
                marginBottom: 15
              }}
              >
                <TextArea
                onChangeText={(text) => onChangeText("adicional", text)}
                value={form.adicional}
                style={{ padding: 10 }}
                rowSpan={5}
                bordered
                placeholder="Informacion adicional"
              />
              </View>
              <View style={[styles.box2]}>
              <View style={{ marginHorizontal: 25 }}>
                <Button
                  onPress={onPressCheckDisponibility}
                  block
                  style={{
                    width: 250,
                    marginVertical: 20,
                    alignSelf: "center",
                    backgroundColor: "#64C2C8",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: "bold",
                      color: "#ffffff",
                    }}
                  >
                    Confirmar
                  </Text>
                </Button>
              </View>
            </View>
          </View>
          <ModalComponent onClose={setDialog} {...dialog} />
        </ScrollView>
      )}


      <View style={styles.centeredView}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible3}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible3);
          }}
        >
          <View style={styles.centeredView}>
            <View
              style={{
                width: responsiveScreenWidth(90),
                height: responsiveScreenHeight(28),
                borderRadius: 20,
                backgroundColor: "white",
                justifyContent: "flex-end",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  width: responsiveScreenWidth(70),
                  alignItems: "center",
                  marginTop: 20,
                }}
              >
                <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-warning' : 'md-warning'}
                                            size="20"
                                            color="black"
                                            />
              </View>
              <View
                style={{ width: responsiveScreenWidth(70), marginVertical: 20 }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: responsiveScreenFontSize(2.5),
                    marginBottom: 20,
                  }}
                >
                  ¿No encuentras tu ciudad? Cambia tu premio por 1150 puntos.
                </Text>
              </View>
              <View
                style={{
                  width: responsiveScreenWidth(90),
                  flexDirection: "row",
                }}
              >
                <TouchableOpacity
                  disabled={isLoadingChange}
                  style={{
                    width: responsiveScreenWidth(45),
                    backgroundColor: "#E7344C",
                    borderBottomLeftRadius: 20,
                    alignItems: "center",
                    padding: 15,
                  }}
                  onPress={changeAwardByPoints}
                >
                  <Text
                    style={{
                      fontSize: responsiveScreenFontSize(2),
                      color: "white",
                    }}
                  >
                    Cambiar por puntos
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    width: responsiveScreenWidth(45),
                    backgroundColor: "#64C2C8",
                    borderBottomRightRadius: 20,
                    alignItems: "center",
                    padding: 15,
                  }}
                  onPress={() => setModalVisible3(!modalVisible3)}
                >
                  <Text
                    style={{
                      fontSize: responsiveScreenFontSize(2),
                      color: "white",
                    }}
                  >
                    Continuar
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible2}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible2(!modalVisible2);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.box_content}>
            <View style={styles.box_icon}>
            <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-gift' : 'md-gift'}
                                            size="20"
                                            color="black"
                                            />
            </View>
            <View style={styles.box_text}>
              <Text style={styles.text}>
                ¡Estamos revisando la disponibilidad de tu premio!
              </Text>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(2),
                  marginHorizontal: 30,
                  textAlign: "center",
                  marginTop: 10,
                }}
              >
                <When condition={!isLoadingVerify}>{messageDisponibility}</When>
                <Unless condition={!isLoadingVerify}>
                  <Spinner color="red" style={styles.spinner} />
                </Unless>
              </Text>
            </View>
            <View
              style={[
                styles.box_buttom,
                {
                  display: "flex",
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "space-between",
                },
              ]}
            >
              <TouchableOpacity
                style={{
                  width: "50%",
                  paddingVertical: 15,
                  alignSelf: "flex-end",
                  backgroundColor: "#E7344C",
                  borderBottomLeftRadius: 10,
                }}
                onPress={() => setModalVisible2(!modalVisible2)}
              >
                <Text style={styles.text_buttom}>CERRAR</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: "50%",
                  paddingVertical: 15,
                  alignSelf: "flex-end",
                  backgroundColor: "#E7344C",
                  borderBottomRightRadius: 10,
                  backgroundColor: enabledButtonRendention
                    ? "#e1e1e1"
                    : "#64C2C8",
                }}
                onPress={onPressCanjear}
                disabled={enabledButtonRendention}
              >
                <Text style={styles.text_buttom}>REDIMIR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.box_content}>
            <View style={styles.box_icon}>
            <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-gift' : 'md-gift'}
                                            size="20"
                                            color="black"
                                            />
            </View>
            <View style={styles.box_text}>
              <Text style={styles.text}>¡Tu premio ha sido canjeado!</Text>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(2),
                  marginHorizontal: 30,
                  textAlign: "center",
                  marginTop: 10,
                }}
              >
                Te contactaremos en las próximas 24h para realizar la entrega
              </Text>
            </View>
            <View style={styles.box_buttom}>
              <TouchableOpacity
                style={styles.buttom}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.text_buttom}>CERRAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  container2: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginTop: 20,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    marginTop: 30,
  },
  box2: {
    width: "100%",
    height: 100,
    marginBottom: 10,
    marginTop: 10,
  },
  box3: {
    width: "85%",
    height: 100,
    marginTop: 10,
  },
  box4: {
    width: "85%",
    height: 100,
  },

  box5: {
    width: "100%",
    height: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  box_content: {
    backgroundColor: "white",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(35),
    borderRadius: 10,
    alignItems: "center",
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(12),
  },
  icon: {
    color: "#E7344C",
    fontSize: responsiveScreenFontSize(8),
  },
  box_text: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(15),
  },
  text: {
    fontSize: responsiveScreenFontSize(3),
    marginHorizontal: responsiveScreenWidth(5),
    textAlign: "center",
  },
  box_buttom: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "flex-end",
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttom: {
    backgroundColor: "#64C2C8",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(6),
    justifyContent: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: "center",
    color: "white",
  },
});
