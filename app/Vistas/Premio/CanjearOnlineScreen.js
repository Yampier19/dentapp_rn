import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Modal,
  TouchableOpacity,
} from "react-native";
import { Button, Card, Item, Label, Input, Spinner, Icon } from "native-base";
import ModalComponent from "../../Components/Modal";
import s from "@Public/Css/style";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { When } from "react-if";
import { Unless } from "react-if";
import { useAwardExchange } from "../../services/dentapp/awards/useAwards";
import { useExchangeAwardProduct } from "../../services/dentapp/navident/useNaviDent";

export default function CanjearOnlineScreen({ route, navigation }) {
  const { premio, profile, campaign, id_navidad } = route.params || {};
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const [form, setForm] = useState({
    receptor: profile?.correo || "",
    telefono: profile?.telefono || "",
    cedula: "",
    cantidad: 1,
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [errorEmptyForm, setErrorEmptyForm] = useState(false);
  const { mutateAsync: awardExchange, isLoading, data } = useAwardExchange();
  const {
    mutateAsync: awardExcahangeProduct,
    isLoading: loadingNavident,
    data: responseNavident,
  } = useExchangeAwardProduct();

  useEffect(() => {
    validField();
  }, [form]);

  const onPressCanjear = async () => {
    if (!errorEmptyForm) {
      if (form.cedula.length > 4 && form.cedula.length < 20) {
        if (form.receptor && form.telefono) {
          let response = null;
          if (campaign === "Navident") {
            response = await awardExcahangeProduct({
              id: id_navidad,
              data: form,
            });
          } else {
            response = await awardExchange({
              award: form,
              awardId: premio.id_premio,
            });
          }
          if (response !== null) {
            if (response?.status === "success") {
              setModalVisible(true);
            } else {
              setDialog({
                visible: true,
                message:
                  response?.message ||
                  "Ocurrió un error al momento de canjear el premio, vuelva a intentarlo",
                type: "error",
              });
            }
          }
        } else {
          setDialog({
            visible: true,
            message: "Tiene campos vacios, por favor verifique.",
            type: "warning",
          });
        }
      } else {
        setDialog({
          visible: true,
          message:
            "La cedula debe tener mas de 4 caracteres y menos a 20 caracteres",
          type: "warning",
        });
      }
    } else {
      setDialog({
        visible: true,
        message: "Tiene campos vacios, por favor verifique.",
        type: "warning",
      });
    }
  };

  const onChangeText = (name, value) => {
    setForm({
      ...form,
      [name]: value,
    });
  };

  const validField = () => {
    Object.values(form).forEach((value) => {
      if (value === "" || value === null || value === undefined) {
        setErrorEmptyForm(true);
      } else {
        setErrorEmptyForm(false);
      }
    });
  };

  const onPressClose = () => {
    setModalVisible(false);
    navigation.navigate("DetallesOnline", {
      response: data || responseNavident,
      premio,
    });
  };

  return (
    <View style={s.container}>
      <When condition={!isLoading || !loadingNavident}>
        <ScrollView>
          <View style={[styles.content]}>
            <View style={[styles.box1]}>
              <Text
                style={{
                  marginHorizontal: 20,
                  fontSize: 20,
                  fontWeight: "bold",
                }}
              >
                Por favor registre y confirme los datos para poder canjear tu
                premio
              </Text>
              <Text
                style={{
                  alignSelf: "flex-end",
                  marginTop: 30,
                  marginHorizontal: 20,
                  fontSize: 15,
                  fontWeight: "bold",
                  color: "gray",
                }}
              >
                Datos de domicilio
              </Text>
              <Card
                style={{
                  width: "90%",
                  height: 80,
                  justifyContent: "flex-end",
                  alignSelf: "center",
                  marginTop: 20,
                  padding: 10,
                }}
              >
                <Item stackedLabel>
                  <Label>Correo electronico</Label>
                  <Input
                    keyboardType="email-address"
                    autoCapitalize="none"
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    placeholder="mail@mail.com"
                    autoCorrect={false}
                    value={form.receptor}
                    onChangeText={(text) => onChangeText("receptor", text)}
                  />
                </Item>
              </Card>
              <Card
                style={{
                  width: "90%",
                  height: 80,
                  justifyContent: "flex-end",
                  alignSelf: "center",
                  marginTop: 10,
                  padding: 10,
                }}
              >
                <Item stackedLabel>
                  <Label>Teléfono</Label>
                  <Input
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    keyboardType="phone-pad"
                    placeholder="777-77-77"
                    value={form.telefono}
                    onChangeText={(text) => onChangeText("telefono", text)}
                  />
                </Item>
              </Card>
              <Card
                style={{
                  width: "90%",
                  height: 80,
                  justifyContent: "flex-end",
                  alignSelf: "center",
                  marginTop: 10,
                  padding: 10,
                }}
              >
                <Item stackedLabel>
                  <Label>Número de identificación</Label>
                  <Input
                    style={{ borderBottomWidth: 1, borderBottomColor: "red" }}
                    keyboardType="phone-pad"
                    placeholder="12312525"
                    value={form.cedula}
                    onChangeText={(text) => onChangeText("cedula", text)}
                  />
                </Item>
              </Card>
            </View>
            <View style={[styles.box2]}>
              <View style={{ marginHorizontal: 25 }}>
                <Button
                  onPress={onPressCanjear}
                  block
                  style={{
                    width: 250,
                    marginVertical: 30,
                    alignSelf: "center",
                    backgroundColor: "#64C2C8",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: "bold",
                      color: "#ffffff",
                    }}
                  >
                    Confirmar
                  </Text>
                </Button>
              </View>
            </View>
          </View>
          <ModalComponent onClose={setDialog} {...dialog} />
        </ScrollView>
      </When>
      <Unless condition={!isLoading || !loadingNavident}>
        <Spinner color="red" style={styles.spinner} />
      </Unless>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.box_content}>
            <View style={styles.box_icon}>
              <Icon name="gift" type="FontAwesome5" style={styles.icon} />
            </View>
            <View style={styles.box_text}>
              <Text style={styles.text}>¡Tu premio ha sido canjeado!</Text>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(2),
                  marginHorizontal: 30,
                  textAlign: "center",
                  marginTop: 10,
                }}
              >
                Te contactaremos en las próximas 24h para realizar la entrega
              </Text>
            </View>
            <View style={styles.box_buttom}>
              <TouchableOpacity style={styles.buttom} onPress={onPressClose}>
                <Text style={styles.text_buttom}>CERRAR</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  container2: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 400,
    marginTop: 30,
  },
  box2: {
    width: "100%",
    height: 150,
    marginBottom: 10,
    marginTop: 10,
  },
  box3: {
    width: "85%",
    height: 200,
    marginTop: 10,
  },
  box4: {
    width: "85%",
    height: 200,
  },

  box5: {
    width: "100%",
    height: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  box_content: {
    backgroundColor: "white",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(35),
    borderRadius: 10,
    alignItems: "center",
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(12),
  },
  icon: {
    color: "#E7344C",
    fontSize: responsiveScreenFontSize(8),
  },
  box_text: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(15),
  },
  text: {
    fontSize: responsiveScreenFontSize(3),
    marginHorizontal: responsiveScreenWidth(5),
    textAlign: "center",
  },
  box_buttom: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "flex-end",
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttom: {
    backgroundColor: "#64C2C8",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(6),
    justifyContent: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: "center",
    color: "white",
  },
});
