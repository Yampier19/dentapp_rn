import React, { useEffect, useState, useCallback } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  RefreshControl,
} from "react-native";
import { Spinner } from "native-base";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { When } from "react-if";
import { Unless } from "react-if";

import { Request } from "../../../api/api";
import { resolveImage } from "../../../utils/utils";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";
import BannerHeader from "./BannerHeader";
import { useProfile } from "../../../services/dentapp/user/useUser";
import { useListAllAwardCategories } from "../../../services/dentapp/awards/useAwards";

const ItemPremio = ({ premio, navigation, profile }) => (
  <TouchableOpacity
    key={premio?.nombre}
    onPress={() => navigation.navigate("PremioSelect", { premio, profile })}
    style={{ width: wp("43%"), height: hp("30%"), marginBottom: 10 }}
  >
    <ImageBackground
      source={{ uri: premio?.file }}
      style={{
        flex: 1,
        resizeMode: "cover",
      }}
    >
      <View
        style={{
          width: wp("43%"),
          height: hp("15%"),
          padding: 10,
          backgroundColor: "rgba(52, 52, 52, 0.5)",
          position: "absolute",
          left: 0,
          right: 0,
          bottom: 0,
        }}
      >
        <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>
          {premio?.numero_estrellas
            .toString()
            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}{" "}
          Puntos
        </Text>
        <Text style={{ color: "white", fontSize: 16, marginVertical: 10 }}>
          {premio?.nombre}
        </Text>
      </View>
    </ImageBackground>
  </TouchableOpacity>
);

export default function PremioOfertaScreen({ props, route, navigation }) {
  const [premios, setPremios] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loadingCategories, setLoadingCategories] = useState(false);
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(null);
  const [loadingPagination, setLoadingPagination] = useState(false);
  const [profile, setProfile] = useState(null);
  const { data: Profile } = useProfile();
  const { data: Categories, isLoading: isLoadingCategories } =
    useListAllAwardCategories();

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    getCategories();
    getPremios();
    setPage(1);
    setPremios([]);
  }, []);

  useEffect(() => {
    getCategories();
    setPremios([]);
    getProfile();
  }, [isLoadingCategories, refreshing]);

  useEffect(() => {
    setLoading(true);
    getPremios();
    setPage(1);
    setPremios([]);
  }, [category, isLoadingCategories, refreshing]);

  useEffect(() => {
    if (page <= totalPage) {
      setLoadingPagination(true);
      getPremios();
    }
  }, [page]);

  const getProfile = async () => {
    try {
      const data = Profile;
      if (data?.status === "success" && data?.datos) {
        const currentData = await resolveImage(
          [data?.datos],
          "cliente/avatar",
          "foto",
          "uploads/",
          true,
          "POST"
        );
        setProfile({
          ...currentData?.[0],
          password: data?.password,
          message: data?.message,
        });
      }
    } catch (error) {}
  };

  const getPremios = async () => {
    try {
      const response = await Request(
        "GET",
        `premio/categoria/${category}/6?page=${page}`
      );
      const data = await response.json();
      if (data?.status === "success" && data?.premios) {
        const filtered = data?.premios.filter((premio) => premio.stock > 0);
        if (filtered.length > 0) {
          const currentData = await resolveImage(
            filtered,
            "premio/getImage",
            "foto",
            "premios/",
            true
          );
          let categoryPremios = [...premios, ...currentData];
          let dataLocal = [];
          categoryPremios.forEach((d) => {
            if (d.tag === category) {
              dataLocal.push(d);
            }
          });
          setPremios(dataLocal);
          setTotalPage(data?.paginacion.total_pages);
          setLoading(false);
          setLoadingPagination(false);
          setRefreshing(false);
        } else {
          setPremios([]);
          setLoading(false);
          setLoadingPagination(false);
          setRefreshing(false);
        }
      }
    } catch (error) {
      setLoading(false);
      setRefreshing(false);
    }
  };

  const getCategories = async () => {
    try {
      setLoadingCategories(true);
      const data = Categories;
      if (data?.status === "success" && data?.categorias.length > 0) {
        const filtered = data?.categorias.filter((cat) => cat.premios > 0);
        if (filtered.length > 0) {
          setCategories(data.categorias);
          setCategory(data.categorias?.[0]?.nombres);
          setLoadingCategories(false);
        } else {
          setPremios(null);
        }
      } else {
        setCategories(null);
        setLoadingCategories(false);
      }
    } catch (error) {
      setLoadingCategories(false);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        onScroll={(e) => {
          if (
            e.nativeEvent.contentOffset.y > 0 &&
            loadingPagination === false &&
            page <= totalPage
          ) {
            let localPage = page;
            setPage((localPage += 1));
          }
        }}
      >
        <BannerHeader refresh={!refreshing} navigation={navigation} />
        <View
          style={{
            height: hp("5%"),
            width: wp("90%"),
            alignSelf: "center",
            marginTop: 10,
          }}
        >
          <When condition={!loadingCategories && categories}>
            <ScrollView horizontal={true} style={{ borderBottomWidth: 1 }}>
              <View style={{ flexDirection: "row" }}>
                {categories &&
                  categories.map((cat, index) => (
                    <View
                      key={`${index}-${cat?.nombres}`}
                      style={{ paddingVertical: 5, paddingHorizontal: 12 }}
                    >
                      <TouchableOpacity
                        onPress={() => setCategory(cat?.nombres)}
                      >
                        <Text
                          style={{
                            fontWeight: "bold",
                            color:
                              category === cat?.nombres ? "#E7344C" : "#000",
                          }}
                        >
                          {cat?.nombres}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ))}
              </View>
            </ScrollView>
          </When>
          <Unless condition={!loadingCategories && categories}>
            <Spinner
              color="red"
              style={{
                alignContent: "center",
                flexDirection: "row",
                alignItems: "center",
              }}
            />
          </Unless>
        </View>
        {loading ? (
          <Spinner color="red" style={styles.spinner} />
        ) : (
          <ScrollView style={{ width: wp("90%"), alignSelf: "center" }}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 20,
                marginTop: 20,
                flexWrap: "wrap",
              }}
            >
              {premios?.length > 0 &&
                premios.map((premio, index) => (
                  <ItemPremio
                    premio={premio}
                    navigation={navigation}
                    profile={profile}
                    key={index}
                  />
                ))}
              {premios.length === 0 && (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100%",
                    marginVertical: 40,
                  }}
                >
                  <Text>
                    La categoría seleccionada no tiene premios disponibles
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        )}
        {loadingPagination && !loading && (
          <Spinner color="red" style={styles.spinner} />
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 50,
    marginBottom: 10,
  },
  box2: {
    width: "100%",
    height: 50,
  },
  box3: {
    width: "100%",
    height: 1000,
  },

  box7: {
    width: "100%",
    height: 200,
    top: "5%",
  },
});
