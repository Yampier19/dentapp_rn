import React, { useEffect, useState } from 'react'
import { View, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import { Spinner } from 'native-base';
import Swiper from 'react-native-web-swiper';

import { resolveImage, getToken } from '../../../utils/utils';
import { useAnnouncementList } from "../../../services/dentapp/announcement/useAnnouncement";
import { useBannersList } from "../../../services/dentapp/banners/useBanners";


export default function HomeScreen(props) {
    const {reload } = props?.route?.params || {}
    const {refresh} = props;
    const [banners, setBanners] = useState([]);
    const [loading, setLoading] = useState(false);
    const [ token, setToken ] = useState(null);
    const { width } = Dimensions.get('window');
    const { data: Announcement, isLoading } = useAnnouncementList(10);
    const { data: Banners, isLoading: isLoadingBanners} = useBannersList();
    const [state, setState] = React.useState({ open: false });

    const onStateChange = ({ open }) => setState({ open });

    const { open } = state;

    useEffect(() => {
        props.navigation.addListener('beforeRemove', (e) => {
            // Prevent default behavior of leaving the screen
            e.preventDefault();
        })
    }, [])

    const getBanners = async () => {
        try {
            setLoading(true);
            const data = Banners;
            if (data?.status === 'success' && data?.banners.length > 0) {
                const images = await resolveImage(data.banners, 'banners/getImage', 'imagen', 'banner/', false);
                if (images.length > 0) {
                    let newImg = []
                    images.forEach((img, index) => {
                        newImg.push({
                            img: img,
                            data: data.banners[index]
                        })
                    })
                    setBanners(newImg)
                }
                setLoading(false);
            }
        } catch (error) {
            setLoading(false);
        }
    };

    const goToVideo = (video) => {
        if(video.archivo){
            props.navigation.navigate('DetallesBanner', { video })
        }
    }

    useEffect(() => {
        main();
    }, [reload, isLoading, refresh, isLoadingBanners])

    const main = async () => {
        const localToken = await getToken();
        if(localToken){
            setToken(localToken);
            getBanners();
        } else {
            props.navigation.navigate('Login')
        }
    }

    if(!token){
        return <></>
    }

    return (
        <View style={{ width: width * 1.0, height: width * 0.5 * 0.9, backgroundColor: '#E7344C'}}>
        {loading ? (
            <Spinner color="white" style={{ flex: 1, justifyContent: "center", alignItems: "center" }} />
        ) : (
           
            <Swiper
            from={1}
            controlsProps={{
                dotsTouchable: true, 
                dotsPos: 'bottom',
                prevPos: false,
                nextPos: false,
              }}
            minDistanceForAction={0.1}
            accessible={true}
            accessibilityLabel="Imagenes promocionales"
            accessibilityHint="Carousel principal" loop={true} autoplay={true} autoplayTimeout={5.0}>
            {
                banners?.map((image, index) => (
                   <TouchableOpacity key={`banners_${index}`} onPress={() => goToVideo(image.data)}>
                        <Image key={`banner-${index}`} style={{ width: width * 1.0, height: width * 0.5 * 0.9 }}
                        resizeMode = 'cover' source={{ uri: image.img }} />
                   </TouchableOpacity>
                ))
            }
        </Swiper>
        )}
    </View>
    )
}


const styles = StyleSheet.create({
    video: {
        flex: 1,
        width: '100%',
        height: 120,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: "center",
    },
    playButton: {
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        top: '30%',
        left: '40%',
        width: 40,
        height: 40,
        borderRadius:20,
        backgroundColor: 'rgba(255,255,255,.8)'
    },
    textPlay: {
        color: 'black'
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    wrapper: {

    },

    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },


    box2: {
        width: '95%',
        height: 370,
    },
    box3: {
        width: '100%',
        height: 500,
        marginTop: 20,
        
    },
    box4: {
        width: '100%',
        height: 350
    },
    box5: {
        width: '100%',
        height: 100
    },

    centeredView: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "flex-end",
      },
      modalView: {
        backgroundColor: "#E7344C",
        borderRadius: 160,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        left: 50, top: 20,
        width: '80%',height: 320,
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        justifyContent: 'center'
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center"
      }
});
