import React, { useState } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Dimensions,
  Pressable,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback
} from "react-native";
import { Icon } from "native-base";
import Swiper from "react-native-swiper";
import { Dialog, List } from "react-native-paper";

export default function ItemScreen({ route, navigation }) {
  const { width, height } = Dimensions.get("window");
  const [visible, setVisible] = React.useState(true);
  const hideDialog = () => {
    navigation.navigate('Help')
    setVisible(false);
  }
    return (
     <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
       <Text style={{fontSize: 20}}>Cargando...</Text>
              <Dialog visible={visible} onDismiss={hideDialog}
              >
                <Dialog.ScrollArea>
                                    <ScrollView contentContainerStyle={{ paddingVertical: 20 }}>
                    <Text style={{ fontSize: 16, marginBottom: 20 }}>
                      Familias participantes, da clic para ver el detalle de los
                      productos;
                    </Text>

                    <List.Accordion
                      title="Adhesivos"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Adper™ Single Bond 2 Frasco por 3gr{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Adper™ Single Bond 2 Frasco por 6gr{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Adper™ Single Bond Universal Frasco por 5ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Scotchbond™ Etchant Frasco12ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Scotchbond™ Etchant Jeringa 5ml{" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Cementos"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ Cem Easymix Kit de Introducción Polvo 30 gr,
                        Liquido 12 ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ Cem Easymix Mini Kit Polvo15gr, Liquido de 6ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Ceramic Primer Frasco por 5ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Temp NE1 pasta base 30gr - 1 catalizador 13gr{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ U200 Color A2 en presentación Clicker 11g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ U200 Color Translucido (TR) en presentación
                        Clicker 11g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Ultimate Clicker Color A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Ultimate Clicker Color B0.5{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Ultimate Clicker Color Translucido{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Relyx™ Veneer Color B0.5 en presentación jeringa 3gr{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Relyx™ Veneer Color TR en presentación jeringa 3gr{" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Ionómeros"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ Molar Easymix A.R.TKit Polvo 12.5gr + Liquido
                        8.5ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ Molar Repuesto Líquido 8.5 ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ Molar Repuesto Polvo 12.5 gr{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ N100 Color A2 en Clicker 12g + Primer 6ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Ketac™ N100 Color A3 en Clicker 12g + Primer 6ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Vitrebond™ Kit Introductorio Polvo 9gr, Liquido 5.5ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Vitrebond™ Mini Kit Polvo 4.5gr, Liquido 2.75ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Vitrebond™ Plus en sistema Clicker 10g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Vitremer™ Kit Color A3, (Polvo 5gr, Liquido 2.5ml,
                        Primer 2ml + Abrillantador 2ml){" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Vitremer™ Repuesto Líquido 8 ml{" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Lámpara"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="newspaper-variant-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          marginBottom: 10,
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Elipar™ DeepCure-L Lámpara de Fotocurado LED
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Materiales de Impresión"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Express™ Adhesivo para cubeta de Polivinilsiloxano
                        (VPS), frasco por 17ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Express™ Cuerpo liviano, polimerización regular (verde)
                        - 2 Cartuchos de 50ml C/U{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Express™ Kit Introductorio - Masilla (Material Pesado) 2
                        unidades de 305ml C/U + 2 cucharas dispensadoras +
                        Cuerpo liviano, 2 Cartuchos de 50ml C/U
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Express™ Masilla Material Pesado 305 ml x 2 unidades,
                        mas 2 cucharas dispensadoras.{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Express™ XT Kit Introductorio - Masilla (Material
                        Pesado) 2 unidades de 250ml C/U + 2 cucharas
                        dispensadoras + Cuerpo liviano, 2 Cartuchos de 50ml C/U
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Postes"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Fiber Post 10 Postes en Fibra de Vidrio Tamaño
                        1.1mm (blanco){" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Fiber Post 10 Postes en Fibra de Vidrio Tamaño
                        1.3 mm (amarillo){" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Fiber Post 10 Postes en Fibra de Vidrio Tamaño
                        1.6 mm(Rojo){" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Prevención"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Clinpro™ Jeringa de 1.2 ml con 10 puntas de aplicación{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Clinpro™ White Varnish Caja Por 100 monodosis + 100
                        Aplicadores Microtip{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Clinpro™ White Varnish Caja Por 100 monodosis + 100
                        Aplicadores Microtip CEREZA{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Clinpro™ White Varnish Caja Por 100 monodosis + 100
                        Aplicadores Microtip MELON{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Clinpro™ XT en presentación Clicker por 10g{" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Postes"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Fiber Post 10 Postes en Fibra de Vidrio Tamaño
                        1.1mm (blanco){" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Fiber Post 10 Postes en Fibra de Vidrio Tamaño
                        1.3 mm (amarillo){" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        RelyX™ Fiber Post 10 Postes en Fibra de Vidrio Tamaño
                        1.6 mm(Rojo){" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Pulido"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        3M ESPE™ Mandril para discos Sof-Lex™ (1 Unidad){" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Kit Introductorio de 120 Discos Extradelgados
                        1/2" (30 Discos Súper Finos, 30 Finos, 30 Medios, 30
                        Grueso) + Mandril{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Mini Kit de 40 Discos Extradelgados 1/2" (10
                        Discos Súper Finos, 10 Finos, 10 Medios, 10 Grueso) +
                        Mandril{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Repuesto Grano Fino 30 Unidades{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Repuesto Grano Medio 30 Unidades{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Repuesto Grano Grueso 30 Unidades{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Repuesto Grano Súper Fino 30 Unidades{" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Resinass"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        FILTEK ONE Essential Kit{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        FILTEK ONE PEDIATRIC KIT{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ 250XT Institutional Kit{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Bulk Fill Flowable 2 Jeringas de 2g C/U Color A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Bulk Fill Flowable 2 Jeringas de 2g C/U Color A2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Bulk Fill Flowable 2 Jeringas de 2g C/U Color A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ One Bulk Posterior Fill Jeringa individual de 4
                        gr Color A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ One Bulk Posterior Fill Jeringa individual de 4
                        gr Color A2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ One Bulk Posterior Fill Jeringa individual de 4
                        gr Color A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ One Bulk Posterior Fill Jeringa individual de 4
                        gr Color B1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ P60 Essential Kit{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ P60 Jeringa Individual de 4g Color A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ P60 Jeringa Individual de 4g Color B2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ P60 Jeringa Individual de 4g Color C2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Capsulas Tono A1 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Capsulas Tono A2 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Capsulas Tono A3 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Capsulas Tono PO 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Capsulas Tono XW 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono A2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono A3.5{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono A4{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono B1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono B2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono D3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono PO{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ UNIVERSAL Jeringa de 4g Tono XW{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono A2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono A3.5{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono B1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono B2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 Jeringa Individual de 4g Tono C2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Essential Kit{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color A2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color A3.5{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color A4{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color B1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color B2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color B3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color C2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Jeringa Individual de 3g Color OA2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250 XT Kit Introductorio 5 Jeringas
                        (A2+A3+B2+B1+A1) 3gr c/u + Adper™ Single Bond 2 de 3gr +
                        Scotchbond Etchant Jeringa 5ml
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z250XT MINI KIT{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Capuslas Color A1B 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Capuslas Color A2B 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Capuslas Color A3B 4g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Essential Kit{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Flow 2 Jeringas de 2g C/U Color A1{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Flow 2 Jeringas de 2g C/U Color A2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Flow 2 Jeringas de 2g C/U Color A3{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Flow 2 Jeringas de 2g C/U Color B2{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A1B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A1D{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A1E{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A2D{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A2E{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A3.5B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A3B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A3D{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color A3E{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color B1B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color B1E{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color B2B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color B2E{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color B3B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color C2B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color CT{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color WB{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color WD{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color WE{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color XWB{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Jeringa Individual de 4g Color XWE{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Kit de Cuerpos 5 Jeringas 4g C/U (A1B,
                        A2B, A3B, A3.5B, B2B) Adhesivo Adper™ Single Bond 2 De
                        3g + Scotchbond Etchant Jeringa 5ml{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XT Kit por 8 Jeringas A1B A2B A3.5B A3B B1B
                        B2B A1E A2D + 1 Pad + 1 Single Bond Universal + 1 Rueda
                        de color + 1 Porta pinceles +60 Pinceles + 1 dispensador{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350 XTJeringa Individual de 4g Color A2B{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350XT BLEACHING KIT{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Filtek™ Z350XT KIT ESTETICA AVANZADA{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Nuevo* Sof-Lex™ Mini Kit de 40 Discos Extradelgados 1/2"
                        (10 Discos Súper Finos, 10 Finos, 10 Medios, 10 Grueso)
                        + Mandril
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Sof-Lex™ Essential Kit de 120 Discos Extradelgados 1/2"
                        (30 Discos Súper Finos, 30 Finos, 30 Medios, 30 Grueso)
                        (Sistema de Diamante para pulido: 3 Discos pre-pulido y
                        3 discos pulido final + Mandril{" "}
                      </Text>
                    </List.Accordion>
                    <List.Accordion
                      title="Temporales"
                      left={(props) => (
                        <List.Icon
                          {...props}
                          icon="file-document-outline"
                          style={{ padding: 0, margin: 0, border: 0 }}
                        />
                      )}
                    >
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        3M ESPE™ Mandril para discos Sof-Lex™ (1 Unidad)
                        Protemp™ 4 Cartucho Refill Color A1 67g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Protemp™ 4 Cartucho Refill Color A2 67g{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Protemp™ 4 Puntas de automezcla para Protemp™ color azul
                        50 unidades{" "}
                      </Text>
                      <Text
                        style={{
                          backgroundColor: "#EEEEEE",
                          paddingVertical: 10,
                          paddingHorizontal: 30,
                        }}
                      >
                        Protemp™ 4Cartucho Refill Color A3 67g{" "}
                      </Text>
                    </List.Accordion>
                   
                  </ScrollView>
                </Dialog.ScrollArea>
              </Dialog>
              </View>
    );
  

  

}
const styles = StyleSheet.create({
  wrapper: {},
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB",
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5",
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9",
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
