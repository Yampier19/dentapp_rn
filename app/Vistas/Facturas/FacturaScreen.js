import React, { useState } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Platform,
  FlatList,
} from "react-native";
import { Icon } from "native-base";
import * as ImagePicker from "expo-image-picker";
import * as DocumentPicker from "expo-document-picker";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";
import { useProfile } from "../../services/dentapp/user/useUser";
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { useResponsiveScreenFontSize } from "react-native-responsive-dimensions";
import ModalComponent from "../../Components/Modal";

export default function FacturaScreen(props) {
  const [image, setImage] = useState(null);
  const { data: Profile } = useProfile();

  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });

  const showImagePicker = async () => {
    const permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("You've refused to allow this appp to access your photos!");
      return;
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: Platform.OS !== "ios" ? true : false,
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const showDocumentPicker = async () => {
    let result = await DocumentPicker.getDocumentAsync({});
    if (result.type !== "cancel") {
      let extension = result.name.slice(
        ((result.name.lastIndexOf(".") - 1) >>> 0) + 2
      );
      if (extension === "pdf") {
        props.navigation.navigate("PreviewFactura", {
          typeImage: "application/pdf",
          image: result.uri,
          ...props.route.params,
        });
      } else if (extension === "jpeg" || extension === "jpg") {
        props.navigation.navigate("PreviewFactura", {
          typeImage: "image/jpeg",
          image: result.uri,
          ...props.route.params,
        });
      }else{
        setDialog({
          visible: true,
          message: "La extensión de la imagen debe ser JPEG o JPG!",
          type: "error",
         
        });
      }
    }
  };

  const openCamera = async () => {
    props.navigation.navigate("CameraFactura", { ...props.route.params });
  };

  return (
    <View style={{ flex: 1 }}>

        <View
          style={{
            width: "90%",
            height: 80,
            alignSelf: "center",
            marginVertical: 10,
          }}
        >
          <Text style={{ fontSize: 18, marginBottom: 10 }}>
            Recuerda que solo ganarás puntos por registrar facturas de:
          </Text>
          <Text style={{ fontSize: 18, color: "#64C2C8", fontWeight: "bold" }}>
            {Profile?.datos.distribuidor}
          </Text>
        </View>
        <View
          style={{
            width: "90%",
            height: 250,
            alignSelf: "center",
            marginVertical: 20,
          }}
        >
          <View
            style={{
              marginBottom: 10,
              width: 120,
              height: 120,
              borderRadius: 80,
              backgroundColor: "#64C2C8",
              alignSelf: "center",
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
             <Icon
        as={Ionicons}
        name={Platform.OS ? 'ios-receipt' : 'md-receipt'}
        size="16"
        color="white"
      />
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: "#64C2C8",
              padding: 10,
              marginVertical: 10,
              borderRadius: 10,
              width: 300,
              alignSelf: "center",
            }}
            onPress={showDocumentPicker}
          >
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: 'center' }}>
            <Icon
              as={Ionicons}
              name={Platform.OS ? 'ios-book' : 'md-book'}
              size="15"
              color="white"
            />
              <Text style={{ color: "white", marginLeft: 10 }}>
                CARGAR ARCHIVO
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: "#64C2C8",
              padding: 10,
              borderRadius: 10,
              width: 300,
              alignSelf: "center",
            }}
            onPress={openCamera}
          >
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: 'center' }}>
            <Icon
              as={Ionicons}
              name={Platform.OS ? 'ios-camera' : 'md-camera'}
              size="15"
              color="white"
            />
              <Text style={{ color: "white", marginLeft: 10 }}>
                TOMAR FOTO A LA FACTURA
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: "90%",
            height: 180,
            alignSelf: "center",
            marginBottom: 10,
          }}
        >
          <Text style={{ textAlign: "center", marginBottom: 10 }}>
            Recomendaciones
          </Text>
          <FlatList
            data={[
              { key: "Cargar un archivo o foto legible." },
              {
                key: "Las facturas no deben tener marcas, rayones y/o tachones en la factura.",
              },
              { key: "Subir facturas solo del distribuidor elegido." },
              { key: "Cargar una factura con fecha no mayor a 30 días" },
            ]}
            renderItem={({ item }) => (
              <View
                style={{
                  flexDirection: "row",
                  marginHorizontal: 20,
                  marginVertical: 5,
                }}
              >
                <Text style={{ color: "#F28D80" }}>■</Text>
                <Text style={{ marginHorizontal: 10, fontSize: 15 }}>
                  {item.key}
                </Text>
              </View>
            )}
          />
        </View>
        <View style={{ width: "90%", height: 120, alignSelf: "center" }}>
          <Text
            style={{ textAlign: "center", marginHorizontal: 20, marginTop: 10 }}
          >
            Los productos participantes los encuentras
          </Text>
          <Text
            style={{ color: "#64C2C8", textAlign: "center" }}
            onPress={() => props.navigation.navigate("Product")}
          >
            aquí
          </Text>
        </View>
        <ModalComponent onClose={setDialog} {...dialog} />        
    </View>
  );
}
