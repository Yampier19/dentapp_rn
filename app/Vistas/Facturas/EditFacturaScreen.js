import React from "react";
import { useState } from "react";
import { Dimensions } from "react-native";
import { TouchableOpacity } from "react-native";
import { ImageBackground } from "react-native";
import { ImageManipulator } from "expo-image-crop";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const { width, height } = Dimensions.get("window");

const EditFacturaScreen = ({ route, navigation }) => {
  const { typeImage, image, campaign } = route.params;
  const [visible, setVisible] = useState(false);
  const [img, setImg] = useState(image);

  const onToggleModal = () => {
    setVisible(!visible);
  };

  const previewFacture = () => {
    navigation.navigate("PreviewFactura", {
      typeImage: typeImage,
      image: img,
      campaign,
    });
  };

  return (
    <ImageBackground
      resizeMode="contain"
      style={{
        flex: 1,
        justifyContent: "center",
        padding: 0,
        alignItems: "center",
        height,
        width,
        backgroundColor: "black",
      }}
      source={{ uri: img }}
    >
      <TouchableOpacity
        onPress={() => setVisible(true)}
        style={{
          height: 40,
          width: 40,
          display: "flex",
          justifyContent: "center",
          alignSelf: "flex-end",
          alignItems: "center",
          position: "absolute",
          top: 10,
          left: 10,
          elevation: 10,
          borderRadius: 20,
          backgroundColor: "#64C1C6",
        }}
      >
        <MaterialCommunityIcons name="image-edit" size={30} color="white" />
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          height: 40,
          width: 40,
          display: "flex",
          justifyContent: "center",
          alignSelf: "flex-end",
          alignItems: "center",
          position: "absolute",
          top: 10,
          right: 10,
          elevation: 10,
          borderRadius: 20,
          backgroundColor: "#64C1C6",
        }}
        onPress={previewFacture}
      >
        <MaterialCommunityIcons name="check" size={30} color="white" />
      </TouchableOpacity>
      <ImageManipulator
        photo={{ uri: img }}
        isVisible={visible}
        onPictureChoosed={({ uri: uriM }) => setImg(uriM)}
        onToggleModal={onToggleModal}
      />
    </ImageBackground>
  );
};

export default EditFacturaScreen;
