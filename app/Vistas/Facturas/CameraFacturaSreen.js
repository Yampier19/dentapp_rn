import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import Camera from "../../Components/Camera";

const CameraFacturaSreen = ({ navigation, route }) => {
  const [image, setImage] = useState(null);
  const [reload, setReload] = useState(true);

  useEffect(() => {
    navigation.addListener("focus", () => {
      setReload(true);
    });
    navigation.addListener("blur", () => {
      setReload(false);
    });
    if (image) {
      navigation.navigate("EditCameraFactura", {
        typeImage: "image/jpeg",
        image: image,
        ...route.params,
      });
    }
  }, [image]);
  return (
    <React.Fragment>
      {reload && <Camera changeImage={setImage} />}
    </React.Fragment>
  );
};

export default CameraFacturaSreen;
