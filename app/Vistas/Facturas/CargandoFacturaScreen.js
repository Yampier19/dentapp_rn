import React, { useEffect } from "react";
import { View, StatusBar, Dimensions, Text } from "react-native";
import * as Animatable from "react-native-animatable";
import { imageBackgroundStyle } from "@Public/Css/General";

export default function LoadingScreen({ route, navigation }) {
  const { width, height } = Dimensions.get("window");
  const { factura, estado, puntos } = route.params || {};
  const goToScreen = () => {
    setTimeout(async () => {
      navigation.navigate("FacturaConfirmada", {
        factura: factura,
        estado: estado,
        puntos: puntos,
      });
    }, 2500);
  };

  useEffect(() => {
    goToScreen();
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#FAFAFA",
      }}
    >
      <Animatable.Image
        easing="ease-out"
        iterationCount="infinite"
        style={{
          width: width * 0.7,
          height: height * 0.4,
        }}
        source={require("@Public/Img/factura.gif")}
      />
      <Text>EXTRAYENDO INFORMACIÓN</Text>
    </View>
  );
}
