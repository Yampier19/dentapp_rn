import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Image,
  Modal,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Pressable,
  Dimensions,
  Linking,
  FlatList,
} from "react-native";
import { Item, Icon, Button, Spinner } from "native-base";
import { resolveImage, notificationAlert, getToken } from "../../utils/utils";
import s from "@Public/Css/style";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";
import { Request } from "../../api/api";
import { WebView } from "react-native-webview";
import * as Print from "expo-print";

export default function FacturaScreen({ route, navigation }) {
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [img, setImg] = useState(null);
  const [typeFile, setTypeFile] = useState(null);

  {
    /*  API WHATSAPP  */
  }
  const [whatsAppMessage, setWhatsAppMessage] = useState();
  const sendMsg = () => {
    let URL = Linking.openURL(
      "whatsapp://send?text=Buen día, necesito ayuda con Dentaap, mi nombre es ...&phone=+573125614712"
    );
    Linking.openURL(URL)
      .then((data) => {
        console.log("WhatsApp Opened");
      })
      .catch(() => {
        Alert.alert("Make sure Whatsapp installed on your device");
      });
  };

  const { factura } = route.params;

  const getImage = async (foto) => {
    setTypeFile(foto.foto.slice(((foto.foto.lastIndexOf(".") - 1) >>> 0) + 2));
    const res = await resolveImage(
      [foto],
      "factura/getArchive",
      "foto",
      "facturas/",
      false
    );
    if (res) {
      setImg(res[0]);
      setLoading(false);
    }
  };

  const getFactureData = async (id) => {
    setLoading(true);
    const res = await Request("GET", `factura/${id}`);
    const result = await res.json();
    if (result.status === "success") {
      getImage(result.factura);
    }
  };

  useEffect(() => {
    getFactureData(factura.id_factura);
  }, [factura]);

  const viewPDF = async () => {
    var fotoUri= factura?.foto;
    var newUri = fotoUri.slice(9)
    
    await Print.printAsync({
      uri: `https://dentapp.panelrp.com/facturas/getArchive/${newUri}`,
    });
  };

  const openImage = () => {
    if(typeFile==="pdf"){
      viewPDF();
    }else if(typeFile==="jpg" || typeFile==="jpeg"){
      setModalVisible(true);
    }
  };

  return (
    <View style={s.container}>
      <ScrollView>
        <View style={[styles.content]}>
          <View style={[styles.box1]}>
            <Text style={{ alignSelf: "flex-end", color: "gray" }}>
              {factura.fecha_factura}
            </Text>
          </View>
          <View style={[styles.box2]}>
            <View
              style={{
                width: 170,
                height: 170,
                borderRadius: 170,
                backgroundColor: "white",
                borderColor: "#F28D80",
                borderWidth: 2,
                alignItems: "center",
                alignSelf: "center",
              }}
            >
              <TouchableOpacity
                onPress={openImage}
                style={{
                  marginBottom: 20,
                  width: 150,
                  height: 150,
                  borderRadius: 80,
                  backgroundColor: "#F28D80",
                  marginTop: "5%",
                }}
              >
                <View
                  style={{
                    width: 150,
                    height: 150,
                    backgroundColor: "#000000",
                    opacity: 0.6,
                    borderRadius: 80,
                  }}
                >
                  <Icon
                    type="FontAwesome5"
                    name="eye"
                    style={{
                      color: "white",
                      fontSize: 50,
                      position: "absolute",
                      alignSelf: "center",
                      marginTop: "25%",
                    }}
                  />
                  <Text
                    style={{
                      color: "white",
                      fontSize: 20,
                      position: "absolute",
                      alignSelf: "center",
                      marginTop: "60%",
                    }}
                  >
                    Ver Foto
                  </Text>
                  <Icon
                    type="Ionicons"
                    name="receipt-outline"
                    style={{
                      fontSize: 80,
                      left: 27,
                      color: "white",
                      marginTop: "20%",
                      opacity: 0.1,
                    }}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={{
                backgroundColor: "#F28D80",
                padding: 10,
                width: 200,
                alignSelf: "center",
                marginTop: 10,
              }}
            >
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <Text
                  style={{
                    color: "white",
                    marginLeft: 10,
                    fontWeight: "bold",
                    textTransform: "uppercase",
                  }}
                >
                  Pendiente
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.box3]}>
            <Text style={{ fontSize: 15, lineHeight: 25 }}>
              Conoce las razones de revisión de tu factura:
            </Text>
          </View>
          <View style={[styles.box4]}>
            <FlatList
              data={[
                { key: "Fotografía borrosa e ilegible." },
                { key: "Información de compra incompleta." },
                {
                  key: "Productos no coinciden con el portafolio 3M seleccionado.",
                },
                { key: "Marcas, rayones y/o tachones en la factura." },
                { key: "Distribuidor y/o punto de compra no autorizado." },
              ]}
              renderItem={({ item }) => (
                <View style={{ flexDirection: "row" }}>
                  <Text style={{ color: "#F28D80" }}>■</Text>
                  <Text style={{ marginHorizontal: 10 }}>{item.key}</Text>
                </View>
              )}
            />
          </View>

          <View style={{ width: "90%", height: 80 }}>
            <Text>
              ¡Nuestro equipo está revisando tu factura! Pronto recibirás una
              respuesta.¡En caso de no recibir respuesta comunícate con
              nosotros.!
            </Text>
          </View>

          <View style={[styles.box6]}>
            <Text
              style={{
                alignSelf: "flex-start",
                color: "gray",
                marginTop: 20,
                fontWeight: "bold",
              }}
            >
              Contacto
            </Text>
            <Text
              style={{
                fontSize: 15,
                fontWeight: "bold",
                lineHeight: 25,
                color: "gray",
              }}
              onPress={() => Linking.openURL("mailto:dentapp@peoplecontact.cc")}
            >
              <Icon
                name="email-check"
                type="MaterialCommunityIcons"
                style={{ color: "gray", fontSize: 16 }}
              />{" "}
              dentapp@peoplecontact.cc
            </Text>
            <TouchableOpacity
              style={{ flexDirection: "row", alignItems: "center" }}
              onPress={sendMsg}
            >
              <Icon
                name="whatsapp"
                type="FontAwesome"
                style={{ color: "green", fontSize: 16 }}
              />
              <Text
                style={{ marginLeft: 3, fontWeight: "bold", color: "green" }}
              >
                WhatsApp:{" "}
              </Text>
              <Text style={{ textDecorationLine: "underline" }}>
                +57 3125614712
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>

      <Modal
        animationType="fade"
        presentationStyle="fullScreen"
        visible={modalVisible}
        statusBarTranslucent
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          
                {loading ? (
                  <Spinner color="red" style={styles.spinner} />
                ) : (
                  <Image style={styles.imagenFactura} source={{ uri: img }} />
                )}
          
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Cerrar</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "red",
  },
  content: {
    alignItems: "center",
    marginTop: 30,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  footer11: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 30,
    paddingHorizontal: 20,
  },
  box2: {
    width: "100%",
    height: 250,
  },
  box3: {
    width: "90%",
    height: 50,
  },
  box4: {
    width: "90%",
    height: 150,
    borderBottomWidth: 1,
    marginBottom: 20,
  },
  box5: {
    width: "90%",
    height: 100,
  },
  box6: {
    width: "90%",
    height: 100,
    marginBottom: 80,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    backgroundColor: "white",
  },
  button: {
    padding: 14,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: "#E7344C",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 16,
    textTransform: "uppercase",
  },
  imagenFactura: {
    alignSelf: "center",
    resizeMode: "contain",
    width: Dimensions.get("screen").width,
    height: Dimensions.get("screen").height,
  },
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
