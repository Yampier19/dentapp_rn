import React, { useState } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Modal,
  TouchableOpacity,
} from "react-native";
import { Button, TextArea } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import s from "@Public/Css/style";
import { TextInput } from "react-native-paper";

export default function FacturaConfirmadaScreen({ route, navigation }) {
  const { factura, estado, puntos } = route.params || {};
  const [state, setState] = useState(estado);

  const functionOnpress = () => {
    setState("");
    navigation.navigate("FacturaAceptada", {
      factura: factura,
      puntos: puntos,
    });
    //setModalVisible(!modalVisible)
  };

  const functionOnpress2 = () => {
    setState("");
    
    navigation.navigate("FacturaPendiente", { factura: factura });
    
    //setModalVisible2(!modalVisible2)
  };

  const functionOnpress3 = () => {
    setState("");
    
    navigation.navigate("FacturaPendiente", { factura: factura });
    
    //setModalVisible3(!modalVisible3)
  };

  return (
    <View style={s.container}>
      <ScrollView style={{ backgroundColor: "#FBFBFB" }}>
        <View style={[styles.content]}>
          <View style={[styles.box0]}>
            <Text
              style={{
                marginTop: 20,
                fontWeight: "bold",
                color: "black",
                fontSize: 20,
              }}
            >
              Resumen de factura
            </Text>
          </View>
          <View style={[styles.box1]}>
            <Text
              style={{
                alignSelf: "flex-end",
                color: "gray",
                fontWeight: "bold",
              }}
            >
              Datos
            </Text>
          </View>
          <View style={[styles.box2]}>
              <View
                stackedText
                style={{ width: "90%", borderBottomColor: "red" }}
              >
                <Text style={{ color: "gray" }}>Nombres del comprador</Text>
                <TextInput
                  disabled
                  value={
                    factura?.distribuidor
                      ? factura.distribuidor
                      : "No hay datos"
                  }
                />
              </View>
              <View
                stackedText
                style={{
                  borderBottomColor: "red",
                  width: "90%",
                  marginVertical: 20,
                }}
              >
                <Text style={{ color: "gray" }}>Valor de la compra</Text>
                <TextInput
                  disabled
                  value={`${
                    factura.monto_total
                      ? factura.monto_total
                          .toString()
                          .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                      : "No hay datos"
                  }`}
                />
              </View>
              <View
                stackedText
                style={{ borderBottomColor: "red", width: "90%" }}
              >
                <Text style={{ color: "gray" }}>
                  Descripción de la compra
                </Text>
                <TextArea
                  disabled
                  value={
                    factura?.descripcion
                      ? factura.descripcion
                      : "No hay descripción"
                  }
                  style={{
                    padding: 10,
                    marginTop: 30,
                    width: "100%",
                    alignSelf: "center",
                    borderBottomWidth: 1,
                    borderBottomColor: "red",
                  }}
                  rowSpan={5}
                  bordered
                />
              </View>
              <Text
                style={{
                  color: "gray",
                  marginTop: 20,
                  fontWeight: "bold",
                  marginLeft: 20,
                  textDecorationLine: "underline",
                }}
              >
                Archivo Adjunto - IMG_JPG
              </Text>
              {state === "aceptado" ? (
                <View
                  stackedText
                  style={{
                    borderBottomColor: "red",
                    width: "90%",
                    marginVertical: 20,
                  }}
                >
                  <Text style={{ color: "gray" }}>Puntos ganados</Text>
                  <TextInput disabled placeholder={`${puntos}`} />
                </View>
              ) : null}
            
          </View>
          <View
            style={{
              width: "90%",
              height: 100,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              style={{
                backgroundColor: "#64C2C8",
                paddingHorizontal: 50,
                paddingVertical: 10,
              }}
              onPress={() => navigation.navigate("Facturas")}
            >
              <Text style={{ color: "#fff" }}>CARGAR NUEVA FACTURA</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={state !== "" && state !== undefined}
            onRequestClose={() => {
              setState("");
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                {state === "aceptado" ? (
                  <React.Fragment>
                    <Icon
                      name="check-circle"
                      type="FontAwesome5"
                      style={{ color: "green", marginBottom: 20, fontSize: 50 }}
                    />
                    <Text
                      style={{
                        fontSize: 20,
                        marginBottom: "5%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      La factura se ha cargado exitosamente
                    </Text>

                    <Text
                      style={{
                        fontSize: 20,
                        color: "#64C2C8",
                        marginBottom: "5%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Usted ha ganado {puntos} puntos
                    </Text>

                    <Button
                      onPress={functionOnpress}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        CERRAR
                      </Text>
                    </Button>
                  </React.Fragment>
                ) : state === "pendiente" ? (
                  <React.Fragment>
                    <Icon
                      name="exclamation-circle"
                      type="FontAwesome5"
                      style={{
                        color: "rgba(231, 52, 76, 1)",
                        marginBottom: 20,
                        fontSize: 50,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Tu factura se encuentra en estado:
                    </Text>
                    <Text style={{ fontWeight: "bold" }}>
                      PENDIENTE POR APROBACIÓN{" "}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "gray",
                        marginVertical: "6%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Estamos verificando tu compra, regresa en 72h para saber
                      más.
                    </Text>

                    <Button
                      onPress={functionOnpress2}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        VER DETALLES
                      </Text>
                    </Button>
                  </React.Fragment>
                ) : state === "rechazado" ? (
                  <React.Fragment>
                    <Icon
                      name="exclamation-circle"
                      type="FontAwesome5"
                      style={{
                        color: "#E7344C",
                        marginBottom: 30,
                        fontSize: 50,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 18,
                        marginHorizontal: 30,
                        textAlign: "center",
                        marginBottom: 70,
                      }}
                    >
                      Tu factura ha sido rechazada.
                    </Text>
                    <Button
                      onPress={functionOnpress3}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        CERRAR
                      </Text>
                    </Button>
                  </React.Fragment>
                ) : null}
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box0: {
    width: "90%",
    height: 50,
    marginBottom: 10,
  },
  box1: {
    width: "90%",
    height: 30,
    marginTop: 10,
  },
  box2: {
    width: "90%",
    height: 500,
    backgroundColor: "#F1F1F1",
  },

  box3: {
    width: "90%",
    height: 200,
  },
  box4: {
    width: "90%",
    height: 150,
    marginBottom: 10,
  },
  box5: {
    width: "90%",
    height: 150,
    marginBottom: 10,
  },
});
