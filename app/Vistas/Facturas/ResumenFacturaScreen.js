import React, { useState } from "react";
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Modal,
  Pressable,
  TouchableOpacity,
} from "react-native";
import { Item, Input, Form, Label, Button, Textarea } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import * as Animatable from "react-native-animatable";
import s from "@Public/Css/style";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";

export default function ResumenFacturaScreen({ route, navigation }) {
  const { factura, estado, puntos } = route.params || {};
  console.log("factura", route.params);

  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [modalVisible3, setModalVisible3] = useState(false);
  const [modalVisible4, setModalVisible4] = useState(false);
  {
    /* TODOS LOS MODALES SON DE PRUEBA PARA QUE PUEDAS CONOCER EL CONTENIDO,
    TODO ESTO DEBE QUEDAR EN UN SOLO MODAL Y VARIAR LA DATA SEGUN CORRESPONDA */
  }

  const functionOnpress = () => {
    navigation.navigate("FacturaConfirmada", { factura: factura });
    //setModalVisible(!modalVisible)
  };

  const functionOnpress2 = () => {
    navigation.navigate("FacturaPendiente", { factura: factura });
    //setModalVisible2(!modalVisible2)
  };

  const functionOnpress3 = () => {
    navigation.navigate("FacturaRechazada", { factura: factura });
    //setModalVisible3(!modalVisible3)
  };

  const functionOnpress4 = () => {
    navigation.navigate("FacturaAceptada", { factura: factura });
    //setModalVisible4(!modalVisible4)
  };

  return (
    <View style={s.container}>
      <ScrollView style={{ backgroundColor: "#FBFBFB" }}>
        <View style={[styles.content]}>
          <View style={[styles.box0]}>
            <Text
              style={{
                marginTop: 20,
                fontWeight: "bold",
                color: "black",
                fontSize: 20,
              }}
            >
              Resumen de factura
            </Text>
          </View>
          <View style={[styles.box1]}>
            <Text
              style={{
                alignSelf: "flex-end",
                color: "gray",
                fontWeight: "bold",
              }}
            >
              Datos
            </Text>
          </View>
          <View style={[styles.box2]}>
            <Form>
              <Item
                stackedLabel
                style={{ width: "90%", borderBottomColor: "red" }}
              >
                <Label style={{ color: "gray" }}>Nombres del comprador</Label>
                <Input disabled value={factura?.distribuido} />
              </Item>
              <Item
                stackedLabel
                style={{
                  borderBottomColor: "red",
                  width: "90%",
                  marginVertical: 20,
                }}
              >
                <Label style={{ color: "gray" }}>Valor de la compra</Label>
                <Input disabled value={`${factura?.monto_total}`} />
              </Item>
              <Item
                stackedLabel
                style={{ borderBottomColor: "red", width: "90%" }}
              >
                <Label style={{ color: "gray" }}>
                  Descripcion de la compra
                </Label>
                <Textarea
                  disabled
                  value={
                    factura?.descripcion
                      ? factura.descripcion
                      : "No hay descripcion"
                  }
                  style={{
                    padding: 10,
                    marginTop: 30,
                    width: "100%",
                    alignSelf: "center",
                    borderBottomWidth: 1,
                    borderBottomColor: "red",
                  }}
                  rowSpan={5}
                  bordered
                />
              </Item>
            </Form>
            <Text
              style={{
                color: "gray",
                marginTop: 20,
                fontWeight: "bold",
                marginLeft: 20,
                textDecorationLine: "underline",
              }}
            >
              Archivo Adjunto - IMG_JPG
            </Text>
          </View>
          {/* <View style={{ width: '90%', height: 380, marginTop: 30, justifyContent: 'center', alignItems: 'center' }}>

                        <Animatable.Image
                            easing="ease-out"
                            iterationCount="infinite"
                            style={
                                {
                                    width: '40%',
                                    height: '45%',
                                }
                            }
                            source={require('@Public/Img/factura.gif')}
                        />
                        <Text style={{ color: 'rgba(231, 52, 76, 1)' }}>CALCULANDO PUNTOS</Text>
                        <Text style={{marginTop: 20}} onPress={() => setModalVisible(true)}>Ver resumen factura cargada</Text>
                   <Text style={{marginVertical: 20}}  onPress={() => setModalVisible2(true)}>Ver pendiente</Text>
                   <Text onPress={() => setModalVisible3(true)}>Ver Rechazada</Text>
                   <Text style={{marginVertical: 20}} onPress={() => setModalVisible4(true)}>Ver Aceptada</Text>
                    </View> */}
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={estado !== ""}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible(!modalVisible);
              estado = "";
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                {estado === "aceptado" ? (
                  <React.Fragment>
                    <Icon
                      name="check-circle"
                      type="FontAwesome5"
                      style={{ color: "green", marginBottom: 20, fontSize: 50 }}
                    />
                    <Text
                      style={{
                        fontSize: 20,
                        marginBottom: "5%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      La factura se ha cargado exitosamente
                    </Text>

                    <Text
                      style={{
                        fontSize: 20,
                        color: "#64C2C8",
                        marginBottom: "5%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Usted ha ganado {puntos} puntos
                    </Text>

                    <Button
                      onPress={functionOnpress}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        CERRAR
                      </Text>
                    </Button>
                  </React.Fragment>
                ) : estado === "pendiente" ? (
                  <React.Fragment>
                    <Icon
                      name="exclamation-circle"
                      type="FontAwesome5"
                      style={{
                        color: "rgba(231, 52, 76, 1)",
                        marginBottom: 20,
                        fontSize: 50,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Tu factura se encuentra en estado:
                    </Text>
                    <Text style={{ fontWeight: "bold" }}>
                      PENDIENTE POR APROBACIÓN{" "}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "gray",
                        marginVertical: "6%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Estamos verificando tu compra, regresa en 72h para saber
                      más.
                    </Text>

                    <Button
                      onPress={functionOnpress2}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        VER DETALLES
                      </Text>
                    </Button>
                  </React.Fragment>
                ) : estado === "rechazado" ? (
                  <React.Fragment>
                    <Icon
                      name="exclamation-circle"
                      type="FontAwesome5"
                      style={{
                        color: "rgba(231, 52, 76, 1)",
                        marginBottom: 20,
                        fontSize: 50,
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 16,
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Tu factura se encuentra en estado:
                    </Text>
                    <Text style={{ fontWeight: "bold" }}>
                      PENDIENTE POR APROBACIÓN{" "}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "gray",
                        marginVertical: "6%",
                        marginHorizontal: 30,
                        textAlign: "center",
                      }}
                    >
                      Estamos verificando tu compra, regresa en 72h para saber
                      más.
                    </Text>

                    <Button
                      onPress={functionOnpress3}
                      style={{
                        backgroundColor: "#64C2C8",
                        justifyContent: "center",
                        borderBottomEndRadius: 20,
                        borderBottomLeftRadius: 20,
                        width: "100%",
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "bold",
                          color: "white",
                        }}
                      >
                        VER DETALLES
                      </Text>
                    </Button>
                  </React.Fragment>
                ) : null}
              </View>
            </View>
          </Modal>
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible2}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible2(!modalVisible2);
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                <Icon
                  name="exclamation-circle"
                  type="FontAwesome5"
                  style={{
                    color: "rgba(231, 52, 76, 1)",
                    marginBottom: 20,
                    fontSize: 50,
                  }}
                />
                <Text
                  style={{
                    fontSize: 16,
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  Tu factura se encuentra en estado:
                </Text>
                <Text style={{ fontWeight: "bold" }}>
                  PENDIENTE POR APROBACIÓN{" "}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: "gray",
                    marginVertical: "6%",
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  Estamos verificando tu compra, regresa en 72h para saber más.
                </Text>

                <Button
                  onPress={functionOnpress2}
                  style={{
                    backgroundColor: "#64C2C8",
                    justifyContent: "center",
                    borderBottomEndRadius: 20,
                    borderBottomLeftRadius: 20,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{ fontSize: 15, fontWeight: "bold", color: "white" }}
                  >
                    VER DETALLES
                  </Text>
                </Button>
              </View>
            </View>
          </Modal>
        </View>

        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible3}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible3(!modalVisible3);
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                <Icon
                  name="exclamation-circle"
                  type="FontAwesome5"
                  style={{
                    color: "rgba(231, 52, 76, 1)",
                    marginBottom: 20,
                    fontSize: 50,
                  }}
                />
                <Text
                  style={{
                    fontSize: 16,
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  Tu factura se encuentra en estado:
                </Text>
                <Text style={{ fontWeight: "bold" }}>
                  PENDIENTE POR APROBACIÓN{" "}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: "gray",
                    marginVertical: "6%",
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  Estamos verificando tu compra, regresa en 72h para saber más.
                </Text>

                <Button
                  onPress={functionOnpress3}
                  style={{
                    backgroundColor: "#64C2C8",
                    justifyContent: "center",
                    borderBottomEndRadius: 20,
                    borderBottomLeftRadius: 20,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{ fontSize: 15, fontWeight: "bold", color: "white" }}
                  >
                    VER DETALLES
                  </Text>
                </Button>
              </View>
            </View>
          </Modal>
        </View>
        <View style={styles.centeredView}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible4}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
              setModalVisible4(!modalVisible4);
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  width: "90%",
                  height: 250,
                  backgroundColor: "white",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  borderRadius: 20,
                }}
              >
                <Icon
                  name="exclamation-circle"
                  type="FontAwesome5"
                  style={{
                    color: "rgba(231, 52, 76, 1)",
                    marginBottom: 20,
                    fontSize: 50,
                  }}
                />
                <Text
                  style={{
                    fontSize: 16,
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  Tu factura se encuentra en estado:
                </Text>
                <Text style={{ fontWeight: "bold" }}>
                  PENDIENTE POR APROBACIÓN{" "}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: "gray",
                    marginVertical: "6%",
                    marginHorizontal: 30,
                    textAlign: "center",
                  }}
                >
                  Estamos verificando tu compra, regresa en 72h para saber más.
                </Text>

                <Button
                  onPress={functionOnpress4}
                  style={{
                    backgroundColor: "#64C2C8",
                    justifyContent: "center",
                    borderBottomEndRadius: 20,
                    borderBottomLeftRadius: 20,
                    width: "100%",
                  }}
                >
                  <Text
                    style={{ fontSize: 15, fontWeight: "bold", color: "white" }}
                  >
                    VER DETALLES
                  </Text>
                </Button>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box0: {
    width: "90%",
    height: 50,
    marginBottom: 10,
  },
  box1: {
    width: "90%",
    height: 30,
    marginTop: 10,
  },
  box2: {
    width: "90%",
    height: 400,
    backgroundColor: "#F1F1F1",
  },

  box3: {
    width: "90%",
    height: 200,
  },
  box4: {
    width: "90%",
    height: 150,
    marginBottom: 10,
  },
  box5: {
    width: "90%",
    height: 150,
    marginBottom: 10,
  },
});
