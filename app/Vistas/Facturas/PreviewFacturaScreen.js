import React from "react";
import { useState } from "react";
import { View } from "react-native";
import { ScrollView } from "react-native";
import { Text } from "react-native";
import { StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native";
import { Modal } from "react-native";
import { Icon } from "native-base";
import { Item } from "native-base";
import { When } from "react-if";
import { Unless } from "react-if";
import GridImageView from "react-native-grid-image-viewer";
import { WebView } from "react-native-webview";
import * as Print from 'expo-print'

import s from "@Public/Css/style";
import TabNavigatorScreen from "@Vistas/Navigation/TabNavigatorScreen";
import ModalComponent from "../../Components/Modal";
import LoadingScreen from "../../Components/LoadingGif";
import { Upload } from "../../api/api";

const PreviewFacturaScreen = ({ route, navigation }) => {
  const [viewPdf, setViewPdf] = useState(false);
  const { typeImage, image } = route.params;
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const [loading, setLoading] = useState(false);
  
  
  const view = async () => {
    setViewPdf(true);
    await Print.printAsync({
      uri: image,
    });
  };

  const upload = async () => {
    let formData = new FormData();
    try {
      if (typeImage === "application/pdf") {
        formData.append("foto", {
          uri: image,
          type: "application/pdf",
          name: "factura",
        });
      } else if (typeImage === "image/jpeg") {
        formData.append("foto", {
          uri: image,
          type: "image/jpeg",
          name: "factura",
        });
      }
      
      setLoading(true);
      let response = null;
      //error de autenticación
      response = await Upload("factura/store", formData);

      if (response !== null) {
        const result = await response.json();
        console.log(result);
        setLoading(false);
        if (result?.status === "success") {
          navigation.navigate("FacturaConfirmada", {
            factura: result?.datos,
            estado: result?.estado,
            puntos: result?.puntos,
          });
        } else if (result?.status === "error") {
          setDialog({
            visible: true,
            message: result?.message,
            type: "error",
            fc: () => navigation.navigate("Facturas"),
          });
        }
      }
    } catch (error) {
      setLoading(false);
      setDialog({
        visible: true,
        message:
          "Ocurrió un error al momento de subir el archivo, intentelo nuevamente",
        type: "error",
        fc: () => navigation.navigate("Facturas"),
      });
    }
  };
  return (
    <View style={s.container}>
      <When condition={!loading}>
        <View style={[styles.content]}>
          <View style={[styles.box1]}>
            <Text style={{ fontSize: 20, textAlign: "center" }}>
              Archivo adjuntado
            </Text>
          </View>
          <View style={[styles.box2]}>
            <When condition={typeImage === "image/jpeg"}>
              <View style={styles.background}>
                <Text style={styles.explore_text}>
                  Pulsa la imagen para hacer zoom
                </Text>
                <View style={{ flex: 1, width: 700 }}>
                  <GridImageView data={[{ image: image }]} />
                </View>
              </View>
            </When>
            <Unless condition={typeImage === "image/jpeg"}>
              <View style={styles.background}>
                <Text style={styles.explore_text}>
                  Archivo cargado exitosamente
                </Text>
                <TouchableOpacity
                  onPress={() => view()}
                  style={{
                    backgroundColor: "#64C2C8",
                    padding: 10,
                    width: 193,
                    alignSelf: "center",
                    borderRadius: 10,
                  }}
                >
                  <View
                    style={{ flexDirection: "row", justifyContent: "center" }}
                  >
                    <Icon
                      type="Ionicons"
                      name="search"
                      style={{ color: "white" }}
                    />
                    <Text
                      style={{ color: "white", marginTop: 5, marginLeft: 10 }}
                    >
                      Ver Archivo
                    </Text>
                  </View>
                </TouchableOpacity>
                
              </View>
            </Unless>
            <TouchableOpacity
              onPress={() => upload()}
              style={{
                backgroundColor: "#64C2C8",
                padding: 10,
                width: 293,
                alignSelf: "center",
              }}
            >
              <View style={{ flexDirection: "row", justifyContent: "center" }}>
                <Icon
                  type="Ionicons"
                  name="cloud-done-sharp"
                  style={{ color: "white" }}
                />
                <Text style={{ color: "white", marginTop: 5, marginLeft: 10 }}>
                  SUBIR FACTURA
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </When>
      <Unless condition={!loading}>
        <LoadingScreen />
      </Unless>

      <ModalComponent onClose={setDialog} {...dialog} />
    </View>
  );
};

export default PreviewFacturaScreen;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainer: {
    width: 400,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  imageContainer: {
    padding: 30,
  },
  image: {
    width: 400,
    height: 300,
    resizeMode: "cover",
  },
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  background: {
    backgroundColor: "#E7344C",
    flex: 1,
    padding: 30,
  },
  headline_text: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold",
    marginTop: 50,
    marginLeft: 20,
  },
  explore_text: {
    marginTop: 20,
    marginBottom: 40,
    color: "white",
    fontSize: 15,
    fontWeight: "600",
    textAlign: "center",
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 80,
    marginTop: 50,
    paddingHorizontal: 20,
  },
  box2: {
    width: "80%",
    height: 420,
    borderTopWidth: 10,
    borderBottomWidth: 10,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderColor: "rgba(0,0,0,0.1)",
  },

  content11: {
    alignItems: "center",
    marginTop: 30,
  },
  footer11: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box11: {
    width: "100%",
    height: 80,
    paddingHorizontal: 20,
  },
  box12: {
    width: "100%",
    height: 310,
  },
});
