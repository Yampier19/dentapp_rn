import React, { useState, useEffect, useCallback } from "react";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { TextInput } from "react-native-paper";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  Image,
  Dimensions,
  StyleSheet
} from "react-native";
import { Item, Input, Form, Button, Icon, Spinner } from "native-base";
import publicIP from "react-native-public-ip";
import { When } from "react-if";
import { Unless } from "react-if";
import { Ionicons, FontAwesome } from '@expo/vector-icons';

import login from "@Public/Css/Login";
import spinner from "@Public/Css/spinner";
import ModalComponent from "../../Components/Modal";
import { Request } from "../../api/api";
import { setToken, setLocalEmail } from "../../utils/utils";
import PushNotification from "../../Components/PushNotificacion";
import { useLogin } from "../../services/dentapp/auth/useAuth";

export default function LoginScreen(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [viewPassword, setViewPassword] = useState(true);
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const [text, setText] = React.useState("");
  const [logged, setLogged] = useState(false);
  const { mutateAsync: Login, isLoading } = useLogin();
  const { width, height } = Dimensions.get("window");



  const onClickLogin = async () => {
    try {
      if (error !== "" && password !== "") {
        if (password.length <= 6) {
          setDialog({
            visible: true,
            message: "La contraseña debe tener mínimo 6 caracteres",
            type: "warning",
          });
          return false;
        }
        if (isValidEmail(email)) {
          const response = await Login({ email, password });
          console.log("response", response);
          if (response?.status === "success") {
            verifyIP();
            setToken(response.accessToken);
            setLocalEmail(email);
            setLogged(true);
            props.navigation.push("Home", {
              reload: JSON.stringify(new Date()),
            });
          } else if (response?.status === "error") {
            if (response?.distribuidor === false) {
              props.navigation.navigate("Distribuidor", {
                email: email,
                password,
              });
            } else {
              const message =
                response?.message ||
                "Ocurrió un error al intentar iniciar sesión, por favor inténtelo nuevamente.";
              setDialog({
                visible: true,
                message: message,
                type: "error",
              });
            }
          } else {
            const message =
              response?.message ||
              "Ocurrió un error al intentar iniciar sesión, por favor inténtelo nuevamente.";
            setDialog({
              visible: true,
              message: message,
              type: "error",
            });
          }
        } else {
          setDialog({
            visible: true,
            message: "Por favor ingresa un correo válido",
            type: "warning",
          });
        }
      } else {
        setDialog({
          visible: true,
          message: "Tiene campos vacios, por favor verifique",
          type: "warning",
        });
      }
    } catch (error) { }
  };

  const isValidEmail = (email) => {
    const res =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return res.test(email.toLowerCase());
  };

  const onChangePassword = (text) => {
    if (text.length > 20) {
      // setError('La contraseña debe tener máximo 20 caracteres');
      setDialog({
        visible: true,
        message: "La contraseña debe tener máximo 20 caracteres",
        type: "warning",
      });
    } else {
      setError(null);
      setPassword(text);
    }
  };

  const verifyIP = async () => {
    try {
      const ip = await publicIP();
      await Request("POST", `cliente/storeip`, { ip });
    } catch (error) { }
  };

  const login = StyleSheet.create({

    bottomActive: {
      width: responsiveScreenWidth(30),
      backgroundColor: '#65C1C7',
      borderRadius: 10,
      borderWidth: 2,
      borderColor: '#65C1C7',
      padding: 5
    },

    bottomInactive: {
      width: responsiveScreenWidth(30),
      borderRadius: 10,
      borderWidth: 2,
      borderColor: '#65C1C7',
      padding: 5,
    },

    textActive: {
      textAlign: 'center',
      fontSize: responsiveScreenFontSize(2),
      color: 'white'
    },

    textInactive: {
      textAlign: 'center',
      fontSize: responsiveScreenFontSize(2),
      color: '#65C1C7'
    },
    input: {
      backgroundColor: 'transparent',
      height: 60,
      justifyContent: "center"
    }

  })


  /* onClickLogin */
  return (
    <View style={{ flex: 1, alignItems: "center", backgroundColor: "white" }}>
      <When condition={!isLoading}>
        <ScrollView>
          <View style={{ width: responsiveScreenWidth(100), height: responsiveScreenHeight(8), alignItems: 'center' }}>
            <View style={{ width: responsiveScreenWidth(90), marginVertical: 20 }}>
              <Text style={{ fontSize: responsiveScreenFontSize(3), fontWeight: 'bold', textAlign: 'center' }}>
                Iniciar sesión
              </Text>
            </View>
          </View>
          <View style={{ width: responsiveScreenWidth(100), alignItems: 'center', justifyContent: 'center', marginVertical: 20 }}>
            <View
              style={{
                width: responsiveScreenWidth(90),
                justifyContent: "center",
                marginBottom: 10
              }}
            >
              <TextInput
                keyboardType="email-address"
                autoCapitalize="none"
                selectionColor="#64C2C8"
                autoCorrect={false}
                style={login.input}
                value={email}
                onChangeText={setEmail}
                theme={{
                  colors: { primary: "#64C2C8" },
                }}
                label="Correo electrónico*"
                left={<TextInput.Icon name="account" color="#64C2C8" />}
              />
            </View>
            <View
              style={{
                width: responsiveScreenWidth(90),
                justifyContent: "center",
              }}
            >
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                textContentType="password"
                autoCompleteType="password"
                selectionColor="#64C2C8"
                password={password}
                onChangeText={onChangePassword}
                style={login.input}
                label="Contraseña*"
                secureTextEntry={viewPassword}
                theme={{
                  colors: { primary: "#64C2C8" },
                }}
                right={
                  <TextInput.Icon
                    name="eye"
                    color="#64C2C8"
                    onPress={() => setViewPassword(!viewPassword)}
                  />
                }
                left={<TextInput.Icon name="key" color="#64C2C8" />}
              />
            </View>
          </View>
          <View style={{ width: responsiveScreenWidth(100), alignItems: 'center' }}>
            <View style={{ width: responsiveScreenWidth(90), alignItems: 'center', marginBottom: 10 }}>
              <TouchableOpacity onPress={onClickLogin} style={{ width: responsiveScreenWidth(90), backgroundColor: '#E7334C', borderRadius: 10, padding: 10 }}>
                <Text style={{ color: 'white', textAlign: 'center', fontSize: responsiveScreenFontSize(2) }}>Ingresar</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              width: responsiveScreenWidth(100),
              alignItems: "center",
              marginVertical: 20,
            }}
          >
            <Text
              onPress={() => {
                props.navigation.navigate("OlvidoPass");
              }}
              style={{
                color: "#E7344C",
                fontSize: responsiveScreenFontSize(1.8),
              }}
            >
              ¿Olvidaste tu contraseña?
            </Text>
          </View>
          <View
            style={{
              width: responsiveScreenWidth(100),
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate("Help");
              }}
              style={{
                flexDirection: "row",
                backgroundColor: "#E7344C",
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 10,
                marginBottom: 10,
                width: responsiveScreenWidth(50),
                height: responsiveScreenHeight(5),
              }}
            >
              <Icon
                as={Ionicons}
                name={Platform.OS ? 'ios-help' : 'md-help'}
                size="15"
                color="white"
              />
              <Text
                style={{
                  color: "white",
                  fontSize: responsiveScreenFontSize(2),
                }}
              >
                Ayuda
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: responsiveScreenWidth(100),
              alignItems: "center",
              justifyContent: "flex-start",
              marginTop: 20,
            }}
          >
            <Text style={{ color: "gray", fontSize: responsiveScreenFontSize(1.8) }}>
              Versión: 2.0
            </Text>
          </View>
        </ScrollView>
      </When>

      <Unless condition={!isLoading}>
        <Spinner color="red" style={spinner.spinner} />
      </Unless>
      <ModalComponent onClose={setDialog} {...dialog} />
      {logged && <PushNotification email={email} />}
    </View>
  );
}
