import React, { useEffect } from 'react'
import { View, StatusBar } from 'react-native'
import * as Animatable from 'react-native-animatable'
import { imageBackgroundStyle } from '@Public/Css/General'
import { getToken } from '../../utils/utils'

const SplashScreen = ({ navigation }) => {

    const goToScreen = () => {
        setTimeout(async () => {
            const token = await getToken();
            if(token){
                navigation.navigate('Home');
            }else {
                navigation.navigate('TutorialScreen');
            }
        }, 2500)
    }

    useEffect(() => {
        goToScreen();
    }, [])

    return (
        <View style={imageBackgroundStyle.image}>
            <StatusBar translucent backgroundColor='rgba(0,0,0,0.2)' />
            <Animatable.Image
                easing="ease-out"
                iterationCount="infinite"
                style={
                    {
                        width: '100%',
                        height: '100%'
                    }
            }
            source={require('@Public/Img/2.gif')}
            />
        </View>
    )
}

export default SplashScreen;