import React, { useState, useEffect } from "react";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  Image,
  Dimensions,
  StyleSheet,
  Modal,
} from "react-native";
import { Item, Input, Label, Form, Button, Icon, Spinner } from "native-base";
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { When } from "react-if";
import { Unless } from "react-if";
import { TextInput } from "react-native-paper";

import spinner from "@Public/Css/spinner";
import ModalComponent from "../../Components/Modal";
import { useSignUp } from "../../services/dentapp/auth/useAuth";

import PickerDoc from './../../Components/Picker/PickerDoc'
import PickerCity from './../../Components/Picker/PickerCity'
import PickerUser from './../../Components/Picker/PickerUser'
import PickerDist from './../../Components/Picker/PickerDist'
import PickerEsp from './../../Components/Picker/PickerEsp'

export default function RegisterScreen(props) {
  const [text, setText] = React.useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [viewPassword, setViewPassword] = useState(true);
  const [viewConfirmPassword, setViewConfirmPassword] = useState(true);
  const [userNew, setUserNew] = useState({
    nombre: "",
    apellido: "",
    telefono: "",
    correo: "",
    especialidad: "",
    distribuidor: "",
    document_number: "",
    document_type_id: "",
    loginrol_id: "",
    nombre_doctor: "",
    city_id: "",
    password: ""
  });
  const [dialog, setDialog] = useState({
    visible: false,
    message: "",
    type: "",
  });
  const [errorEmptyForm, setErrorEmptyForm] = useState(false);
  const { mutateAsync: createUser, isLoading } = useSignUp();

  useEffect(() => {
    validField();
  }, [userNew]);

  const functionOnpress = () => {
    console.log("El usuario ha sido registrado");
    props.navigation.navigate("Verify");
    setModalVisible(!modalVisible);
    setUserNew({
      nombre: "",
      apellido: "",
      telefono: "",
      correo: "",
      especialidad: "",
      distribuidor: "",
      document_number: "",
      document_type_id: "",
      loginrol_id: "",
      city_id: "",
      paswword: ""
    });
  };

  const validField = () => {
    if (
      userNew.apellido !== "" &&
      userNew.telefono !== "" &&
      userNew.nombre !== "" &&
      userNew.correo !== "" &&
      userNew.distribuidor !== "" &&
      userNew.especialidad !== "" &&
      userNew.document_number !== "" &&
      userNew.document_type_id !== "" &&
      userNew.loginrol_id !== "" &&
      userNew.city_id !== "" &&
      userNew.password !== ""
    ) {
      setErrorEmptyForm(false);
    } else {
      setErrorEmptyForm(true);
    }
  };

  const singUp = async () => {
    try {
      validField();
      if (!errorEmptyForm) {
        if (isValidEmail(userNew.correo)) {
          let formData = new FormData();
          let user = {
            ...userNew,
          };
          Object.keys(user).forEach((key) => formData.append(key, user[key]));
          let res = await createUser(formData);
          console.log("res", res);
          if (res && res.status === "success") {
            setModalVisible(true);
          } else {
            setDialog({ visible: true, message: res.message, type: "error" });
          }
        } else {
          setDialog({
            visible: true,
            message: "Por favor ingresa un correo válido",
            type: "warning",
          });
        }
      } else {
        setDialog({
          visible: true,
          message: "Tiene campos vacios, por favor verifique",
          type: "warning",
        });
      }
    } catch (error) {
      setDialog({
        visible: true,
        message: "Acabo de ocurrir un problema!",
        type: "error",
      });
      console.log(error.response);
    }
  };

  const isValidEmail = (email) => {
    const res =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return res.test(email.toLowerCase());
  };


  return (
    <View style={{ flex: 1, alignItems: "center", backgroundColor: "white" }}>
      <When condition={!isLoading}>
        <ScrollView>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Nombre:</Text>
            <TextInput
              onChangeText={(e) => setUserNew({ ...userNew, nombre: e })}
              style={register.input}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Apellidos:</Text>
            <TextInput
              onChangeText={(e) => setUserNew({ ...userNew, apellido: e })}
              style={register.input}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Tipo de documento:</Text>
            <PickerDoc value={userNew.document_type_id}
              onChange={(e) => setUserNew({ ...userNew, document_type_id: e })} />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Número de documento:</Text>
            <TextInput
              style={register.input}
              onChangeText={(e) => setUserNew({ ...userNew, document_number: e })}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Correo electrónico:</Text>
            <TextInput
              onChangeText={(e) => setUserNew({ ...userNew, correo: e })}
              style={register.input}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Número de contacto:</Text>
            <TextInput
              type="outlined"
              keyboardType="numeric"
              onChangeText={(e) => setUserNew({ ...userNew, telefono: e })}
              style={register.input}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Tipo de usuario:</Text>
            <PickerUser value={userNew.loginrol_id}
              onChange={(e) => setUserNew({ ...userNew, loginrol_id: e })} />
          </View>

          {userNew.loginrol_id == '4' ?

            <View
              style={{
                justifyContent: "center",
                alignSelf: 'center',
                width: responsiveScreenWidth(90),
                height: responsiveScreenHeight(8),
                marginTop: responsiveScreenHeight(2.5)
              }}
            >
              <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Nombre de tu doctor a cargo:</Text>
              <TextInput
                type="outlined"
                onChangeText={(e) => setUserNew({ ...userNew, nombre_doctor: e })}
                style={register.input}
              />
            </View>

            : null}

          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Ciudad:</Text>
            <PickerCity value={userNew.city_id}
              onChange={(e) => setUserNew({ ...userNew, city_id: e })} />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Especialización:</Text>
            <PickerEsp value={userNew.especialidad}
              onChange={(e) => setUserNew({ ...userNew, especialidad: e })} />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Mi distribuidor:</Text>
            <PickerDist value={userNew.distribuidor}
              onChange={(e) => setUserNew({ ...userNew, distribuidor: e })} />
          </View>

          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Nueva contraseña:</Text>
            <TextInput
              left={<TextInput.Icon name="key" color={"#64C2C8"} />}
              onChangeText={(e) => setUserNew({ ...userNew, password: e })}
              style={register.input}
              secureTextEntry={viewPassword}
              right={
                <TextInput.Icon
                  name="eye"
                  onPress={() => setViewPassword(!viewPassword)}
                />
              }
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignSelf: 'center',
              width: responsiveScreenWidth(90),
              height: responsiveScreenHeight(8),
              marginTop: responsiveScreenHeight(2.5)
            }}
          >
            <Text style={{ fontWeight: 'bold', fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Confirma tu nueva contraseña:</Text>
            <TextInput
              secureTextEntry={viewConfirmPassword}
              left={<TextInput.Icon name="key" color={"#64C2C8"} />}
              right={
                <TextInput.Icon
                  name="eye"
                  onPress={() => setViewConfirmPassword(!viewConfirmPassword)}
                />
              }
              style={register.input}
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              width: responsiveScreenWidth(90),
              alignSelf: "center",
              alignItems: "center",
              height: responsiveScreenHeight(12),
            }}
          >
            <Text
              style={{
                fontSize: responsiveScreenFontSize(2),
                color: "#64C2C8",
              }}
            >
              *Recuerda que solo ganarás puntos por registrar facturas del
              distribuidor que elijas y no podrás cambiarlo en 60 días.
            </Text>
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              width: responsiveScreenWidth(100),
              height: responsiveScreenHeight(10),
              marginBottom: 20,
            }}
          >
            <TouchableOpacity
              onPress={singUp}
              style={{
                width: responsiveScreenWidth(60),
                height: responsiveScreenHeight(5),
                borderRadius: 10,
                backgroundColor: "#E7334C",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(2),
                  color: "white",
                }}
              >
                REGISTRARSE
              </Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </When>
      <Unless condition={!isLoading}>
        <View
          style={{
            width: "90%",
            height: 710,
            alignSelf: "center",
            padding: 10,
            marginTop: 10,
          }}
        >
          <Spinner color="red" style={spinner.spinner} />
        </View>
      </Unless>

      <ModalComponent onClose={setDialog} {...dialog} />

      <View style={styles.centeredView}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.box_content}>
              <View style={styles.box_icon}>
                <Icon
                  as={Ionicons}
                  name={Platform.OS ? 'ios-check' : 'md-check'}
                  size="15"
                  color="white"
                />
              </View>
              <View style={styles.box_text}>
                <Text style={styles.text}>Te has registrado exitosamente</Text>
              </View>
              <View style={styles.box_buttom}>
                <TouchableOpacity
                  style={styles.buttom}
                  onPress={functionOnpress}
                >
                  <Text style={styles.text_buttom}>CERRAR</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const register = StyleSheet.create({
  input: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#D9D9D9",
    height: 40,
    width: responsiveScreenWidth(90),
  },
});

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  box_content: {
    backgroundColor: "white",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(30),
    borderRadius: 10,
    alignItems: "center",
  },
  box_icon: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(10),
  },
  icon: {
    color: "#64C2C8",
    fontSize: responsiveScreenFontSize(8),
  },
  box_text: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "center",
    height: responsiveScreenHeight(12),
  },
  text: {
    fontSize: responsiveScreenFontSize(2.8),
    marginHorizontal: responsiveScreenWidth(1),
    textAlign: "center",
  },
  text2: {
    fontSize: responsiveScreenFontSize(2),
    marginHorizontal: responsiveScreenWidth(10),
    textAlign: "center",
  },
  box_buttom: {
    width: responsiveScreenWidth(90),
    alignItems: "center",
    justifyContent: "flex-end",
    height: responsiveScreenHeight(8),
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  buttom: {
    backgroundColor: "#64C2C8",
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(6),
    justifyContent: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  text_buttom: {
    fontSize: responsiveScreenFontSize(2),
    textAlign: "center",
    color: "white",
  },
});
