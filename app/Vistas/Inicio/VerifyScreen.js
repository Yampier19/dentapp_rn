import React from "react";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { Text, View, ScrollView, TouchableOpacity } from "react-native";
import { Icon } from "native-base";

export default function VerifyScreen(props) {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
      }}
    >
      <ScrollView>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            width: responsiveScreenWidth(90),
            height: responsiveScreenHeight(30),
          }}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              width: responsiveScreenWidth(40),
              height: responsiveScreenHeight(18),
              borderRadius: 100,
              borderColor: "black",
              borderWidth: 8,
            }}
          >
            <Icon
              type="FontAwesome5"
              name="envelope"
              style={{ fontSize: responsiveScreenFontSize(10) }}
            />
          </View>
        </View>
        <View
          style={{
            width: responsiveScreenWidth(90),
            height: responsiveScreenHeight(25),
          }}
        >
          <Text
            style={{
              fontSize: responsiveScreenFontSize(3),
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            ¡MUY BIEN!
          </Text>
          <Text
            style={{
              fontSize: responsiveScreenFontSize(2.5),
              textAlign: "center",
              marginTop: 10,
            }}
          >
            Revisa tu correo electrónico para activar tu cuenta.
          </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("Login")}
            style={{
              backgroundColor: "#64C2C8",
              width: responsiveScreenWidth(50),
              height: responsiveScreenHeight(5),
              borderRadius: 10,
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginVertical: 50,
            }}
          >
            <Text style={{ color: "white" }}>VOLVER</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}
