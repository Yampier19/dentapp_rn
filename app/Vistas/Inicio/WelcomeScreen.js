import React, { useState, useEffect } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
 
} from "react-native";
import { Icon} from 'native-base';
import {
  responsiveScreenFontSize,
  responsiveScreenHeight,
  responsiveScreenWidth,
} from "react-native-responsive-dimensions";




export default function WelcomeScreen(props) {

    

useEffect(() => {
    props.navigation.addListener("beforeRemove", (e) => {
      // Prevent default behavior of leaving the screen
      e.preventDefault();
    });
  }, []);

  
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.box_firts}>
          <Text style={styles.box_firts_title}>
            ¡Hola! Bienvenid@ a DentApp!
          </Text>
        </View>
        <View style={styles.box_second}>
          <View
            style={{
              width: responsiveScreenWidth(90),
              alignItems: "center",
              marginBottom: 10,
            }}
          >
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Register")}
              style={{
                width: responsiveScreenWidth(90),
                backgroundColor: "#65C1C7",
                borderRadius: 10,
                padding: 10,
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: responsiveScreenFontSize(2),
                }}
              >
                Crear una cuenta
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{ width: responsiveScreenWidth(90), alignItems: "center" }}
          >
            <TouchableOpacity onPress={() => props.navigation.navigate('Login')}
              style={{
                width: responsiveScreenWidth(90),
                backgroundColor: "#E7334C",
                borderRadius: 10,
                padding: 10,
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: responsiveScreenFontSize(2),
                  fontWeight: "700",
                }}
              >
                Ingresar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.box_third}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Help");
            }}
            style={{
              flexDirection: "row",
              backgroundColor: "#E7344C",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 10,
              width: responsiveScreenWidth(50),
              height: responsiveScreenHeight(5),
            }}
          >
            <Icon
              type="FontAwesome5"
              name="info-circle"
              style={{
                marginRight: 5,
                color: "white",
                fontSize: responsiveScreenFontSize(2),
              }}
            />
            <Text
              style={{
                color: "white",
                fontSize: responsiveScreenFontSize(2),
              }}
            >
              Ayuda
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.box_four}>
            <Text
              style={{ color: "gray", fontSize: responsiveScreenFontSize(1.8) }}
            >
              Versión: 2.0
            </Text>
          </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
  },

  box_firts: {
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(30),
    justifyContent: "center",
  },

  box_firts_title: {
    fontSize: responsiveScreenFontSize(3),
    paddingHorizontal: responsiveScreenWidth(10),
    textAlign: "center",
    fontWeight: "700",
  },

  box_second: {
    width: responsiveScreenWidth(90),
    justifyContent: "center",
    marginBottom: 50
  },

  box_third: {
    width: responsiveScreenWidth(90),
    justifyContent: "center",
    alignItems: 'center',
    marginBottom: 25
  },

  box_four: {
    width: responsiveScreenWidth(90),
    justifyContent: "center",
    alignItems: 'center'
  },
});
