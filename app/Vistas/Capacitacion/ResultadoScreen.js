import React, { useEffect, useState } from 'react'
import { Text, View, Image, ScrollView, StyleSheet } from 'react-native'
import { Button, Spinner } from 'native-base';

import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import { resolveImage } from '../../utils/utils';
import s from '@Public/Css/style'
import { useProfile } from "../../services/dentapp/user/useUser";

export default function ResultadoScreen({route, navigation}) {
    const { response, preguntas, trivia } = route.params || {};
    const [profile, setProfile] = useState(null);
    const [loading, setLoading] = useState(false);
    const { data: Profile } = useProfile();

    useEffect(() => {
        getProfile();
    }, [])

    const getProfile = async () => {
        try {
            setLoading(true);
            const data = Profile;
            if(data?.status === 'success' && data?.datos){
                const currentData = await resolveImage([data?.datos], 'cliente/avatar', 'foto', 'uploads/', true, 'POST');
                setProfile({
                    ...currentData?.[0],
                    password: data?.password,
                    message: data?.message
                });
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    };

    return (
        <View style={s.container}>


            <ScrollView >
                {
                    loading ? (
                        <Spinner color="red" style={styles.spinner} />
                    ) : (
                    <View style={[styles.content]}>
                    <View style={[styles.box1]}>
                        <Text style={{ marginHorizontal: 20, fontSize: 20, fontWeight: 'bold' }}>¡Felicidades {profile?.nombre}!</Text>

                        <Image source={require('@Public/Img/resultado.png')} style={{ width: 230, height: 150, alignSelf: 'center', marginVertical: 20 }} />

                    </View>
                    <View style={[styles.box2]}>
                        <View style={{ marginHorizontal: 25 }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'gray' }}>
                                Has aprobado la capacitación en {trivia?.nombre}.
                            </Text>
                            <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'gray', marginTop: 20 }}>
                                Por realizar esta actividad has ganado {response?.puntos || 0} puntos
                            </Text>
                        </View>
                    </View>
                    <View style={[styles.box3]}>
                        <Button onPress={() => {
                            navigation.navigate('Capacitacion')
                        }} block style={{ width: 330, alignSelf: 'center', backgroundColor: '#64C2C8' }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#ffffff' }}>
                                Ver mas capacitaciones
                            </Text>
                        </Button>
                        <Button onPress={() => {
                            navigation.navigate('Nivel', {profile})
                        }} block style={{ width: 250, marginVertical: 10, alignSelf: 'center', backgroundColor: '#64C2C8' }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#ffffff' }}>
                                Ver mi proceso
                            </Text>
                        </Button>
                    </View>            
                </View>
                )}
            </ScrollView>

        </View>

    )
}

const styles = StyleSheet.create({

    video: {
        alignSelf: 'center',
        width: 320,
        height: 200,
    },

    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '100%',
        height: 250,
        marginTop: 50
    },
    box2: {
        width: '100%',
        height: 150,
        marginBottom: 10,
        marginTop: 10,
    },
    box3: {
        width: '100%',
        height: 150,
        marginTop: 20
    },

    box5: {
        width: '100%',
        height: 100
    }
});
