import React, { useState, useEffect, useCallback } from 'react'
import { Text, View, ScrollView, StyleSheet, TouchableOpacity, RefreshControl } from 'react-native'
import { Spinner} from 'native-base';
import { Request } from '../../api/api';
import { resolveImage } from '../../utils/utils';
import ViewFile from '../../Components/ViewFile';
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import { Searchbar } from 'react-native-paper';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import debounce from 'lodash.debounce';
import {
    responsiveScreenHeight,
    responsiveScreenWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions";
  import capacitacion from '@Public/Css/Capacitacion/Capacitacion'


export default function CapacitacionScreen(props) {
    const [searchQuery, setSearchQuery] = React.useState('');
    const onChangeSearch = query => setSearchQuery(query);
    const [ trainings, setTrainings ] = useState([]);
    const [ searchResult, setSearchResult ] = useState([]);
    const [ loading, setLoading ] = useState(false);
    const [ search, setSearch ] = useState('');
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = useCallback(() => {
        setRefreshing(true);
        getCapacitaciones();
    }, []);

    const debouncedSave = useCallback(
		debounce(nextValue => searchCapacitaciones(nextValue), 1000),
		[], // will be created only once initially
	);

    const getCapacitaciones = async () => {
        try {
            setLoading(true);
            const response = await Request('GET', 'training/list/20');
            const data = await response.json();
            if(data?.status === 'success' && data?.trainings?.length > 0){
                const currentData = await resolveImage(data?.trainings, 'training/getImage', 'imagen', 'training/', true);
                setTrainings(currentData);
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
        setRefreshing(false)
    };

    const searchCapacitaciones = async (str) => {
        try {
            setLoading(true);
            const response = await Request('GET', `training/buscador/${str}/20`);
            const data = await response.json();
            if(data?.status === 'success' && data?.trainings?.length > 0){
                const currentData = await resolveImage(data?.trainings, 'training/getImage', 'imagen', 'training/', true);
                setSearchResult(currentData);
            }
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    };

    const onSearch = (text) => {
        if(!text){
            setSearch('');
        } else {
            // searchCapacitaciones(text);
            debouncedSave(text);
            setSearch(text)
        }
    };

    useEffect(() => {
        getCapacitaciones();
    }, [refreshing])

    return (
        <ScrollView style={{backgroundColor: 'white'}} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}> 
            <View style={capacitacion.content}>
                <View style={capacitacion.box_title}>
                    <Text style={{fontSize: responsiveScreenFontSize(2)}}>
                    Busca y elige el tema sobre el cuál quieres aprender y gana puntos extra por realizar el entrenamiento:
                    </Text>
                </View>
                <View style={capacitacion.box_search}>
                    <Searchbar
                            placeholder="Buscar capacitación"
                            onChangeText={(text) => onSearch(text)} value={search}
                            value={search}
                            style={{width: responsiveScreenWidth(90), height: responsiveScreenHeight(6)}}
                    />
                </View>
                <View style={capacitacion.box_title_sessions}>
                    <Text style={{fontSize: responsiveScreenFontSize(2), fontWeight: 'bold'}}>
                        SESIONES GRABADAS
                    </Text>
                </View>
                <View style={{width: wp('90%'), alignSelf: 'center'}}>
                    {
                        loading && !refreshing ? <Spinner color="red" style={styles.spinner} /> : (
                                <View style={{ flexDirection: 'row', width: wp('100%'),flexWrap:'wrap',flex:1, paddingBottom: 50}}>
                                           
                                            {
                                                search !== '' && searchResult.length > 0 && searchResult.map(cap =>
                                                    <TouchableOpacity style={{marginTop: 20, marginRight: 5 }} key={cap?.id_capacitacion} onPress={() => {
                                                        props.navigation.navigate('SelectCapacitacion', {uid: cap?.id_capacitacion})
                                                    }}>
                                                        <ViewFile style={{width: wp('45%'), height: hp('30%')}} filetype={cap?.filetype} file={cap?.file} token={cap?.token || null} />
                                                        <View style={{width: wp('45%'), padding: 10, backgroundColor: 'rgba(52, 52, 52, 0.3)', position: 'absolute', left: 0, right: 0, bottom: 0}}>
                                                            <Text style={{color: 'white', fontSize: 18, fontWeight: 'bold'}}>{cap?.nombre}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    
                                                )
                                                
                                            }
                                            
                                            {
                                                search === '' && trainings.length > 0 && trainings.map(cap =>
                                                    <TouchableOpacity style={{marginTop: 20, marginRight: 5 }} key={cap?.id_capacitacion} onPress={() => {
                                                        props.navigation.navigate('SelectCapacitacion', {uid: cap?.id_capacitacion})
                                                    }}>
                                                        <ViewFile style={{width: wp('45%'), height: hp('30%')}} filetype={cap?.filetype} file={cap?.file} token={cap?.token || null} />
                                                        <View style={{width: wp('45%'), padding: 10, backgroundColor: 'rgba(52, 52, 52, 0.3)', position: 'absolute', left: 0, right: 0, bottom: 0}}>
                                                            <Text style={{color: 'white', fontSize: 18, fontWeight: 'bold'}}>{cap?.nombre}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                )
                                            }

                                            {
                                        trainings?.length === 0 && <View>
                                            <Text>No hay capacitaciones disponibles</Text>
                                        </View>
                                    }
                                    {
                                        search !== '' && searchResult?.length === 0 && <View>
                                            <Text>No hay resultados para: {search}</Text>
                                        </View>
                                    }                       
                                </View>
                        )
                    }
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
})