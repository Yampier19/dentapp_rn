import React, { useEffect, useState } from 'react'
import { Text, View, ScrollView, StyleSheet } from 'react-native'
import { Icon, Button, Spinner } from 'native-base';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { Request } from '../../api/api';
import ModalComponent from '../../Components/Modal';
import s from '@Public/Css/style'
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'

export default function InicioCapacitacionScreen({route, navigation}) {
    const { trivia } = route.params || {};
    const [encuesta, setEncuesta] = useState(null);
    const [loading, setLoading] = useState(false);
    const [dialog, setDialog] = useState({visible: false, message: '', type: ''});

    useEffect(() => {
        if(trivia?.id_trivia){
            getTriviaById();
        }
    }, [trivia])

    const getTriviaById = async () => {
        try {
            setLoading(true);
            const response = await Request('GET', `encuesta/preguntas/${trivia?.id_trivia}`);
            const data = await response.json();
            if(data?.status === 'success'){
                setEncuesta(data?.preguntas);
            } else {
                setDialog({
                    visible: true,
                    message: data?.message || 'Ocurrió un error al consultar los datos, vuelva a intentarlo',
                    type: 'info'
                });
            }
            setLoading(false);
        } catch (error) {
          setLoading(false);
        }
    };

    return (
        <View style={s.container}>
            <ScrollView >
                <View style={[styles.content]}>
                    <View style={[styles.box1]}>
                        <View style={{ backgroundColor: '#64C2C8', borderRadius: 80, width: 150, height: 150, marginTop: 20, justifyContent: 'center', alignSelf: 'center', alignItems: 'center' }}>
                            <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-school' : 'md-school'}
                                            size="20"
                                            color="white"
                                            />
                        </View>
                    </View>
                    <View style={[styles.box2]}>
                        <View style={{ marginHorizontal: 25 }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center', color: '#000000' }}>
                                Pon a prueba tu aprendizaje en la capacitación de {trivia?.nombre}
                            </Text>
                        </View>

                        {loading ? (
                            <Spinner color="red" />
                        ) : (
                            encuesta && (
                                <>
                                    <View style={{ width: 300, alignSelf: 'center', alignItems: 'center', marginTop: 50, flexDirection: 'row' }}>
                                        <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-book' : 'md-book'}
                                            size="15"
                                            color="black"
                                            />
                                        <Text style={{ marginTop: 3, marginLeft: 10, color: 'gray', fontSize: 15, fontWeight: 'bold' }}>{`Este examen tiene ${encuesta.length > 1 ? encuesta.length + ' preguntas' : encuesta.length + ' pregunta'}`}</Text>
                                    </View>
                                    <View style={{ width: 300, alignSelf: 'center', marginTop: 30, flexDirection: 'row' }}>
                                    <Icon
                                            as={Ionicons}
                                            name={Platform.OS ? 'ios-book' : 'md-book'}
                                            size="15"
                                            color="black"
                                            />
                                        <Text style={{ marginHorizontal: 10, color: 'gray', fontSize: 15, fontWeight: 'bold' }}>
                                            No te preocupes, puedes intentar pasar el examen tantas veces como quieras
                                        </Text>
                                    </View>
                                </>
                            )
                        )}

                    </View>

                    {
                        encuesta !== null && (
                            <View style={[styles.box3]}>
                                <Button  accessible={true}
                                    accessibilityLabel="Iniciar Capacitacion"
                                    accessibilityHint="Comenzar la capacitacion"
                                    onPress={() => {
                                        navigation.navigate('Evaluacion',{trivia, preguntas: encuesta})
                                    }} block style={{ width: 250, height: 50, alignSelf: 'center', backgroundColor: '#64C2C8' }}>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#ffffff' }}>
                                        Iniciar
                                    </Text>
                                </Button>
                            </View>
                        )
                    }
                </View>
            </ScrollView>
            <ModalComponent onClose={setDialog} {...dialog} />
         
        </View>

    )
}

const styles = StyleSheet.create({

    video: {
        alignSelf: 'center',
        width: 320,
        height: 200,
    },


    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '100%',
        height: 180,
        marginTop: 50
    },
    box2: {
        width: '100%',
        height: 250,
        marginBottom: 10,
        marginTop: 40
    },
    box3: {
        width: '100%',
        height: 80,
        marginTop: 20
    },
    box4: {
        width: '100%',
        height: 450,
        marginTop: 30
    },
    box5: {
        width: '100%',
        height: 100
    }
});
