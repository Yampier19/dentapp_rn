import React, { useEffect, useState } from 'react'
import { Text, View, Image, ScrollView, StyleSheet } from 'react-native'
import { Button, Spinner } from 'native-base';
import ViewFile from '../../Components/ViewFile';
import { Request } from '../../api/api';
import { resolveImage } from '../../utils/utils';
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { useIsFocused  } from '@react-navigation/native';

export default function SelectCapacitacionScreen({ route, navigation}) {
    const {categoria, uid} = route.params || {};
    const [loading, setLoading] = useState(false);
    const [training, setTraining] = useState(null);
    const stopVideo = useIsFocused();

    useEffect(() => {
        if(categoria){
            setLoading(true);
            getTraining();
        }
    }, [categoria])

    useEffect(() => {
        if(uid){
            setLoading(true);
            getTrainingById(uid);
        }
    }, [uid])

    const getTraining = async () => {
        try {
            const response = await Request('GET', `training/categoria/${categoria}`);
            const data = await response.json();
            if(data?.status === 'success' && data?.trainings?.length > 0){
                await getTrainingById(data?.trainings?.[0]?.id_capacitacion);
            } else {
                setLoading(false);
            }
        } catch (error) {
          setLoading(false);
        }
    };

    const getTrainingById = async (uid) => {
        try {
            const response = await Request('GET', `training/${uid}`);
            const data = await response.json();
            if(data?.status === 'success' && data?.training ){
                const currentData = await resolveImage([data?.training], 'training/getArchive', 'archivo', 'training/', true);
                setTraining(currentData?.[0]);
              }
            setLoading(false);
        } catch (error) {
          setLoading(false);
        }
    };

    return (
        <View style={{flex: 1}}>
            {loading ? (
                <Spinner color="red" style={styles.spinner} />
            ) : (
                training ? (
                    <ScrollView>
                        <View style={{width: wp('90%'), marginTop: 30, alignSelf: 'center'}}>
                            <Text style={{fontSize: 18, fontWeight: 'bold'}}>{training?.nombre}</Text>
                        </View>
                        <View style={{width: wp('90%'), alignSelf: 'center', marginTop: 40}}>
                            <Text style={{fontSize: 16}}>{training?.informacion}</Text>
                        </View>
                        <View style={{width: wp('100%'), height: wp('75%'), marginVertical: 20, alignSelf: 'center'}}>
                            <ViewFile resizeMode="contain" style={{width: '100%', height: '100%'}} filetype={training?.filetype} file={training?.file} token={training?.token} stop={stopVideo} />
                        </View>
                        <View style={{width: wp('90%'), marginVertical: 80, alignSelf: 'center', marginTop: 8}}>
                            {training?.trivia && training?.trivia.estado !== 0 && (
                                <Button onPress={() => {
                                    navigation.navigate('InicioCapacitacion', {trivia: training?.trivia})
                                }} block style={{ width: 250, height: 50, marginVertical: 10, alignSelf: 'center', backgroundColor: '#64C2C8' }}>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#ffffff' }}>
                                        Realizar encuesta
                                    </Text>
                                </Button>
                            )}
                        </View>
                
                    </ScrollView>
                ) : (
                    loading ? (
                        <Spinner color="red" style={styles.spinner} />
                    ) : (
                        <View style={styles.notfound}>
                            <Text style={styles.textnotfound}>No hay capacitaciones disponibles para la categoría {categoria}.</Text>
                        </View>
                    )
                )
            )}
            
        </View>
    )
}

const styles = StyleSheet.create({
    spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    notfound:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    textnotfound: {
        fontSize: 22,
        color: '#443F3C',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    content: {
        alignItems: 'center',
        marginBottom: 40
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    box1: {
        width: '100%',
        height: 170,
        marginTop: 30
    },
    box2: {
        width: '100%',
        height: 250,
        marginBottom: 10,
        marginTop: 40,
    },
    box3: {
        width: '100%',
        height: 80,
        marginTop: 40
    },
    box4: {
        width: '100%',
        height: 450,
        marginTop: 30
    },
    box5: {
        width: '100%',
        height: 100
    }
});
