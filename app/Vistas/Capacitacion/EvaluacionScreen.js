import React, { useState } from "react";
import { View, ScrollView, StyleSheet, TouchableOpacity } from "react-native";
import { Text, Spinner } from "native-base";
import TabNavigatorScreen from '@Vistas/Navigation/TabNavigatorScreen'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Divider, Checkbox, RadioButton } from 'react-native-paper';
import { Request } from '../../api/api';
import ModalComponent from '../../Components/Modal';

export default function EvaluacionScreen({route, navigation}) {
  const { preguntas, trivia } = route.params || {};
  const [loading, setLoading] = useState(false);
  const [selectList, setSelectList] = useState([]);
  const [selectListRadio, setSelectListRadio] = useState([]);
  const [dialog, setDialog] = useState({visible: false, message: '', type: ''});

  const onPressSend = () => {
    const respuestas = [];
    
    preguntas.forEach(pregunta => {
      pregunta?.respuestas.forEach(respuesta => {
        if(pregunta.tipo === 'radio'){
          if(isExist(pregunta,respuesta?.informacion, 'radio')){
            respuestas.push({
              pregunta: pregunta?.informacion,
              respuesta: respuesta?.informacion,
              correcta: respuesta?.correcta === 1 ? true : false
            })
          }
        } else {
          if(isExist(pregunta,respuesta?.informacion, 'checkbox')){
            respuestas.push({
              pregunta: pregunta?.informacion,
              respuesta: respuesta?.informacion,
              correcta: respuesta?.correcta === 1 ? true : false
            })
          }
        }
      });
    });

    const payload = {
      trivia: respuestas,
      total_correctas: countCorrectas(respuestas)
    };
    sendRespuesta(payload)
  }

  const sendRespuesta = async (payload) => {
    try {
        setLoading(true);
        const response = await Request('POST', `encuesta/respuestas/${trivia?.id_trivia}`, payload);
        const data = await response.json();
        if(data?.status === 'success'){
          navigation.navigate('Resultado', {response: data, preguntas, trivia})
        } else {
            setDialog({
                visible: true,
                message: data?.message || 'Ocurrió un error al procesar los datos, vuelva a intentarlo',
                type: 'error'
            });
        }
        setLoading(false);
    } catch (error) {
      setLoading(false);
    }
};

  const countCorrectas = (res) => {
    const correctas = res.filter(item => item?.correcta === true);
    return correctas?.length || 0;
  };

  const isExist = (pregunta, respuesta, type) => {
    if(type === 'radio'){
      const filter = selectListRadio.filter(item => pregunta?.informacion === item.pregunta && item.respuesta === respuesta)
      return filter.length > 0 ? true : false;
    } else {
      const filter = selectList.filter(item => item.respuesta === respuesta)
      return filter.length > 0 ? true : false;
    }
  };

  const resolveRadio = (pregunta, respuesta) => {
    const currentRespuestas = selectListRadio;
    const find = currentRespuestas.find(item => item.pregunta === pregunta);
    if(find){
      if(find.pregunta === pregunta && find.respuesta === respuesta){
        const currentList = currentRespuestas.filter(item => item?.pregunta !== pregunta);
        setSelectListRadio(currentList)
      } else {
        find.respuesta = respuesta;
        const index = currentRespuestas.findIndex(item => item.pregunta === pregunta)
        if(index !== -1){
          currentRespuestas[index] = find;
          setSelectListRadio([...currentRespuestas]);
        }
      }
    } else {
      setSelectListRadio([...selectListRadio, {
        pregunta: pregunta,
        respuesta: respuesta
      }])

    }
  }

  const resolveCheckbox = (pregunta, respuesta) => {
    const find = selectList.find(item => item.respuesta === respuesta)
    if(find){
      const currentList = selectList.filter(item => item?.respuesta !== respuesta)
      setSelectList(currentList)
    } else {
      const currentRespuestas = selectList;
      currentRespuestas.push({
        pregunta: pregunta,
        respuesta: respuesta
      })
      setSelectList(currentRespuestas)
    }
  }
  
  return (
    <View style={{flex: 1, backgroundColor: '#fff', height: hp('100%')}}>
          <View style={{width: wp('90%'), height: hp('10%'), alignSelf: 'center', marginTop: 30}}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Capacitación:</Text>
            <Text>{trivia?.nombre}</Text>
          </View>
          
      <ScrollView>
        {loading ? <Spinner color="red" style={styles.spinner} /> : (
          <View style={{marginBottom: '20%'}}>
           {preguntas.length > 0 && preguntas.map((pregunta) => (
            <View key={`${pregunta?.id_pregunta}-${pregunta?.created_at}`} style={{ borderWidth: 1, marginTop: 10, borderColor: '#E1E1E1', width: wp('90%'), padding: 10, alignSelf: 'center', borderRadius: 15}}>
              <Text>{pregunta?.informacion}</Text>
              <Divider style={{width: wp('85%'), height: hp('0.3%'), backgroundColor: '#64C2C8', marginVertical: 10}}/>

              {pregunta?.respuestas.length > 0 && pregunta?.respuestas.map((respuesta) => (
              <View key={`${respuesta?.id_respuesta}-${respuesta?.created_at}`} style={{flexDirection: 'row', width: wp('85%'), alignItems: 'center'}}>
             
                {
                  pregunta?.tipo === 'radio' ? (
                    <RadioButton 
                      status={isExist(pregunta, respuesta?.informacion, 'radio') ? 'checked' : 'unchecked'}
                      value={`${respuesta?.id_respuesta}-${respuesta?.informacion}`}
                      uncheckedColor="#64C2C8"
                      color="#64C2C8"
                      onPress={()=> resolveRadio(pregunta?.informacion, respuesta?.informacion)}
                    />
                  ) : (
                    <Checkbox 
                      status={isExist(pregunta,respuesta?.informacion, 'checkbox') ? 'checked' : 'unchecked'}
                      uncheckedColor="#64C2C8"
                      color="#64C2C8"
                      onPress={() => resolveCheckbox(pregunta?.informacion, respuesta?.informacion)}
                    />
                  )
                }

    <View style={{width: wp('70%')}}>
            <Text>{respuesta?.informacion}</Text>
      </View>        
              </View>
              ))}
          </View>
           ))}
           <TouchableOpacity onPress={onPressSend} style={{backgroundColor: '#64C2C8', width: wp('90%'), height: hp('5%'), borderRadius: 10, alignSelf: 'center', marginTop: 20, justifyContent: 'center'}}>
             <Text style={{fontSize: 18, textAlign: 'center', color: 'white'}}>ENVIAR EVALUACION</Text>
             </TouchableOpacity>
           </View>
        )}
      </ScrollView>
      <ModalComponent onClose={setDialog} {...dialog} />
    </View>
  );
}

const styles = StyleSheet.create({
  video: {
    alignSelf: "center",
    width: 320,
    height: 200,
  },
  spinner: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
  checkboxContainer: {
    flexDirection: "row",
    // marginBottom: 20,
    // marginVertical: 10,
    // marginLeft: 10,
    width: "100%",
  },
  preList: {
    width: "100%",
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
    color: "gray",
  },

  container: {
    flex: 1,
    backgroundColor: "red",
  },

  content: {
    alignItems: "center",
    marginBottom: 40,
  },
  footer: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
  box1: {
    width: "100%",
    height: 50,
    marginTop: 30,
  },
  box2: {
    width: "90%",
    height: 280,
    marginBottom: 10,
    marginTop: 40,
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    elevation: 3,
    backgroundColor: "white",
  },
  box3: {
    width: "90%",
    height: 280,
    marginBottom: 10,
    marginTop: 20,
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    elevation: 3,
    backgroundColor: "white",
  },
  box4: {
    width: "90%",
    height: 280,
    marginBottom: 10,
    marginTop: 20,
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
    elevation: 3,
    backgroundColor: "white",
  },
  box5: {
    width: "100%",
    height: 100,
    marginTop: 20,
  },
  box6: {
    width: "100%",
    height: 100,
    marginTop: 30,
  },
});
