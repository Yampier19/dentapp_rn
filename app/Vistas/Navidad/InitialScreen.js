import React from "react";
import { useEffect } from "react";
import { View, Image } from "react-native";
import { useList } from "../../services/dentapp/navident/useNaviDent";
export default function InitialScreen(props) {
  const { data: List, isLoading } = useList();
  useEffect(() => {
    if (!isLoading) {
      props.navigation.navigate("Navidad", { premio: List });
    }
  }, [List]);
  return (
    <View style={{ flex: 1 }}>
      <Image
        source={require("@Public/Img/navidad.png")}
        style={{ width: "100%", height: "100%" }}
      />
    </View>
  );
}
