/**
 * MICHAEL ESTE ES EL LINK DE LA DOCUMENTACION: https://github.com/DKbyo/react-native-casino-roulette
 * @flow
 */

import React from "react";
import { useState } from "react";
import { useRef } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { Icon } from "native-base";
import Roulette from "react-native-casino-roulette";
import wheel from "@Public/Img/Navidad/wheel.png";
import marker from "@Public/Img/Navidad/marker.png";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";

//Roulette numbers
const numbers = [0, 3, 4, 6, 7, 5, 1, 2];
const options = numbers.map((o) => ({ index: o }));
const customOptions = numbers.map((o) => <Text index={o}>{o}</Text>);

const Ruleta = ({ setStateRuleta, disabled, ruletaPremios }) => {
  const [state, setState] = useState({
    option: "Option selected:",
    optionCustom: "Option selected:",
    rouletteState: "stop",
    rouletteCustomState: "stop",
  });
  const onRotateChange = (event) => {
    setState({
      ...state,
      rouletteState: event,
    });
    setStateRuleta(event);
  };

  const onRotate = (option) => {
    setState({
      ...state,
      option: option.index,
    });
  };

  const onRotateCustomChange = (event) => {
    setState({
      ...state,
      rouletteCustomState: event,
    });
  };

  const onRotateCustom = (option) => {
    setState({
      ...state,
      optionCustom: option.props.index,
    });
  };

  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <View style={{ alignItems: "center" }}>
        <Roulette
          ref={ruletaPremios}
          enableUserRotate={disabled}
          background={wheel}
          onRotate={onRotate}
          onRotateChange={onRotateChange}
          marker={marker}
          options={options}
          markerWidth={30}
        />
      </View>
    </View>
  );
};
export default Ruleta;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5,
  },
});
