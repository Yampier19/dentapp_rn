import React, { useState, useEffect } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
} from "react-native";
import { Icon } from "native-base";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { useUserProvider } from "../../../providers/user/user-provider";
import {
  useFonts,
  DancingScript_400Regular,
} from "@expo-google-fonts/dancing-script";

const ModalNavidad = (props) => {
  let [fontsLoaded] = useFonts({
    DancingScript_400Regular,
  });
  const [modalVisible, setModalVisible] = useState(false);
  const [typeModal, setTypeModal] = useState("");
  const [premio, setPremio] = useState(null);
  const { navigation, tickets, register, listPending } = props;
  const User = useUserProvider();

  const handleExchangeAward = () => {
    if (premio?.tipo === "bono") {
      navigation.navigate("CanjearQuantum", {
        profile: User?.data?.datos,
        campaign: "Navident",
        premio: premio?.premio,
        id_navidad: premio?.id_navidad_canjeado,
        typeModal: typeModal,
      });
    } else if (premio?.tipo === "Producto") {
      if (premio?.entrega === "Domicilio") {
        navigation.navigate("CanjearDomicilio", {
          premio: premio?.premio,
          profile: User?.data?.datos,
          campaign: "Navident",
          id_navidad: premio?.id_navidad_canjeado,
        });
      } else if (premio?.entrega === "Online") {
        navigation.navigate("CanjearOnline", {
          premio: premio?.premio,
          profile: User?.data?.datos,
          campaign: "Navident",
          id_navidad: premio?.canjeo?.id_navidad_canjeado,
        });
      }
    } else if (premio?.tipo === "Cena") {
      navigation.navigate("CanjearDomicilio", {
        premio: premio?.premio,
        profile: User?.data?.datos,
        campaign: "Navident",
        id_navidad: premio?.id_navidad_canjeado,
        type: "cena",
      });
    }
  };

  const getTitleAndDescription = (data) => {
    switch (data) {
      case "tickets":
        return {
          title: "¡ESPERA!",
          description: "Recuerda que tienes un ticket disponible",
          bottom: "Ir a la ruleta",
          goTo: () => navigation.navigate("Navidad"),
        };
      case "register":
        return {
          title: "¡ESPERA!",
          description:
            "Recuerda que debes subir tu factura para poder ganar con DentApp",
          bottom: "Subir factura",
          goTo: handleExchangeAward,
        };
      case "pending":
        return {
          title: "¡ESPERA!",
          description: "Recuerda que tienes un premio por canjear",
          bottom: "Redimir mi premio",
          goTo: handleExchangeAward,
        };
      default:
        return {
          title: "¡ESPERA!",
          description: "Recuerda que tienes un premio por canjear",
          bottom: "Redimir mi premio",
          goTo: () => setModalVisible(false),
        };
    }
  };

  useEffect(() => {
    if (tickets?.status === "success") {
      if (tickets?.datos?.tickets > 0) {
        setTypeModal("tickets");
        setPremio(tickets?.premios[0]);
        setModalVisible(true);
      } else {
        if (register?.status === "warning") {
          setTypeModal("register");
          setPremio(register?.premios[0]);
          setModalVisible(true);
        } else {
          if (listPending?.status === "warning") {
            setTypeModal("pending");
            setPremio(listPending?.premios[0]);
            setModalVisible(true);
          }
        }
      }
    } else {
      if (register?.status === "warning") {
        setTypeModal("register");
        setPremio(register?.premios[0]);
        setModalVisible(true);
      } else {
        if (listPending?.status === "warning") {
          setTypeModal("pending");
          setPremio(listPending?.premios[0]);
          setModalVisible(true);
        }
      }
    }
  }, [tickets, register, listPending]);

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ImageBackground
              source={require("@Public/Img/Navidad/MODAL_HOME.png")}
              style={{ width: "100%", height: "100%" }}
            >
              <View
                style={{
                  width: responsiveScreenWidth(90),
                  height: responsiveScreenHeight(5),
                  justifyContent: "center",
                  alignItems: "flex-end",
                }}
              >
                <Icon
                  type="FontAwesome"
                  name="times-circle"
                  style={{ marginRight: 10, color: "#E7344C" }}
                  onPress={() => setModalVisible(!modalVisible)}
                />
              </View>

              <View style={styles.box_title}>
                <Text style={styles.title}>
                  {getTitleAndDescription(typeModal).title}
                </Text>
              </View>

              <View style={styles.box_description}>
                <Text style={styles.text_description}>
                  {getTitleAndDescription(typeModal).description}
                </Text>
              </View>

              <View style={styles.box_redimir}>
                <TouchableOpacity
                  onPress={getTitleAndDescription(typeModal).goTo}
                  style={styles.boton_redimir}
                >
                  <Text
                    style={{
                      fontSize: responsiveScreenFontSize(2),
                      color: "white",
                    }}
                  >
                    {getTitleAndDescription(typeModal).bottom}
                  </Text>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.8)",
  },
  modalView: {
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(75),
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  box_title: {
    width: responsiveScreenWidth(80),
    height: responsiveScreenWidth(20),
    alignSelf: "center",
    marginVertical: 20,
  },
  title: {
    fontFamily: "DancingScript_400Regular",
    fontSize: responsiveScreenFontSize(6),
    color: "white",
    textAlign: "center",
  },
  box_description: {
    width: responsiveScreenWidth(80),
    height: responsiveScreenWidth(20),
    alignSelf: "center",
  },
  text_description: {
    color: "white",
    fontSize: responsiveScreenFontSize(2.5),
    textAlign: "center",
  },
  box_redimir: {
    width: responsiveScreenWidth(55),
    height: responsiveScreenHeight(8),
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginVertical: 20,
  },
  boton_redimir: {
    textAlign: "center",
    backgroundColor: "#E7344C",
    paddingHorizontal: 40,
    paddingVertical: 10,
    borderRadius: 10,
  },
});
export default ModalNavidad;
