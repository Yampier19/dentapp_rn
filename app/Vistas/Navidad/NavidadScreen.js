import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Modal,
  Animated,
} from "react-native";
import {
  responsiveScreenHeight,
  responsiveScreenWidth,
  responsiveScreenFontSize,
} from "react-native-responsive-dimensions";
import { Icon, Spinner } from "native-base";
import {
  useFonts,
  DancingScript_400Regular,
} from "@expo-google-fonts/dancing-script";
import { When } from "react-if";
import { Unless } from "react-if";

import Progress from "./Progress";
import { useProfile } from "../../services/dentapp/user/useUser";
import { useList } from "../../services/dentapp/navident/useNaviDent";
import { useRuleta } from "../../services/dentapp/navident/useNaviDent";
import { resolveImage } from "../../utils/utils";

export default function NavidadScreen(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const { navigation, route } = props;
  const { premio } = route.params || {};
  const { data: Profile } = useProfile();
  const { data: ListRuletaInfo, isLoading } = useList();
  const { data: Ruleta, refetch } = useRuleta({
    refetchOnWindowFocus: false,
    enabled: false,
    manual: true,
  });
  const [stateRuleta, setStateRuleta] = useState(null);
  const [countAnimated, setCountAnimated] = useState(true);
  let [fontsLoaded] = useFonts({
    DancingScript_400Regular,
  });
  const RuletaPremio = useRef(null);
  const Button = useRef(null);
  const handleClickRuleta = () => {
    Animated.timing(RuletaPremio.current.state._animatedValue, {
      toValue: Number(countAnimated * 15),
      duration:  1500,
    }).start(() => {
      setCountAnimated(!countAnimated);
      setStateRuleta("stop");
    });
    refetch();
  };

  const handleExchangeAward = () => {
    if (Ruleta?.canjeo?.tipo === "bono") {
      navigation.navigate("CanjearQuantum", {
        profile: Profile.datos,
        campaign: "Navident",
        premio: Ruleta?.premio,
        id_navidad: Ruleta?.canjeo?.id_navidad_canjeado,
      });
    } else if (Ruleta?.canjeo?.tipo === "Producto") {
      if (Ruleta?.premio?.entrega === "Domicilio") {
        navigation.navigate("CanjearDomicilio", {
          premio: Ruleta?.premio,
          profile: Profile.datos,
          campaign: "Navident",
          id_navidad: Ruleta?.canjeo?.id_navidad_canjeado,
        });
      } else if (Ruleta?.premio?.entrega === "Online") {
        navigation.navigate("CanjearOnline", {
          premio: Ruleta?.premio,
          profile: Profile.datos,
          campaign: "Navident",
          id_navidad: Ruleta?.canjeo?.id_navidad_canjeado,
        });
      }
    } else if (Ruleta?.canjeo?.tipo === "Cena") {
      navigation.navigate("CanjearDomicilio", {
        premio: Ruleta?.premio,
        profile: Profile.datos,
        campaign: "Navident",
        id_navidad: Ruleta?.canjeo?.id_navidad_canjeado,
        type: "cena",
      });
    }
  };

  useEffect(() => {
    const getImage = async () => {
      const currentData = await resolveImage(
        [Ruleta?.premio],
        "premio/getImage",
        "foto",
        "premios/",
        true
      );
    };
    if (Ruleta?.premio) {
      getImage();
    }
  }, [Ruleta, isLoading]);

  useEffect(() => {
    if (stateRuleta === "stop") {
      setTimeout(() => {
        if (Ruleta.status === "success") {
          setModalVisible(true);
        }
        setStateRuleta(null);
      }, 800);
    }
  }, [stateRuleta, isLoading]);

  return (
    <View style={{ flex: 1, alignItems: "center" }}>
      <When condition={!isLoading}>
        <ImageBackground
          source={require("@Public/Img/background_navidad.png")}
          resizeMode="cover"
          style={styles.image}
        >
          <ScrollView>
            <View
              style={{
                width: responsiveScreenWidth(95),
                height: 120,
                alignSelf: "center",
              }}
            >
              <Image
                style={{
                  flex: 1,
                  width: null,
                  height: null,
                  resizeMode: "stretch",
                  borderRadius: 10,
                }}
                source={require("@Public/Img/Navidad/CUADRO-2.png")}
              />
            </View>

            <View
              style={{
                width: responsiveScreenWidth(95),
                height: 80,
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  width: responsiveScreenWidth(60),
                  height: responsiveScreenHeight(5),
                  backgroundColor: "#64C2C8",
                  borderRadius: 10,
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onPress={() =>
                  navigation.navigate("Facturas", { campaign: "Navident" })
                }
              >
                <Icon
                  type="Ionicons"
                  name="receipt-outline"
                  style={{
                    marginRight: 10,
                    color: "white",
                    fontSize: responsiveScreenFontSize(3),
                  }}
                />
                <Text
                  style={{
                    fontSize: responsiveScreenFontSize(2),
                    color: "white",
                  }}
                >
                  SUBIR FACTURA
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: responsiveScreenWidth(95),
                height: 300,
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  width: responsiveScreenWidth(76),
                  height: responsiveScreenHeight(35),
                  backgroundColor: "yellow",
                  borderRadius: 160,
                }}
              >
                <ImageBackground
                  style={{
                    flex: 1,
                    width: null,
                    height: null,
                    resizeMode: "stretch",
                  }}
                  source={require("@Public/Img/Navidad/marker3.png")}
                >
                  <Progress
                    setStateRuleta={setStateRuleta}
                    disabled={false}
                    ruletaPremios={RuletaPremio}
                  />
                </ImageBackground>
              </View>
            </View>

            <View
              style={{
                width: responsiveScreenWidth(95),
                height: 100,
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <TouchableOpacity
                ref={Button}
                onPress={handleClickRuleta}
                style={{
                  flexDirection: "row",
                  width: responsiveScreenWidth(60),
                  height: responsiveScreenHeight(5),
                  backgroundColor:
                    ListRuletaInfo?.status === "success" &&
                    ListRuletaInfo?.datos?.tickets > 0
                      ? "#E7344C"
                      : "#e1e1e1",
                  borderRadius: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  marginVertical: 10,
                }}
                disabled={!(ListRuletaInfo?.status === "success")}
              >
                <Text
                  style={{
                    fontSize: responsiveScreenFontSize(2),
                    color: "white",
                  }}
                >
                  GIRAR RULETA
                </Text>
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: responsiveScreenFontSize(2),
                  textAlign: "center",
                }}
              >
                {!isLoading && ListRuletaInfo?.status === "success"
                  ? `Tienes ${ListRuletaInfo?.datos?.tickets} giros disponibles`
                  : `${premio?.message}`}
              </Text>
            </View>
          </ScrollView>
        </ImageBackground>
      </When>
      <Unless condition={!isLoading}>
        <Spinner color="red" style={styles.spinner} />
      </Unless>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ImageBackground
              source={require("@Public/Img/Navidad/fondo-gana.png")}
              style={styles.image}
            >
              <View
                style={{
                  width: responsiveScreenWidth(90),
                  height: responsiveScreenHeight(5),
                  justifyContent: "center",
                  alignItems: "flex-end",
                }}
              >
                <Icon
                  type="FontAwesome"
                  name="times-circle"
                  style={{ marginRight: 10, color: "#E7344C" }}
                  onPress={() => setModalVisible(!modalVisible)}
                />
              </View>
              <View
                style={{
                  width: responsiveScreenWidth(90),
                  height: responsiveScreenHeight(10),
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    fontFamily: "DancingScript_400Regular",
                    fontSize: responsiveScreenFontSize(6),
                    color: "white",
                    textAlign: "center",
                  }}
                >
                  ¡Felicidades Ganaste!
                </Text>
              </View>
              <View
                style={{
                  width: responsiveScreenWidth(55),
                  height: responsiveScreenHeight(8),
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center",
                  marginVertical: 20,
                }}
              >
                <Text
                  style={{
                    fontSize: responsiveScreenFontSize(2.3),
                    textAlign: "center",
                    color: "white",
                  }}
                >
                  La suerte esta de tu lado, tu premio es
                </Text>
              </View>
              <View
                style={{
                  width: responsiveScreenWidth(35),
                  height: 180,
                  alignSelf: "center",
                  backgroundColor: "red",
                }}
              >
                <TouchableOpacity
                  style={{
                    flex: 1,
                    width: null,
                    height: null,
                    resizeMode: "stretch",
                    borderRadius: 10,
                  }}
                >
                  <Image
                    style={{
                      flex: 1,
                      width: null,
                      height: null,
                      resizeMode: "stretch",
                      borderRadius: 10,
                    }}
                    source={
                      premio?.status === "success"
                        ? { uri: Ruleta?.premio?.file }
                        : require("@Public/Img/Navidad/BONO-DE-PRUEBA.png")
                    }
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: responsiveScreenWidth(55),
                  height: responsiveScreenHeight(5),
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center",
                  marginVertical: 20,
                }}
              >
                <TouchableOpacity
                  style={{
                    textAlign: "center",
                    backgroundColor: "#E7344C",
                    paddingHorizontal: 50,
                    paddingVertical: 10,
                  }}
                  onPress={handleExchangeAward}
                >
                  <Text
                    style={{
                      fontSize: responsiveScreenFontSize(2),
                      color: "white",
                    }}
                  >
                    Reclamar
                  </Text>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(52, 52, 52, 0.8)",
  },
  modalView: {
    width: responsiveScreenWidth(90),
    height: responsiveScreenHeight(75),
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  spinner: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
