import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView, Linking } from 'react-native';
import { FlatList, Heading, Avatar, HStack, VStack, Box, Spacer } from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome"
import { List } from 'react-native-paper';
import Carousel from './../Portal/Carousel';
import { useNavigation } from '@react-navigation/native';


import s from '@Public/Css/style'
import { responsiveScreenWidth } from 'react-native-responsive-dimensions';

const Example = () => {
    const navigation = useNavigation();

    const data = [{
        id: 1,
        fullName: "¿Qué es DentApp?",
    }, {
        id: 2,
        fullName: "¿Quiénes participan en DentApp?",
    }, {
        id: 3,
        fullName: "¿Cómo gano puntos?",
    }, {
        id: 4,
        fullName: "¿Cómo avanzar de nivel?",
    }, {
        id: 5,
        fullName: "¿Qué puedo hacer con mis puntos?",
    },
    {
        id: 6,
        fullName: "Tengo problemas para ingresar",
    }];

    function whereTo(id) {
        switch (id) {

            case 1:
                navigation.navigate('OneScreen');
                break;

            case 2:
                navigation.navigate('TwoScreen');
                break;

            case 3:
                navigation.navigate('ThreeScreen');
                break;

            case 4:
                navigation.navigate('FourScreen');
                break;

            case 5:
                navigation.navigate('FiveScreen');
                break;

                case 6:
                    navigation.navigate('SixScreen');
                    break;
        }
    }



    return <Box>
        <Heading fontSize="xl" p="4" pb="3">
            Ayuda
        </Heading>
        <FlatList data={data} renderItem={({
            item
        }) => <Box borderBottomWidth="1" _dark={{
            borderColor: "gray.600"
        }} borderColor="coolGray.200" pl="4" pr="5" py="2">

                <TouchableOpacity onPress={() => whereTo(item.id)}>



                    <HStack space={3} justifyContent="space-between">

                        <VStack style={{ marginTop: 5 }}>
                            <Text _dark={{
                                color: "warmGray.50"
                            }} color="coolGray.800" bold>
                                {item.fullName}
                            </Text>

                        </VStack>
                        <Spacer />
                        <Avatar style={{ backgroundColor: 'white' }} size="30px" source={{
                            uri: 'https://cdn-icons-png.flaticon.com/512/758/758645.png'
                        }} />

                    </HStack>
                </TouchableOpacity>

            </Box>
        }
            keyExtractor={item => item.id} />
    </Box >;
}


export default function HelpScreen(props) {

    return (
        <View style={s.container}>
            <ScrollView>

                <View style={{ alignSelf: "center" }}>
                    <Carousel navigation={props.navigation} />
                </View>

                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }}>
                    <Example />
                </View>

            </ScrollView>
        </View>
    )
}
