import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Box, NativeBaseProvider } from 'native-base'
import { responsiveScreenFontSize, responsiveScreenWidth } from 'react-native-responsive-dimensions'

export default function ThreeScreen(props) {


    const LinearGradient = require('expo-linear-gradient').LinearGradient;
    const config = {
        dependencies: {
            'linear-gradient': LinearGradient
        }
    };
    return (
        <NativeBaseProvider config={config}>
            <ScrollView>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center', marginVertical: 30 }}>
                    <Text style={{ fontSize: responsiveScreenFontSize(2.5), fontWeight: 'bold', textAlign: 'center' }}>¿Cómo gano puntos?</Text>
                </View>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }} >

                    <Box style={{ alignSelf: 'center' }} bg={{
                        linearGradient: {
                            colors: ['gray.200', 'gray.100'],
                            start: [0, 0],
                            end: [1, 0]
                        }
                    }} p="12" rounded="xl" _text={{
                        fontSize: 'md',
                        fontWeight: 'medium',
                        color: 'warmGray.50',
                        textAlign: 'center'
                    }}>
                        <Text style={{ fontSize: responsiveScreenFontSize(2), marginBottom: 10 }}>Existen 2 formas de ganar puntos:</Text>
                        <Text style={{ fontSize: responsiveScreenFontSize(2) }}>1. Subiendo facturas de compra de productos 3M, los puntos ganados van a depender del valor neto de los productos en cada factura. Las referencias indicadas aquí otorgan puntos extra.</Text>
                        <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                            <Text style={{ marginRight: 3, fontWeight: 'bold', color: '#64C2C8' }} onPress={() => props.navigation.navigate('ProductHelp')}>Lista de productos</Text>
                        </View>
                        <Text style={{ fontSize: responsiveScreenFontSize(2) }}>2. Realizando cada una de las capacitaciones ofrecidas por 3M y respondiendo las trivias acerca de dichas capacitaciones.</Text>
                    </Box>

                </View>
            </ScrollView>
        </NativeBaseProvider>
    )
}
