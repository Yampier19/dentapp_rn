import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Box, NativeBaseProvider } from 'native-base'
import { responsiveScreenFontSize, responsiveScreenWidth } from 'react-native-responsive-dimensions'

export default function FourScreen(props) {


    const LinearGradient = require('expo-linear-gradient').LinearGradient;
    const config = {
        dependencies: {
            'linear-gradient': LinearGradient
        }
    };
    return (
        <NativeBaseProvider config={config}>
            <ScrollView>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center', marginVertical: 30 }}>
                    <Text style={{ fontSize: responsiveScreenFontSize(2.5), fontWeight: 'bold', textAlign: 'center' }}>¿Cómo avanzar de nivel?</Text>
                </View>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }} >

                    <Box style={{ alignSelf: 'center' }} bg={{
                        linearGradient: {
                            colors: ['gray.200', 'gray.100'],
                            start: [0, 0],
                            end: [1, 0]
                        }
                    }} p="12" rounded="xl" _text={{
                        fontSize: 'md',
                        fontWeight: 'medium',
                        color: 'warmGray.50',
                        textAlign: 'center'
                    }}>
                        <Text style={{ fontSize: responsiveScreenFontSize(2) }}>De acuerdo a las compras acumuladas registradas en el programa, por medio de las facturas con productos 3M, cada compra que se realice se va sumando.
                            Para saber cuál es el avance, solo debes ir a la sección de NIVELES que se encuentra en tu perfil.
                            ¡Recuerda que tienes 6 meses para llegar al nivel de Diamante!.</Text>
                    </Box>
                </View>
            </ScrollView>
        </NativeBaseProvider>
    )
}
