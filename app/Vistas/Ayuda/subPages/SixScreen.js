import { StyleSheet, Text, View, ScrollView, Linking, TouchableOpacity,Alert } from 'react-native'
import React, {useState} from 'react'
import { Box, NativeBaseProvider } from 'native-base'
import { responsiveScreenFontSize, responsiveScreenWidth } from 'react-native-responsive-dimensions'
import Icon from "react-native-vector-icons/FontAwesome"

export default function FiveScreen(props) {


    {/*  API WHATSAPP  */ }
    const [whatsAppMessage, setWhatsAppMessage] = useState();
    const sendMsg = () => {
        let URL = Linking.openURL('whatsapp://send?text=Buen día, necesito ayuda con Dentapp, mi nombre es ...&phone=+573125614712')
        Linking.openURL(URL)
            .then((data) => {
                console.log('WhatsApp Opened');
            })
            .catch(() => {
                Alert.alert('Make sure Whatsapp installed on your device');
            });
    };


    const LinearGradient = require('expo-linear-gradient').LinearGradient;
    const config = {
        dependencies: {
            'linear-gradient': LinearGradient
        }
    };
    return (
        <NativeBaseProvider config={config}>
            <ScrollView>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center', marginVertical: 30 }}>
                    <Text style={{ fontSize: responsiveScreenFontSize(2.5), fontWeight: 'bold', textAlign: 'center' }}>Tengo problemas para ingresar</Text>
                </View>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }} >

                    <Box style={{ alignSelf: 'center' }} bg={{
                        linearGradient: {
                            colors: ['gray.200', 'gray.100'],
                            start: [0, 0],
                            end: [1, 0]
                        }
                    }} p="12" rounded="xl" _text={{
                        fontSize: 'md',
                        fontWeight: 'medium',
                        color: 'warmGray.50',
                        textAlign: 'center'
                    }}>
                        <Text style={{ fontSize: responsiveScreenFontSize(2), marginBottom: 5 }}>Si tienes problemas para ingresar, por favor revisa las siguientes opciones: </Text>
                        <Text style={{ fontSize: responsiveScreenFontSize(2), marginBottom: 5 }}>1. Olvidaste tu contraseña: Da click aquí e ingresa tu correo electrónico, donde te llegarán las instrucciones para asignarte una nueva contraseña.</Text>
                        <Text style={{ fontSize: responsiveScreenFontSize(2), marginBottom: 5 }}>2. No tienes un usuario y contraseña: Estos datos fueron enviados a tu correo electrónico, si no te llegaron, es porque no fuiste escogido para participar en el programa.</Text>

                        <View style={{ flexDirection: 'row' }}>
                            <Text>Comunícate a la linea de</Text>
                            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 5 }} onPress={sendMsg}>
                                <Icon name="whatsapp" style={{ color: 'green', backgroundColor: 'green', borderRadius: 40, color: 'white', height: 11, width: 11, fontSize: 13 }} type="FontAwesome5" /><Text style={{ marginLeft: 3, fontWeight: 'bold', color: 'green' }}>WhatsApp</Text>
                            </TouchableOpacity>
                        </View>
                        <Text>para mayor información</Text>
                    </Box>
                </View>
            </ScrollView>
        </NativeBaseProvider>
    )
}
