import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Box, NativeBaseProvider } from 'native-base'
import { responsiveScreenFontSize, responsiveScreenWidth } from 'react-native-responsive-dimensions'

export default function FiveScreen(props) {


    const LinearGradient = require('expo-linear-gradient').LinearGradient;
    const config = {
        dependencies: {
            'linear-gradient': LinearGradient
        }
    };
    return (
        <NativeBaseProvider config={config}>
            <ScrollView>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center', marginVertical: 30 }}>
                    <Text style={{ fontSize: responsiveScreenFontSize(2.5), fontWeight: 'bold', textAlign: 'center' }}>¿Qué puedo hacer con los puntos?</Text>
                </View>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }} >

                    <Box style={{ alignSelf: 'center' }} bg={{
                        linearGradient: {
                            colors: ['gray.200', 'gray.100'],
                            start: [0, 0],
                            end: [1, 0]
                        }
                    }} p="12" rounded="xl" _text={{
                        fontSize: 'md',
                        fontWeight: 'medium',
                        color: 'warmGray.50',
                        textAlign: 'center'
                    }}>
                        <Text style={{ fontSize: responsiveScreenFontSize(2) }}>Los puntos podrán ser redimidos por productos que se encuentran en la sección de PREMIOS, donde se podrá encontrar una variedad de categorías, una vez sean redimidos, los puntos disminuirán.</Text>
                    </Box>
                </View>
            </ScrollView>
        </NativeBaseProvider>
    )
}
