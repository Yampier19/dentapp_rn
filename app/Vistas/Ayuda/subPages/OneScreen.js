import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Box, NativeBaseProvider } from 'native-base'
import { responsiveScreenFontSize, responsiveScreenWidth } from 'react-native-responsive-dimensions'

export default function OneScreen() {


    const LinearGradient = require('expo-linear-gradient').LinearGradient;
    const config = {
        dependencies: {
            'linear-gradient': LinearGradient
        }
    };
    return (
        <NativeBaseProvider config={config}>
            <ScrollView>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center', marginVertical: 30 }}>
                    <Text style={{ fontSize: responsiveScreenFontSize(2.5), fontWeight: 'bold', textAlign: 'center' }}>¿Qué es DentApp?</Text>
                </View>
                <View style={{ width: responsiveScreenWidth(90), alignSelf: 'center' }} >

                    <Box style={{ alignSelf: 'center' }} bg={{
                        linearGradient: {
                            colors: ['gray.200', 'gray.100'],
                            start: [0, 0],
                            end: [1, 0]
                        }
                    }} p="12" rounded="xl" _text={{
                        fontSize: 'md',
                        fontWeight: 'medium',
                        color: 'warmGray.50',
                        textAlign: 'center'
                    }}>
                        <Text style={{ fontSize: responsiveScreenFontSize(2) }}>DentApp es un programa de incentivos dirigido a odontólogos que adquieren productos 3M, donde podrán ganar por las compras realizadas y aprender sobre temas relacionados a su profesión.
                            A los participantes se les enviará una invitación por medio del correo electrónico, desde allí deberán seguir las instrucciones para ingresar al programa y empezar a participar.
                            Ganarán puntos por subir las facturas de compra de productos 3M, los puntos serán otorgados de acuerdo con el valor de la compra, algunos productos dan puntos extra y adicionalmente por cada capacitación realizada, también ganarán puntos.</Text>
                    </Box>

                </View>
            </ScrollView>
        </NativeBaseProvider>
    )
}
